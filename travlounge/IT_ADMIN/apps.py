from django.apps import AppConfig


class ItAdminConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'IT_ADMIN'
    verbose_name = "IT ADMIN"
