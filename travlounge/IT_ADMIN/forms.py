from dataclasses import field
from django import forms
from IT_ADMIN .models import Facility_docs, Service, Banner, Service_type
from IT_ADMIN .models import Carwash, Facility_docs, Service, Banner


def validate_file_extension(value):
    import os
    from django.core.exceptions import ValidationError
    ext = os.path.splitext(value.name)[1]
    valid_extensions = ['.pdf']
    if not ext.lower() in valid_extensions:
        raise ValidationError(u'Only PDF are allowded.')


def validate_image_extension(value):
    import os
    from django.core.exceptions import ValidationError
    ext = os.path.splitext(value.name)[1]
    valid_extensions = ['.jpeg', 'png']
    if not ext.lower() in valid_extensions:
        raise ValidationError(u'Unsupported file extension.')


class CouplingUploadForm(forms.ModelForm):

    docs = forms.FileField(label=' File Upload:',
                           required=True, validators=[validate_file_extension])

    class Meta:
        model = Facility_docs
        fields = '__all__'


class service_logo_Form(forms.ModelForm):
    logo = forms.ImageField()
    logo_image_id = forms.CharField(widget=forms.HiddenInput)

    class Meta:
        model = Service
        fields = "__all__"
        widgets = {'logo_image_id': forms.Textarea}

    def __init__(self, *args, **kwargs):
        super(service_logo_Form, self).__init__(*args, **kwargs)
        self.fields['logo'].required = False
        self.fields['logo_image_id'].required = False
        self.fields['logo_image_id'].label = ""


class Banner_Form(forms.ModelForm):
    photo = forms.ImageField()
    banner_image_id = forms.CharField(widget=forms.HiddenInput)

    class Meta:
        model = Banner
        fields = "__all__"
        widgets = {'banner_image_id': forms.Textarea}

    def __init__(self, *args, **kwargs):
        super(Banner_Form, self).__init__(*args, **kwargs)
        self.fields['photo'].required = False
        self.fields['banner_image_id'].required = False
        self.fields['banner_image_id'].label = ""


class vehicle_Form(forms.ModelForm):
    image = forms.ImageField()
    vehicle_image_id = forms.CharField(widget=forms.HiddenInput)

    class Meta:
        model = Carwash
        fields = "__all__"
        widgets = {'vehicle_image_id': forms.Textarea}

    def __init__(self, *args, **kwargs):
        super(vehicle_Form, self).__init__(*args, **kwargs)
        self.fields['image'].required = False
        self.fields['vehicle_image_id'].required = False
        self.fields['vehicle_image_id'].label = ""


class speciality_Form(forms.ModelForm):
    image = forms.ImageField()
    speciality_image_id = forms.CharField(widget=forms.HiddenInput)

    class Meta:
        model = Carwash
        fields = "__all__"
        widgets = {'speciality_image_id': forms.Textarea}

    def __init__(self, *args, **kwargs):
        super(speciality_Form, self).__init__(*args, **kwargs)
        self.fields['image'].required = False
        self.fields['speciality_image_id'].required = False
        self.fields['speciality_image_id'].label = ""


class CouplingUploadForm3(forms.ModelForm):

    docs = forms.FileField(label=' File Upload:',
                           required=True, validators=[validate_file_extension])

    class Meta:
        model = Facility_docs
        fields = '__all__'


class ProductForm(forms.ModelForm):
    @staticmethod
    def parse_filter_kwargs(**kwargs):
        if 'initial' in kwargs:
            if u'_changelist_filters' in kwargs['initial']:
                filters = kwargs['initial'].pop('_changelist_filters')
                for key, value in [p.split('=') for p in filters.split('&')]:
                    kwargs['initial'][key] = value
        return kwargs

    def __init__(self, *args, **kwargs):
        kwargs = self.parse_filter_kwargs(**kwargs)
        super(ProductForm, self).__init__(*args, **kwargs)


# class servicetype_form(forms.ModelForm):

#     image_id = forms.CharField()

#     class Meta:
#         model = Service_type
#         fields = "__all__"

#     def __init__(self, *args, **kwargs):
#         super(servicetype_form, self).__init__(*args, **kwargs)
#         self.fields['image_id'].required = False
