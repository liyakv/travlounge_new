from email.mime import image
from django.utils import timezone
import datetime
from django.db import models
from django.contrib.auth.models import Group, User
from django.db.models.deletion import SET_NULL
from django.utils.translation import ugettext_lazy as _
import cloudinary
from cloudinary.models import CloudinaryField
from django.core.validators import FileExtensionValidator
from django.utils import timezone
from django.db.models.signals import pre_save, post_save, post_delete
import requests
import json
from django.core import serializers
from django.forms.models import model_to_dict

# from HR.models import Employee

# Create your models here.
# ----------------------------Rename Groups as Roles---------------------


class Role(Group):
    class Meta:
        proxy = True
        app_label = 'auth'
        verbose_name = _('Role')
# -----------------------------basemodel-------------------------------------------------------------------------------------------------------------


class BaseModel(models.Model):
    """Model for subclassing."""
    created_on = models.DateTimeField(auto_now_add=True, null=True)
    updated_on = models.DateTimeField(auto_now=True)
    is_deleted = models.BooleanField(default=False)

    class Meta:
        abstract = True
        ordering = ['-created_on']
# --------------------IT ADMIN----------------------------------

TYPE = (("s","sleeping pod"),
                 ("t","toloo"),
                 ("cw","carwash"),
                 ("ec","e-charge"),
                 ("c","coffee"),
                 ("m","minimart"))

class Service(BaseModel):  # create new service (example: Toloo)@@@@
    service_name = models.CharField(max_length=60)
    type= models.CharField(max_length=60,choices=TYPE, unique=True)
    description = models.TextField(max_length=500)
    act_as_service = models.BooleanField(default=True)
    logo = CloudinaryField('images/service')

    def __str__(self):
        return self.service_name


def service_post_save(sender, created, instance, *args, **kwargs):
    logo = str(instance.logo)
    logo = logo.replace("http", "https")

    headers = {"Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjEzNzUwOTIwMDA1LCJpYXQiOjE2NTQ5MjAwMDUsIm1vYmlsZSI6IjExMjIzMzQ0NTUifQ.AmsZIvdFsld_KzvufGMiB2YtdE67ezjnFOYFdVvZtqU"}
    if created:
        r = requests.post('http://128.199.139.248:6969/createService',
                          data=json.dumps({
                              "id": instance.id,
                              "service_name": instance.service_name,
                              "type": instance.type,
                              "logo": logo,
                              "description": instance.description,
                              "act_as_service": instance.act_as_service
                          }),headers=headers)
    else:
        r = requests.put(f'http://128.199.139.248:6969/getServiceById/{instance.id}',
                         data=json.dumps({
                             "id": instance.id,
                             "service_name": instance.service_name,
                             "type": instance.type,
                             "logo": logo,
                             "description": instance.description,
                             "act_as_service": instance.act_as_service,
                             "is_deleted": instance.is_deleted,
                         }),headers=headers)


post_save.connect(service_post_save, sender=Service)


class Service_type(BaseModel):  # create new service type (example: TL4x4 )
    service_type = models.CharField(max_length=60)
    service_name = models.ForeignKey(
        Service, on_delete=models.CASCADE, null=True)
    act_as_service = models.BooleanField(default=True)
    count = models.IntegerField(default=0)

    def __str__(self):
        return self.service_type

    class Meta:
        verbose_name_plural = 'Service types'
        verbose_name = 'New Service type'


def service_type_post_save(sender, created, instance, *args, **kwargs):
    print(instance.service_name,"jjjjjjjjjjjjjjjjjjjjjjjjjjj")
    headers = {"Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjEzNzUwOTIwMDA1LCJpYXQiOjE2NTQ5MjAwMDUsIm1vYmlsZSI6IjExMjIzMzQ0NTUifQ.AmsZIvdFsld_KzvufGMiB2YtdE67ezjnFOYFdVvZtqU"}
    if created:
        r = requests.post('http://128.199.139.248:6969/createServiceType',
                          data=json.dumps({
                              "id": instance.id,
                              "service_id": instance.service_name.id,
                              "type": instance.service_type,
                              "count": instance.count,
                              "act_as_service": instance.act_as_service
                          }),headers=headers)
    else:
        r = requests.put(f'http://128.199.139.248:6969/getServiceTypeById/{instance.id}',
                         data=json.dumps({
                             "id": instance.id,
                             "service_id": instance.service_name.id,
                             "type": instance.service_type,
                             "count": instance.count,
                             "act_as_service": instance.act_as_service,
                             "is_deleted": instance.is_deleted
                         }),headers=headers)


post_save.connect(service_type_post_save, sender=Service_type)


class Service_to_role(BaseModel):  # Mapping roles to service types
    service_type = models.ForeignKey(
        Service_type, on_delete=models.CASCADE, null=True)
    role = models.ForeignKey(Group, on_delete=models.CASCADE, null=True)
    no_of_role = models.PositiveIntegerField()

    def __str__(self):
        return str(self.role)


# ---------------CREATE WAREHOUSE---------------------
SIZE_CHOICES = (("small", "small"), ("medium", "medium"), ("large", "large"),)
# class Warehouse_details(BaseModel):
#     name = models.CharField(max_length=50)
#     size = models.CharField(max_length=20, choices=SIZE_CHOICES)
#     address = models.TextField(max_length=300)
#     cash = models.DecimalField(max_digits=6, decimal_places=2, default=0.00)
#     def __str__(self):
#         return self.name
# ---------------CREATE FACILITY---------------------
STATUS_CHOICES = (("running", "running"),
                  ("stopped", "stopped"),
                  ("pending", "pending"),
                  ("initiated", "initiated"),)


class Facility(BaseModel):
    facility_name = models.CharField(max_length=60)
    state = models.CharField(max_length=60)
    District = models.CharField(max_length=60)
    pincode = models.CharField(max_length=60)
    longitude = models.CharField(max_length=60)
    latitude = models.CharField(max_length=60)
    # Pending or Initialised or running or stopped
    facility_status = models.CharField(
        max_length=12, choices=STATUS_CHOICES, default='pending')
    Address = models.TextField(max_length=300)
    # warehouse = models.ManyToManyField(Warehouse_details)
    sts = models.BooleanField(default=False)

    def __str__(self):
        return self.facility_name + " : "+self.facility_status

    class Meta:
        verbose_name_plural = 'Facilities'
        verbose_name = 'New Facility'

def facility_post_save(sender, created, instance, *args, **kwargs):
    print("kkkkkkkkkkkkkkkkkkkkkkkkkkkk")
    headers = {"Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjEzNzUwOTIwMDA1LCJpYXQiOjE2NTQ5MjAwMDUsIm1vYmlsZSI6IjExMjIzMzQ0NTUifQ.AmsZIvdFsld_KzvufGMiB2YtdE67ezjnFOYFdVvZtqU"}
    if created:
        print("createdddddddddddddddddddddddddddddd")
        r = requests.post('http://128.199.139.248:6969/createFacilities',
                          data=json.dumps({
                              "id": instance.id,
                              "name": instance.facility_name,
                              "state": instance.state,
                              "district": instance.District,
                              "pincode": instance.pincode,
                              "longitude": instance.longitude,
                              "latitude": instance.latitude,
                              "facility_status": instance.facility_status,
                              "address": instance.Address,
                          }),headers=headers)
    else:
        print("puttttttttttttttttttttttttttttt")
        r = requests.put(f'http://128.199.139.248:6969/getFacilityById/{instance.id}',
                         data=json.dumps({
                             "id": instance.id,
                             "name": instance.facility_name,
                             "state": instance.state,
                             "district": instance.District,
                             "pincode": instance.pincode,
                             "longitude": instance.longitude,
                             "latitude": instance.latitude,
                             "facility_status": instance.facility_status,
                             "address": instance.Address,
                             "is_deleted": instance.is_deleted,
                         }),headers=headers)


post_save.connect(facility_post_save, sender=Facility)



# ------------------------------------------------------FacilitySleepingPod------------------------------------------------------------


STATUS = (
    ("stopped", "stopped"),
    ("running", "running")
)
class Facility_to_service(BaseModel):  # Mapping A facility to service type
    service_type = models.ForeignKey(
        Service_type, on_delete=models.CASCADE, null=True,verbose_name="service")
    facility = models.ForeignKey(Facility, on_delete=models.CASCADE, null=True)
    gstin = models.CharField(max_length=60)
    status = models.CharField(max_length=20, default='running', choices=STATUS)

    def __str__(self):
        return self.facility.facility_name + " - " + self.service_type.service_type

    class Meta:
        verbose_name = "Services available in Facilites"


def facility_to_service_post_save(sender, created, instance, *args, **kwargs): 
    if_sleeping_pod=instance.service_type.service_name.type
    scount=instance.service_type.count
    facility_id = instance.facility
    print("kkkkkkkkkkkkk")

    id = instance.id
    facility_name=instance.facility.id
    print(facility_name,"faccccccccccc")
    headers = {"Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjEzNzUwOTIwMDA1LCJpYXQiOjE2NTQ5MjAwMDUsIm1vYmlsZSI6IjExMjIzMzQ0NTUifQ.AmsZIvdFsld_KzvufGMiB2YtdE67ezjnFOYFdVvZtqU"}

    if created:
        print(instance.service_type.id,"createdddddddddddddddddddddddddddddddd")
        r = requests.post('http://128.199.139.248:6969/createFacilityServiceType',
                          data=json.dumps({
                              "service_type_id": instance.service_type.id,
                              "facility_id": facility_name,
                          }),headers=headers)
        if if_sleeping_pod =='s':
            for i in range(1,scount+1) :
                FSPsave=FacilitySleepingPod(facility_to_service_id=instance,sleeping_pod_number=i)
                FSPsave.save()   
    else:
        r = requests.put(f'http://128.199.139.248:6969/getcreateFacilityServiceType/{instance.id}',
                         data=json.dumps({
                             "service_type_id": instance.service_type.id,
                             "facility_id": facility_name,
                             "is_deleted": instance.is_deleted,
                         }),headers=headers)

        # print(if_sleeping_pod,"ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo")
        # if if_sleeping_pod == 's':
        #     if instance.is_deleted==True:
        #         f_t_s_id=instance.id
        #         FSdelete=FacilitySleepingPod.objects.filter(facility_to_service_id=f_t_s_id)
        #         print(FSdelete,".............................FSdelete")
        #         for facser in FSdelete:
        #             if FacilitySleepingPod.objects.filter(id=facser.id).exists():
        #                 facser.delete()
        #         Facility_to_service.objects.get(id=f_t_s_id).delete()
post_save.connect(facility_to_service_post_save, sender=Facility_to_service)


class FacilitySleepingPod(BaseModel):
    facility_to_service_id = models.ForeignKey(Facility_to_service, on_delete=models.CASCADE)
    sleeping_pod_number = models.IntegerField(default=0)
    is_available = models.BooleanField( default=True)


def FacilitySleepingPod_post_save(sender, created, instance, *args, **kwargs):
    headers = {"Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjEzNzUwOTIwMDA1LCJpYXQiOjE2NTQ5MjAwMDUsIm1vYmlsZSI6IjExMjIzMzQ0NTUifQ.AmsZIvdFsld_KzvufGMiB2YtdE67ezjnFOYFdVvZtqU"}
    if created:
        r = requests.post('http://128.199.139.248:6969/facilitySleepingpod',
                          data=json.dumps({
                                "id": instance.id,
                                "facility_id": instance.facility_to_service_id.facility.id,
                                "sleeping_pod_number": instance.id,
                          }),headers=headers)

    # id: int
    # facility_id: int
    # sleeping_pod_number: int
    else:
        r = requests.put(f'http://128.199.139.248:6969/getFacilitySleepingpodById/{instance.id}',
                         data=json.dumps({
                                "id": instance.id,
                                "facility_id": instance.facility_to_service_id.facility.id,
                                "sleeping_pod_number": instance.id,
                                "is_deleted": instance.is_deleted,  
                         }),headers=headers)


post_save.connect(FacilitySleepingPod_post_save, sender=FacilitySleepingPod)


# def Facility_to_service_post_save(sender, created, instance, *args, **kwargs):
#     if_sleeping_pod=instance.service_type.service_name.type
#     scount=instance.service_type.count
#     facility_id = instance.facility

#     if created:
#         if if_sleeping_pod =='s':
#             for i in range(1,scount+1) :
#                 FSPsave=FacilitySleepingPod(facility_to_service_id=instance,sleeping_pod_number=i)
#                 FSPsave.save()        
#     else:
#         print(if_sleeping_pod,"ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo")
#         if if_sleeping_pod == 's':
#             if instance.is_deleted==True:
#                 f_t_s_id=instance.id
#                 FSdelete=FacilitySleepingPod.objects.filter(facility_to_service_id=f_t_s_id)
#                 print(FSdelete,".............................FSdelete")
#                 for facser in FSdelete:
#                     if FacilitySleepingPod.objects.filter(id=facser.id).exists():
#                         facser.delete()
#                 Facility_to_service.objects.get(id=f_t_s_id).delete()

# post_save.connect(Facility_to_service_post_save, sender=Facility_to_service)

# -------------------------------for multiple images-----------------------------------------


class Photos_facility(BaseModel):
    facility_id = models.ForeignKey(
        Facility, on_delete=models.CASCADE, null=True)
    photo = CloudinaryField('images/facility')


class Photos_service_type(BaseModel):
    FacilityServiceId = models.ForeignKey(Facility_to_service, on_delete=models.SET_NULL, null=True)
    # serviceType = models.ForeignKey(Service_type,on_delete = models.CASCADE)
    photo = models.ImageField('image/service')
# ---------------------------------------------------------------------------------------------


class Facility_docs(BaseModel):
    facility_id = models.ForeignKey(
        Facility, on_delete=models.CASCADE, related_name="doc")
    doc_name = models.CharField(max_length=60, null=True)
    docs = CloudinaryField('Facility documents')

    def __str__(self):
        return self.doc_name

    class Meta:
        verbose_name = 'Facility document'
# ------------------------------------------------------------------------------------------------------------------------------------------------------------------


class Custom_User(BaseModel):  # Add users basic details
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    Facilities = models.ManyToManyField(Facility, blank=True)
    role = models.ForeignKey(Group, on_delete=models.CASCADE)
    # warehouse = models.ManyToManyField(
    #     Warehouse_details, blank=True, help_text="Only HR role can select Warehouses. ")

    def __str__(self):
        return str(self.user.first_name)
# /////////////////////////////////////////////////////////////////////////////////////


class token_get(BaseModel):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    token_get = models.CharField(max_length=200)


class Packages(BaseModel):
    package_name = models.CharField(max_length=200,unique=True)
    amount = models.IntegerField()
    months = models.IntegerField()
    description = models.TextField(max_length=100)

    def __str__(self):
        return self.package_name

    class Meta:
        verbose_name = 'Create package'
        verbose_name_plural = 'Create packages'


def packages_post_save(sender, created, instance, *args, **kwargs):
    headers = {"Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjEzNzUwOTIwMDA1LCJpYXQiOjE2NTQ5MjAwMDUsIm1vYmlsZSI6IjExMjIzMzQ0NTUifQ.AmsZIvdFsld_KzvufGMiB2YtdE67ezjnFOYFdVvZtqU"}
    if created:
        r = requests.post('http://128.199.139.248:6969/createPackage',
                          data=json.dumps({
                              "id": instance.id,
                              "name": instance.package_name,
                              "amount": instance.amount,
                              "months": instance.months,
                              "description": instance.description,
                              "toloo_checkin_per_time":10,#change..............................
                          }),headers=headers)
    else:
        r = requests.put(f'http://128.199.139.248:6969/getPackageById/{instance.id}',
                         data=json.dumps({
                             "id": instance.id,
                             "name": instance.package_name,
                             "amount": instance.amount,
                             "months": instance.months,
                             "description": instance.description,
                             "is_deleted": instance.is_deleted,
                             "toloo_checkin_per_time":10,#change...............................+
                         }),headers=headers)


post_save.connect(packages_post_save, sender=Packages)

def packages_post_delete(sender, instance, *args, **kwargs):
    headers = {"Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjEzNzUwOTIwMDA1LCJpYXQiOjE2NTQ5MjAwMDUsIm1vYmlsZSI6IjExMjIzMzQ0NTUifQ.AmsZIvdFsld_KzvufGMiB2YtdE67ezjnFOYFdVvZtqU"}
    r = requests.put(f'http://128.199.139.248:6969/getPackageById/{instance.id}',
        data=json.dumps({
            "id": instance.id,
            "name": instance.package_name,
            "amount": instance.amount,
            "months": instance.months,
            "description": instance.description,
            "is_deleted": True,
            "toloo_checkin_per_time":10,#change...............................+
        }),headers=headers)

post_delete.connect(packages_post_delete, sender=Packages)


class Package_to_service(BaseModel):
    Package = models.ForeignKey(Packages, on_delete=models.CASCADE)
    service = models.ForeignKey(Service, on_delete=models.CASCADE, null=True)
    # service = models.ForeignKey(Service_type, on_delete=models.CASCADE, verbose_name="service type")
    number = models.IntegerField()

    def __str__(self):
        return str(self.Package.package_name)



def package_to_service_post_save(sender, created, instance, *args, **kwargs):
    headers = {"Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjEzNzUwOTIwMDA1LCJpYXQiOjE2NTQ5MjAwMDUsIm1vYmlsZSI6IjExMjIzMzQ0NTUifQ.AmsZIvdFsld_KzvufGMiB2YtdE67ezjnFOYFdVvZtqU"}
    if created:
        print(type(instance),"createdddddddddddddddddddddddd")
        r = requests.post('http://128.199.139.248:6969/createPackageService',
                          data=json.dumps({
                              "id": instance.id,
                              "package_id": instance.Package.id,
                              "service_id": instance.service.id,
                              "number": instance.number,
                          }),headers=headers)
    else:
        r = requests.put(f'http://128.199.139.248:6969/getPackageServiceById/{instance.id}',
                         data=json.dumps({
                             "id": instance.id,
                             "package_id": instance.Package.id,
                             "service_id": instance.service.id,
                             "number": instance.number,
                         }),headers=headers)
post_save.connect(package_to_service_post_save, sender=Package_to_service)


class Banner(BaseModel):
    name = models.CharField(max_length=100)
    service_name = models.ForeignKey(
        Service, on_delete=models.CASCADE, null=True)
    photo = CloudinaryField('images/facility')

    def __str__(self):
        return self.name


def banner_post_save(sender, created, instance, *args, **kwargs):
    print(instance.photo,"fffffffffffffffffffff")
    photo = str(instance.photo)
    photo = photo.replace("http", "https")
    headers = {"Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjEzNzUwOTIwMDA1LCJpYXQiOjE2NTQ5MjAwMDUsIm1vYmlsZSI6IjExMjIzMzQ0NTUifQ.AmsZIvdFsld_KzvufGMiB2YtdE67ezjnFOYFdVvZtqU"}
    if created:
        print("createeeeeeeeeeeeeeeeeee")
        r = requests.post('http://128.199.139.248:6969/createBanners',
                          data=json.dumps({
                              "id": instance.id,
                              "title": instance.name,
                              "image": photo,
                              "service_id":instance.service_name.id,
                              "link":'',
                          }),headers=headers)
    else:
        print("puttttttttttttttttttttt")
        r = requests.put(f'http://128.199.139.248:6969/getBannerById/{instance.id}',
                         data=json.dumps({
                            "id": instance.id,
                             "title": instance.name,
                             "image": photo,
                             "service_id":instance.service_name.id,
                             "link":'',
                             "is_deleted": instance.is_deleted,
                         }),headers=headers)


post_save.connect(banner_post_save, sender=Banner)

def banner_post_delete(sender, instance, *args, **kwargs):
    print(instance,"bannerrrrrrrrrrrrrrrrrrrrrrrr")
    photo = str(instance.photo)
    photo = photo.replace("http", "https")
    headers = {"Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjEzNzUwOTIwMDA1LCJpYXQiOjE2NTQ5MjAwMDUsIm1vYmlsZSI6IjExMjIzMzQ0NTUifQ.AmsZIvdFsld_KzvufGMiB2YtdE67ezjnFOYFdVvZtqU"}
    r = requests.put(f'http://128.199.139.248:6969/getBannerById/{instance.id}',
        data=json.dumps({
                        "id": instance.id,
                        "title": instance.name,
                        "image": photo,
                        "service_id":instance.service_name.id,
                        "link":'',
                        "is_deleted": True,
        }),headers=headers)


post_delete.connect(banner_post_delete, sender=Banner)

# def post_delete(sender, created, instance, *args, **kwargs):
#     photo = str(instance.photo)
#     photo = photo.replace("http", "https")
#     headers = {"Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjEzNzUwOTIwMDA1LCJpYXQiOjE2NTQ5MjAwMDUsIm1vYmlsZSI6IjExMjIzMzQ0NTUifQ.AmsZIvdFsld_KzvufGMiB2YtdE67ezjnFOYFdVvZtqU"}
#     r = requests.post('http://128.199.139.248:6969/createBanners',
#         data=json.dumps({
#             "id": instance.id,
#             "title": instance.name,
#             "image": photo,
#             "service_id":instance.service_name.id,
#             "link":'',
#         }),headers=headers)



class Carwash(BaseModel):
    vehicle = models.CharField(max_length=25,unique=True)
    vehicle_type = models.CharField(max_length=25)
    image = CloudinaryField('images/vehicle')

    def __str__(self):
        return self.vehicle

    class Meta:
        verbose_name_plural = 'car wash'


def carwash_post_save(sender, created, instance, *args, **kwargs):
    image = str(instance.image)
    image = image.replace("http", "https")
    
    headers = {"Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjEzNzUwOTIwMDA1LCJpYXQiOjE2NTQ5MjAwMDUsIm1vYmlsZSI6IjExMjIzMzQ0NTUifQ.AmsZIvdFsld_KzvufGMiB2YtdE67ezjnFOYFdVvZtqU"}
    if created:
        r = requests.post('http://128.199.139.248:6969/createCarTypes',
                          data=json.dumps({
                              "id": instance.id,
                              "car_types": instance.vehicle_type,
                              "car_images": image,
                          }),headers=headers)
    else:
        r = requests.put(f'http://128.199.139.248:6969/getCarTypesById/{instance.id}',
                             data=json.dumps({
                              "id": instance.id,
                              "car_types": instance.vehicle_type,
                              "car_images": image,
                              "is_deleted": instance.is_deleted,
                         }),headers=headers)


post_save.connect(carwash_post_save, sender=Carwash)

def carwash_post_delete(sender, instance, *args, **kwargs):
    print("carwashhhhhhhhhhhhhhhhhhhhhh")
    image = str(instance.image)
    image = image.replace("http", "https")
    headers = {"Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjEzNzUwOTIwMDA1LCJpYXQiOjE2NTQ5MjAwMDUsIm1vYmlsZSI6IjExMjIzMzQ0NTUifQ.AmsZIvdFsld_KzvufGMiB2YtdE67ezjnFOYFdVvZtqU"}   
    r = requests.put(f'http://128.199.139.248:6969/getCarTypesById/{instance.id}',
                             data=json.dumps({
                              "id": instance.id,
                              "car_types": instance.vehicle_type,
                              "car_images": image,
                              "is_deleted": True,
                         }),headers=headers)
post_delete.connect(carwash_post_delete, sender=Carwash)

class Speciality(BaseModel):
    title = models.CharField(max_length=25)
    service_name = models.ForeignKey(
        Service, on_delete=models.CASCADE, null=True)
    description = models.CharField(max_length=200)
    image = CloudinaryField('images/speciality')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = 'Specialities'


def speciality_post_save(sender, created, instance, *args, **kwargs):   
    image = str(instance.image)
    print(image,"llllllllllllllllll")
    image = image.replace("http", "https")
    headers = {"Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjEzNzUwOTIwMDA1LCJpYXQiOjE2NTQ5MjAwMDUsIm1vYmlsZSI6IjExMjIzMzQ0NTUifQ.AmsZIvdFsld_KzvufGMiB2YtdE67ezjnFOYFdVvZtqU"}
    if created:
        print("create specillllll")
        r = requests.post('http://128.199.139.248:6969/createSpecialities',
                          data=json.dumps({
                              "id": instance.id,
                              "title": instance.title,
                              "heading":'abcd',
                              "description": instance.description,
                              "service_id":instance.service_name.id,
                              "image": image,
                          }),headers=headers)
    else:
        print("puttttttttt specillllll")
        print(instance.is_deleted,"lllllllllllllllllllllllllllllllllllllllllllll")
        r = requests.put(f'http://128.199.139.248:6969/getSpecialitiesById/{instance.id}',
                         data=json.dumps({
                             "id": instance.id,
                             "title": instance.title,
                             "description": instance.description,
                             "heading":'abcd',
                             "service_id":instance.service_name.id,
                             "image": image,
                             "is_deleted": instance.is_deleted,
                         }),headers=headers)


post_save.connect(speciality_post_save, sender=Speciality)


def speciality_post_delete(sender, instance, *args, **kwargs):
    print("specialityyyyyyyyyyyyyyyyyyyyyyyyy")
    image = str(instance.image)
    image = image.replace("http", "https")
    headers = {"Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjEzNzUwOTIwMDA1LCJpYXQiOjE2NTQ5MjAwMDUsIm1vYmlsZSI6IjExMjIzMzQ0NTUifQ.AmsZIvdFsld_KzvufGMiB2YtdE67ezjnFOYFdVvZtqU"}   
    r = requests.put(f'http://128.199.139.248:6969/getSpecialitiesById/{instance.id}',
                data=json.dumps({
                    "id": instance.id,
                    "title": instance.title,
                    "description": instance.description,
                    "heading":'abcd',
                    "service_id":instance.service_name.id,
                    "image": image,
                    "is_deleted": True,
                }),headers=headers)


post_delete.connect(speciality_post_delete, sender=Speciality)





