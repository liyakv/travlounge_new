from re import L
from webbrowser import get
from IT_ADMIN .forms import CouplingUploadForm, service_logo_Form, Banner_Form, ProductForm, vehicle_Form, speciality_Form
from django.core.files.base import ContentFile
import base64
from django.http import HttpResponse
from IT_ADMIN .forms import CouplingUploadForm, service_logo_Form, Banner_Form, ProductForm
from FCMManager import sendPush
from FCMManager import *
import math
from django.contrib import admin
from django.contrib.auth.models import Group
from IT_ADMIN.models import Service, Service_type, Service_to_role, Role, Facility, Facility_docs, Facility_to_service, Photos_facility, Photos_service_type, Speciality
from django.http import HttpResponseRedirect, request, HttpResponse
from django.contrib.auth.models import Group, User
from IT_ADMIN.models import FacilitySleepingPod,Carwash, Service, Service_type, Service_to_role, Role, Facility, Facility_docs, Facility_to_service, Photos_facility, Photos_service_type, Custom_User, token_get, Packages, Package_to_service, Banner
from django.http import HttpResponseRedirect
from django.db.models import Q
from django.conf import settings
from django import forms
from django.urls import path
from django.shortcuts import redirect, render
from Travlounge.settings import LIST_PER_PAGE
import cloudinary
from django.utils.html import format_html
# from cloudinary.models import CloudinaryField
from django.contrib.admin.models import LogEntry
from django.contrib.admin import SimpleListFilter
from django.utils.translation import ugettext_lazy as _
from django.contrib import messages
from django.http import JsonResponse
from django.forms.models import BaseInlineFormSet
import json
from django.http import JsonResponse
from django.core.exceptions import ValidationError
import requests

from django.contrib.auth.admin import UserAdmin
from nested_inline.admin import NestedStackedInline, NestedModelAdmin
from django.forms.models import BaseInlineFormSet
from django.core.validators import MinValueValidator


# Register your models here.
# admin.site.register(Warehouse_details)

# admin.site.register(Role)
@admin.register(Role)
class Role_Admin(admin.ModelAdmin):
    list_per_page = 10

admin.site.site_url = None
admin.site.site_header = "Travlounge"


admin.site.unregister(Group)


def validate_file_extension(value):
    import os
    from django.core.exceptions import ValidationError
    ext = os.path.splitext(value.name)[1]  # [0] returns path+filename
    valid_extensions = ['.pdf', '.doc', '.docx',
                        '.jpg', '.png', '.xlsx', '.xls']
    if not ext.lower() in valid_extensions:
        raise ValidationError('Unsupported file extension.')


@admin.action(description='Delete')
def delete_action(modeladmin, request, queryset):
    queryset.update(is_deleted=True)
    print(queryset,"speciallllllyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy")

# ---------------------------------LOG TABLE--------------------------------------------------------------------------------------------------


class Logproxy(LogEntry):
    class Meta:
        proxy = True
        verbose_name = 'View Log'  # object name here
        verbose_name_plural = 'View Log'  # plural object names here


@admin.register(Logproxy)
class LogEntryAdmin(admin.ModelAdmin):
    # to have a date-based drilldown navigation in the admin page
    date_hierarchy = 'action_time'

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False
    # to filter the resultes by users, content types and action flags
    list_filter = [
        'user',
        'action_flag'
    ]

    # when searching the user will be able to search in both object_repr and change_message
    search_fields = [
        'object_repr',
        'change_message'
    ]

    list_display = [
        'action_time',
        'user',
        'content_type',
        'action_flag',
    ]
# -----------------------To create service----------------------------


@admin.register(Service)
class ServiceAdmin(admin.ModelAdmin):
    list_display = ('service_name',)
    fields = [('service_name'),('type'),('description'),
              ('logo_image_id'), ('logo')]
    # actions = [delete_action]
    form = service_logo_Form
    change_form_template = "logo.html"

    def add_view(self, request, form_url='', extra_context=None):
        if request.is_ajax():
            type_lst=list(Service.objects.filter(is_deleted=False).values_list('type',flat=True))
            values_to_remove = []
            [values_to_remove.append(x) for x in type_lst if x not in values_to_remove]
            choices=Service.type.field.choices
            dct={}
            dct = dict((x, y) for x, y in choices)
            # values_to_remove = ["o","t"]
            for key in values_to_remove:
                del dct[key]
            print(dct,".........................................dct")
            dict1={"type_list":dct}
            return JsonResponse(dict1)
        else:
            return super(ServiceAdmin, self).add_view(request, form_url, extra_context=extra_context)


    # def has_delete_permission(self, request, obj=None):
    #     return False

    # def has_add_permission(self, request, obj=None):
    #     return False

    def get_queryset(self, request):
        queryset = super(ServiceAdmin, self).get_queryset(request)
        return queryset.filter(is_deleted=False)

    # def add_view(self, request, form_url='', extra_context=None):
    #     if request.method == 'GET':
    #         object_passed = request.GET.getlist('data')

    #         if len(object_passed) != 0:
    #             list_tuple = Service.objects.values_list(
    #                 'service_name').filter(is_deleted=False)
    #             list_service = [i[0] for i in list_tuple]
    #             if object_passed[0] in list_service:
    #                 return JsonResponse({"message": "Successfully published", 'status': 200})
    #     return super(ServiceAdmin, self).add_view(request, form_url, extra_context=extra_context)

    def save_model(self, request, obj, file, change):
        super().save_model(request, obj, file, change)
        if request.POST.get('logo_image_id'):
            img = request.POST.get('logo_image_id')
            out1 = cloudinary.uploader.upload(img)
            objres1 = out1['url']
            data_save = Service.objects.get(id=obj.id)
            data_save.logo = objres1
            data_save.save()

    # def save_model(self, request, obj, file, change):
    #     try:
    #         img = request.POST.get('logo_image_id')
    #         out1 = cloudinary.uploader.upload(img)
    #         objres1 = out1['url']
    #         print(objres1,"................objres1objres1objres1objres1objres1objres1objres1objres1objres1objres1objres1objres1")
    #         super().save_model(request, obj, file, change)
    #         data_save = Service.objects.get(id=obj.id)
    #         data_save.logo = objres1
    #         data_save.save()
    #     except:
    #         print("false.............................................0000000000000000000000000000000000000000000000000")

    def change_view(self, request, object_id, form_url='', extra_context=None, **kwargs):
        extra_context = extra_context or {}
        extra_context['logo_img'] = Service.objects.get(id=object_id)
        return super().change_view(request, object_id, form_url,  extra_context=extra_context, **kwargs)


class RequiredInlineFormSet(BaseInlineFormSet):
    def _construct_form(self, i, **kwargs):
        form = super(RequiredInlineFormSet, self)._construct_form(i, **kwargs)
        form.empty_permitted = False
        return form


# -----------------------To Allot no of roles to service types----------------------------
class Service_to_roleAdmin(admin.StackedInline):
    model = Service_to_role
    fields = [('role', 'no_of_role')]
    list_display = ('service_type',)
    extra = 1
    formset = RequiredInlineFormSet


# -----------------------To create service Type----------------------------

class servicetype_Form(forms.ModelForm):
    count = forms.IntegerField(min_value=0)


@admin.register(Service_type)
class Service_typeAdmin(admin.ModelAdmin):
    search_fields = ['service_type','service_name__service_name']
    list_display = ('service_type', 'service_name',)
    list_filter = (('service_name', admin.RelatedOnlyFieldListFilter),)
    readonly_fields = []
    
    form = servicetype_Form
    # actions = [delete_action]
    exclude = ('is_deleted',)
    inlines = [Service_to_roleAdmin, ]
    list_per_page=10

    # class Media:
    #     js = ('https://cdnjs.cloudflare.com/ajax/libs/cropperjs/2.0.0-alpha.2/cropper.js',)
    #     css = {
    #         'all': ('https://cdnjs.cloudflare.com/ajax/libs/cropperjs/2.0.0-alpha.2/cropper.css',)
    #     }

    def has_delete_permission(self, request, obj=None):
        return False

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return self.readonly_fields + ['service_type', 'service_name','act_as_service', 'count',]
        return self.readonly_fields

    def get_queryset(self, request):
        queryset = super(Service_typeAdmin, self).get_queryset(request)
        return queryset.filter(is_deleted=False)

    class Meta:
        model = Service_type

    # def get_fields(self, request, obj=None):
    #     if obj:
    #         readonly_fields = [('service_type'), ('service_name'),('act_as_service'), ('count')]
    #     return self.fields

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "service_name":
            kwargs["queryset"] = Service.objects.filter(is_deleted=False)
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    # def save_model(self, request, obj, form, change):
    #     file = request.FILES['image']
    #     data = request.POST['image_id']

    #     is_non_empty_file = bool(file)
    #     is_non_empty_data = bool(data)
    #     if hasattr(self, 'instance') and self.instance.pk is not None:
    #         if is_non_empty_file == True and is_non_empty_data == False:
    #             return HttpResponseRedirect(request.path_info)
    #         else:
    #             super().save_model(request, obj, file, change)

    #     else:
    #         if data:
    #             out1 = cloudinary.uploader.upload(data)
    #             objres1 = out1['url']
    #             obj.image = objres1
    #             obj.save()

    #         else:

    #             return HttpResponseRedirect(request.path_info)

        # def save_model(self, request, obj, file, change):

        #     data = request.POST['image_id']
        #     super().save_model(request, obj, file, change)
        #     out1 = cloudinary.uploader.upload(data)
        #     objres1 = out1['url']
        #     print(objres1)
        #     data_save = Service_type.objects.get(id=obj.id)
        #     data_save.image = objres1
        #     data_save.save()

    def add_view(self, request, form_url='', extra_context=None):
        if request.method == 'GET':
            object_passed = request.GET.getlist('data')

            if len(object_passed) != 0:
                list_tuple = Service_type.objects.values_list(
                    'service_type').filter(is_deleted=False)
                list_service = [i[0] for i in list_tuple]
                if object_passed[0] in list_service:
                    return JsonResponse({"message": "Successfully published", 'status': 200})

        return super(Service_typeAdmin, self).add_view(request, form_url, extra_context=extra_context)


# -----------------------To create Facility--------------------------------------------------


class facility_to_service_Form(forms.ModelForm):
    image = forms.ImageField(allow_empty_file=True, required=False, widget=forms.ClearableFileInput(
        attrs={'multiple': True, 'class': 'custom_fileupload', 'onclick': 'load_getUserListByGroupID(this)'}))
    # image_taf = forms.CharField()
        # image_taf = forms.CharField(widget=forms.HiddenInput(
        # attrs={'class': 'form-control', }))

    class Meta:
        model = Facility_to_service
        fields = "__all__"
        widgets = {'image_taf': forms.Textarea}
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['service_type'].widget.can_add_related = False
        self.fields['service_type'].widget.can_change_related = False


    # def __init__(self, *args, **kwargs):
    #     super().__init__(*args, **kwargs)
    #     self.fields['image_taf'].required = False


# facility to service_type mapping
class Facility_to_serviceAdmin(admin.StackedInline):
    model = Facility_to_service
    exclude = ('status','is_deleted',)
    extra = 1
    can_delete = False
    form = facility_to_service_Form
    

    list_display = (
        ('service_type', 'gstin'),)

    # def get_fields(self, request, obj=None):
    #     fields = super(Facility_to_serviceAdmin, self).get_fields(request, obj)
    #     if not obj:  # obj will be None on the add page, and something on change pages
    #         fields.remove('is_deleted')
    #     return fields



    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        print(self,222222222222222222222)
        if db_field.name == "service_type":
            kwargs["queryset"] = Service_type.objects.filter(is_deleted=False)
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


class facility_img_Form(forms.ModelForm):
    image = forms.ImageField(allow_empty_file=True, required=False, 
                             widget=forms.ClearableFileInput(attrs={'multiple': True, 'class': 'custom_fileupload_small', 'accept': 'image/*',  'onclick': 'load_facilityfunction(this)'},))
    longitude = forms.DecimalField(
        widget=forms.NumberInput(attrs={'step': '0.001', 'min': '0.000', 'max': '999.99'}))
    latitude = forms.DecimalField(
        widget=forms.NumberInput(attrs={'step': '0.001', 'min': '0.000', 'max': '999.99'}))
    image_id = forms.CharField(widget=forms.HiddenInput())
    imageFacility_id = forms.CharField(widget=forms.HiddenInput())

    class Meta:
        model = Facility
        fields = "__all__"
        widgets = {'image_id': forms.Textarea}

    def __init__(self, *args, **kwargs):
        super(facility_img_Form, self).__init__(*args, **kwargs)
        self.fields['image_id'].required = False
        self.fields['imageFacility_id'].required = False


@ admin.register(Facility)  # facility creation
class FacilityAdmin(admin.ModelAdmin):
    list_display = ('facility_name', 'facility_status',)
    list_filter = ['facility_status',]
    search_fields = ['facility_name',]
    actions = ['update_status']
    inlines = [Facility_to_serviceAdmin, ]
    exclude = ('is_deleted','facility_status','sts',)
    form = facility_img_Form
    list_per_page=10
    # change_form_template = 'service_img_crop.html'
    change_form_template = 'crop.html'
    # --------------------------------------------------------------------------------------------------------------

    def change_view(self, request, object_id, form_url='', extra_context=None, **kwargs):
        extra_context = extra_context or {}
        a = ''
        if object_id:
            a = object_id
        extra_context['my_extra_content'] = a
        if request.GET.get('id_delete'):
            id_delete = request.GET.get('id_delete')
            Photos_facility.objects.filter(
                id=id_delete).update(is_deleted=True)
        if request.GET.get('id_delete_service'):
            id_delete_service = request.GET.get('id_delete_service')
            Photos_service_type.objects.filter(
                id=id_delete_service).update(is_deleted=True)
        return super().change_view(request, object_id, form_url,  extra_context=extra_context, **kwargs)

    def get_queryset(self, request):
        queryset = super(FacilityAdmin, self).get_queryset(request)
        return queryset.filter(is_deleted=False)

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        context.update({
            'show_save_and_continue': False,
        })
        return super().render_change_form(request, context, add, change, form_url, obj)

    def save_model(self, request, obj, change,file=''):
       
        super().save_model(request, obj,  change,file)
        # if (json.loads(request.POST.get("imageFacility_id")))!='':
        # print(request.POST.get("imageFacility_id"),"kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk")
        if request.POST.get("imageFacility_id"):
            imageFacility= json.loads(request.POST.get("imageFacility_id"))
            if(imageFacility[0]['imgs']!=[]):
                if(imageFacility[0]['imgs'][0]!='') :
                    base_pattern = []
                    for i in imageFacility[0]['imgs']:
                        # print(i,'\n\n\n\n\n\n\n\n\n\n iiiii',type(i),i[50:90])
                        if(i[50:90] not in base_pattern):
                            out2 = cloudinary.uploader.upload(i)
                            objres2 = out2['url']
                            Photos_facility(facility_id=obj, photo=objres2).save()
                            base_pattern.append(i[50:90])
  
    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=True)
        if request.POST.get("image_id"):
            list_of_data=json.loads(request.POST.get("image_id"))
            for count, i in enumerate(list_of_data):
                image=i['imgs']
                for y in image:
                    # print(y,'\n\n\n\n\n\n\n yyyyy')
                    out1 = cloudinary.uploader.upload(y)
                    objres1 = out1['url']
                    print(count)
                    print(instances,'\n\n\n')
                    # print(request.POST.get("facility_to_service_set-0-id"))
                    # print(request.get_full_path().split('/')[-2])
                    status = request.get_full_path().split('/')[-2]
                    if status == 'change':
                        if (instances!=[]):
                            count = len(instances)
                            Photos_service_type(FacilityServiceId=instances[count-1], photo=objres1).save()
                        else:
                            count_str = str(count)
                            oj = Facility_to_service.objects.get(facility_id=request.POST.get("facility_to_service_set-0-facility"),service_type_id=request.POST.get("facility_to_service_set-"+count_str+"-service_type"))
                            # print(oj,'\n\n\n')
                            Photos_service_type(FacilityServiceId=oj, photo=objres1).save()
                    else:
                        if (instances!=[]):
                            Photos_service_type(FacilityServiceId=instances[count], photo=objres1).save()
                        else:
                            count_str = str(count)
                            oj = Facility_to_service.objects.get(facility_id=request.POST.get("facility_to_service_set-0-facility"),service_type_id=request.POST.get("facility_to_service_set-"+count_str+"-service_type"))
                            # print(oj,'\n\n\n')
                            Photos_service_type(FacilityServiceId=oj, photo=objres1).save()


    def response_change(self, request, obj):
        # print(request.POST,'response_change\n\n')
        if request.POST.get('images'):
            service_lst = list(Photos_service_type.objects.filter(FacilityServiceId__facility=obj.id,
                                                                  is_deleted=False).values_list('FacilityServiceId__service_type__service_type', flat=True))
            service_list = list(dict.fromkeys(service_lst))
            out1 = {}
            for service in service_list:
                out = Photos_service_type.objects.filter(FacilityServiceId__facility=obj.id, is_deleted=False, FacilityServiceId__service_type__service_type=service).values(
                    'id', 'photo', 'FacilityServiceId__service_type__service_type')
                result = [(q['id'], q['photo']) for q in out]
                out1[service] = result
            out_facility = Photos_facility.objects.filter(
                facility_id=obj.id, is_deleted=False)
            return render(request, 'change_form_img.html', context={"form": out1, "form_f": out_facility, "id": obj})
        else:
            return super().response_change(request, obj)

    # -------------------------------------------------------------------------------------------------------------------------

    class Media:
        if hasattr(settings, 'GOOGLE_MAPS_API_KEY') and settings.GOOGLE_MAPS_API_KEY:
            css = {
                'all': ('css/admin/location_picker.css',),
            }
            js = (
                'https://maps.googleapis.com/maps/api/js?libraries=places&key={}'.format(
                    settings.GOOGLE_MAPS_API_KEY),
                'js/admin/loc.js',

            )

    def add_view(self, request, form_url='', extra_context=None):
        if request.method == 'GET':
            object_passed = request.GET.getlist('data')
            if len(object_passed) != 0:
                list_tuple = Facility.objects.values_list(
                    'facility_name').filter(is_deleted=False)
                list_facility = [i[0] for i in list_tuple]
                if object_passed[0] in list_facility:

                    return JsonResponse({"message": "Successfully published", 'status': 200})
        return super(FacilityAdmin, self).add_view(request, form_url, extra_context=extra_context)

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        context.update({
            'show_save_and_continue': False,
            'show_save_and_add_another': False,
        })
        return super().render_change_form(request, context, add, change, form_url, obj)

    def has_delete_permission(self, request, obj=None):
        return False
# -----------------------To upload Facility documents------------------------------------------


@ admin.register(Facility_docs)
class Facility_docsAdmin(admin.ModelAdmin):
    list_display = ('doc_name',)
    actions = [delete_action]
    form = CouplingUploadForm
    fields = [('facility_id'), ('doc_name'), ('docs'), ]
    search_fields = ['doc_name',]
    list_per_page= 10

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "facility_id":
            kwargs["queryset"] = Facility.objects.filter(is_deleted=False)
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def get_queryset(self, request):
        queryset = super(Facility_docsAdmin, self).get_queryset(request)
        return queryset.filter(is_deleted=False)

    def has_delete_permission(self, request, obj=None):
        return False
# -----------------------To Approve facility after HR initialisation----------------------------


class FacilityApproveProxy(Facility):
    class Meta:
        proxy = True
        verbose_name = 'Facilities Approved'  # object name here
        verbose_name_plural = 'Approve Facilities'  # plural object names here


@ admin.register(FacilityApproveProxy)
class FacilityApproveProxyAdmin(admin.ModelAdmin):
    list_display = ('facility_name', 'facility_status',)
    readonly_fields = [('facility_name'), ('state'),
                       ('District'), ('pincode'), ('longitude'), ('latitude')]
    fields = [('facility_name'), ('state'), ('District'),
              ('pincode'), ('longitude'), ('latitude')]
    search_fields = ['facility_name',]
    
    change_form_template = "change_form.html"
    list_per_page=10

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj=None):
        return False

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        return queryset.filter(facility_status='initiated')

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        context.update({
            'show_save_and_continue': False,
            'show_save': False,
        })
        return super().render_change_form(request, context, add, change, form_url, obj)

    def response_change(self, request, obj):
        if "apply" in request.POST:
            id = obj.id
            self.model.objects.filter(id=id).update(facility_status="running")
            headers = {"Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjEzNzUwOTIwMDA1LCJpYXQiOjE2NTQ5MjAwMDUsIm1vYmlsZSI6IjExMjIzMzQ0NTUifQ.AmsZIvdFsld_KzvufGMiB2YtdE67ezjnFOYFdVvZtqU"}    
            r = requests.put(f'http://128.199.139.248:6969/getFacilityById/{obj.id}',
                    data=json.dumps({
                        "id": obj.id,
                        "name": obj.facility_name,
                        "state": obj.state,
                        "district": obj.District,
                        "pincode": obj.pincode,
                        "longitude": obj.longitude,
                        "latitude": obj.latitude,
                        "facility_status":"running",
                        "address": obj.Address,
                        "is_deleted": obj.is_deleted,
            }),headers=headers)
            self.message_user(request, "FACILITY HAS BEEN APPROVED")
            custom = Custom_User.objects.values_list(
                'user').filter(Facilities=id)
            out = [item for t in custom for item in t]
            tk = token_get.objects.values_list(
                "token_get").filter(user__in=out)
            registration_token = [item for t in tk for item in t]
            # print(registration_token, 'newtoken')
            sendPush(title="facility approved",
                     msg=f'approved facility={obj.facility_name}', registration_token=registration_token)
            return redirect('/admin/IT_ADMIN/facilityapproveproxy/')
        return super().response_change(request, obj)
# -----------------------To View all Running Facilities and Stop them if needed----------------------------


# MAKING A CUSTOM FILTER TO ONLY SHOW RUNNING AND STOPPED FACILITIES
class RunningFilter(admin.SimpleListFilter):
    title = _('Facility status')
    parameter_name = 'facility_status'

    def lookups(self, request, model_admin):
        return (
            (1, 'Running'),
            (0, 'Stopped'),
        )

    def queryset(self, request, queryset):
        if self.value() is None:
            self.used_parameters[self.parameter_name] = 2
            return queryset.filter(Q(facility_status='running') | Q(facility_status='stopped')).filter(is_deleted=False)
        else:
            self.used_parameters[self.parameter_name] = int(self.value())
        if self.value() == 1:
            return queryset.filter(facility_status="running", is_deleted=False)
        else:
            return queryset.filter(facility_status="stopped", is_deleted=False)


class FacilityRunningProxy(Facility):
    class Meta:
        proxy = True
        verbose_name = 'All initiated Facilities '  # object name here
        verbose_name_plural = 'Facilities Status'  # plural object names here


@ admin.register(FacilityRunningProxy)
class FacilityRunningProxyAdmin(admin.ModelAdmin):
    list_filter = (RunningFilter,)
    list_display = ('facility_name', 'facility_status',)
    search_fields = ['facility_name','facility_status']
    readonly_fields = [('facility_name'), ('state'),
                       ('District'), ('pincode'), ('longitude'), ('latitude')]
    fields = [('facility_name'), ('state'), ('District'),
              ('pincode'), ('longitude'), ('latitude')]
    change_form_template = "change_form1.html"
    list_per_page = 10

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj=None):
        return False

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        return queryset.filter(Q(facility_status='running') | Q(facility_status='stopped'))

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        context.update({
            'show_save_and_continue': False,
            'show_save': False,
        })
        return super().render_change_form(request, context, add, change, form_url, obj)

    def response_change(self, request, obj):
        if "apply1" in request.POST:
            id = obj.id
            self.model.objects.filter(id=id).update(facility_status="stopped")
            self.message_user(request, "FACILITY STOPPED")
            Facility_to_service.objects.filter(facility=id).update(status='stopped')
            headers = {"Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjEzNzUwOTIwMDA1LCJpYXQiOjE2NTQ5MjAwMDUsIm1vYmlsZSI6IjExMjIzMzQ0NTUifQ.AmsZIvdFsld_KzvufGMiB2YtdE67ezjnFOYFdVvZtqU"}    
            r = requests.put(f'http://128.199.139.248:6969/getFacilityById/{obj.id}',
                data=json.dumps({
                    "id": obj.id,
                    "name": obj.facility_name,
                    "state": obj.state,
                    "district": obj.District,
                    "pincode": obj.pincode,
                    "longitude": obj.longitude,
                    "latitude": obj.latitude,
                    "facility_status": "stopped",
                    "address": obj.Address,
                    "is_deleted": obj.is_deleted,
                }),headers=headers)
            return HttpResponseRedirect(".")
        elif "apply0" in request.POST:
            id = obj.id
            self.model.objects.filter(id=id).update(facility_status="running")
            headers = {"Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjEzNzUwOTIwMDA1LCJpYXQiOjE2NTQ5MjAwMDUsIm1vYmlsZSI6IjExMjIzMzQ0NTUifQ.AmsZIvdFsld_KzvufGMiB2YtdE67ezjnFOYFdVvZtqU"}    
            r = requests.put(f'http://128.199.139.248:6969/getFacilityById/{obj.id}',
                    data=json.dumps({
                        "id": obj.id,
                        "name": obj.facility_name,
                        "state": obj.state,
                        "district": obj.District,
                        "pincode": obj.pincode,
                        "longitude": obj.longitude,
                        "latitude": obj.latitude,
                        "facility_status": "running",
                        "address": obj.Address,
                        "is_deleted": obj.is_deleted,
                    }),headers=headers)
            self.message_user(request, "FACILITY HAS BEEN INITIATED")
            self.message_user(request, "FACILITY RESTARTED")
            Facility_to_service.objects.filter(
                facility=id).update(status='runnig')
            return redirect('/admin/IT_ADMIN/facilityrunningproxy/')
        return super().response_change(request, obj)

    # function used to check if html passes button to stop or restart facility
    def change_view(self, request, object_id, form_url='', extra_context=None):
        check_facility = Facility.objects.filter(id=object_id)
        check_status = check_facility[0].facility_status
        extra_context = extra_context or {}
        extra_context['status'] = check_status
        return super().change_view(request, object_id, form_url, extra_context=extra_context)


# //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// custom user


class Custom_UserInline(NestedStackedInline):  # user basic details are entered
    model = Custom_User
    exclude = ('is_deleted',)
    min_num = 1
    readonly_fields = ['is_deleted', ]
    fields = ('user', 'role',)
    verbose_name = 'Custom User'  # object name here
    verbose_name_plural = 'Custom User'
    form = ProductForm

    def has_delete_permission(self, request, obj=None):
        return False

    def get_fields(self, request, obj=None):
        if obj:
            c = obj.id
            qr = Custom_User.objects.get(user=c)
            h = qr.role
            self.qn = Custom_User.objects.values_list(
                'Facilities', flat=True).filter(role=h)
            self.ls = Custom_User.objects.values_list(
                'Facilities', flat=True).filter(user=c)
            return self.fields + ('Facilities',)
        return self.fields

    def formfield_for_manytomany(self, db_field, request=None, **kwargs):
        if db_field.name == 'Facilities':
            list1 = list(self.qn)
            # print(list1, 'list111111111111')
            list2 = list(Facility.objects.values_list('id', flat=True))
            # print(list2, "list2222222222")
            list3 = list(self.ls)
            list2 = list(Facility.objects.values_list('id', flat=True))
            for element in list1:
                if element in list2:
                    list2.remove(element)
            for i in list2:
                list3.append(i)
            kwargs["queryset"] = Facility.objects.filter(id__in=list3)
        return super(Custom_UserInline, self).formfield_for_manytomany(db_field, request, **kwargs)

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return self.readonly_fields + ['role', ]
        return self.readonly_fields


class CustomUserproxy(User):  # To create a copy of users table
    class Meta:
        proxy = True
        verbose_name_plural = 'Add Users'
        verbose_name = 'Users'


@ admin.register(CustomUserproxy)
# To modify copy of users table with our customuser table
class CustomUserAdmin(UserAdmin):
    list_display = ('username', 'email', 'first_name',
                    'last_name', 'is_active', 'date_joined', 'is_staff')
    # readonly_fields = ('role',)
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        ('Personal info', {'fields': ('first_name', 'last_name', 'email')}),
        # ('Important dates', {'fields': ('last_login', 'date_joined')}),
        ('Permissions', {'fields': ('is_active', 'is_staff', 'groups')}),
    )
    readonly_fields = ['groups', ]
    inlines = (Custom_UserInline, )
    list_per_page = 10

    def get_queryset(self, request):
        queryset = super(CustomUserAdmin, self).get_queryset(request)
        us = Custom_User.objects.all().values_list('user', flat=True)
        return queryset.filter(id__in=us)

    def has_delete_permission(self, request, obj=None):
        return False

    def change_view(self, request, object_id, form_url='', extra_context=None):
        user1 = User.objects.get(id=object_id)
        Customrole = Custom_User.objects.get(user=user1)
        role1 = Customrole.role
        user1.groups.add(role1)
        return super(CustomUserAdmin, self).change_view(request, object_id, form_url, extra_context=extra_context)


admin.site.unregister(User)

class package_Form(forms.ModelForm):
    amount = forms.IntegerField(min_value=1)
    months = forms.IntegerField(min_value=1)

class Package_to_service(admin.StackedInline):
    exclude = ('is_deleted',)
    verbose_name = "Package to service"
    verbose_name_plural = "Package to service"
    model = Package_to_service
    extra = 0
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'service':
            kwargs['queryset'] = Service.objects.filter(is_deleted=False)
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


@ admin.register(Packages)
class Packages_Admin(admin.ModelAdmin):
    exclude = ('is_deleted',)
    form = package_Form
    inlines = [Package_to_service, ]
    # change_form_template = "packages.html"
    search_fields = ['package_name', ]
    list_per_page = 10
    # def save_model(self, request, obj, file, change):
    #     if '_save' in request.POST:
    #         save_as = False
    #     super().save_model(request, obj, file, change)
        # pass
        # if request.POST.get('vehicle_image_id'):
        #     img = request.POST.get('vehicle_image_id')
        #     out1 = cloudinary.uploader.upload(img)
        #     objres1 = out1['url']
        #     data_save = Carwash.objects.get(id=obj.id)
        #     data_save.image = objres1
        #     data_save.save()


@ admin.register(Banner)
class BannerAdmin(admin.ModelAdmin):
    change_form_template = "banner.html"
    list_display = ('name',)
    fields = [('name'), ('photo'), ('banner_image_id'),('service_name'),]
    search_fields = ['name', ]
    # actions = [delete_action]
    form = Banner_Form
    list_per_page = 10

    # def has_delete_permission(self, request, obj=None):
    #     return False

    def get_queryset(self, request):
        queryset = super(BannerAdmin, self).get_queryset(request)
        return queryset.filter(is_deleted=False)

    # def add_view(self, request, form_url='', extra_context=None):
    #     if request.method == 'GET':
    #         object_passed = request.GET.getlist('data')
    #         if len(object_passed) != 0:
    #             list_tuple = Banner.objects.values_list(
    #                 'name').filter(is_deleted=False)
    #             list_banner = [i[0] for i in list_tuple]
    #             if object_passed[0] in list_banner:
    #                 return JsonResponse({"message": "Successfully published", 'status': 200})
    #     return super(BannerAdmin, self).add_view(request, form_url, extra_context=extra_context)

    def save_model(self, request, obj, file, change):
        super().save_model(request, obj, file, change)
        if request.POST.get('banner_image_id'):
            img = request.POST.get('banner_image_id')
            out1 = cloudinary.uploader.upload(img)
            objres1 = out1['url']
            data_save = Banner.objects.get(id=obj.id)
            data_save.photo = objres1
            data_save.save()

    def change_view(self, request, object_id, form_url='', extra_context=None, **kwargs):
        extra_context = extra_context or {}
        extra_context['banner_img'] = Banner.objects.get(id=object_id)
        return super().change_view(request, object_id, form_url,  extra_context=extra_context, **kwargs)


@admin.register(Carwash)
class CarwashAdmin(admin.ModelAdmin):
    change_form_template = "carwash.html"
    list_filter = ['vehicle_type',]
    list_display = ('vehicle', 'vehicle_type')
    fields = [('vehicle'), ('vehicle_type'), ('image'), ('vehicle_image_id')]
    # actions = [delete_action]
    search_fields = ['vehicle', 'vehicle_type', ]
    form = vehicle_Form
    list_per_page = 10

    # def has_delete_permission(self, request, obj=None):
    #     return False

    def get_queryset(self, request):
        queryset = super(CarwashAdmin, self).get_queryset(request)
        return queryset.filter(is_deleted=False)

    def save_model(self, request, obj, file, change):
        super().save_model(request, obj, file, change)
        if request.POST.get('vehicle_image_id'):
            img = request.POST.get('vehicle_image_id')
            out1 = cloudinary.uploader.upload(img)
            objres1 = out1['url']
            data_save = Carwash.objects.get(id=obj.id)
            data_save.image = objres1
            data_save.save()

    def change_view(self, request, object_id, form_url='', extra_context=None, **kwargs):
        extra_context = extra_context or {}
        extra_context['vehicle_img'] = Carwash.objects.get(id=object_id)
        return super().change_view(request, object_id, form_url,  extra_context=extra_context, **kwargs)

    def add_view(self, request, form_url='', extra_context=None):
        print(request.GET,"................................request.GET")
        if request.GET.get('data'):
            data=request.GET.get('data')
            is_exist=Carwash.objects.filter(is_deleted=False,vehicle__iexact=data).exists()
            if is_exist==True:
                return JsonResponse({"result": 0})
            else:
                return JsonResponse({"result": 1})
        return super().add_view(request, form_url, extra_context)


@admin.register(Speciality)
class SpecialityAdmin(admin.ModelAdmin):
    change_form_template = "speciality.html"
    list_display = ('title', 'description')
    fields = [('title'), ('description'), ('image'), ('speciality_image_id'),('service_name'),]
    search_fields = ['title',]
    # actions = [delete_action]
    form = speciality_Form

    # def has_delete_permission(self, request, obj=None):
    #     return False

    def get_queryset(self, request):
        queryset = super(SpecialityAdmin, self).get_queryset(request)
        return queryset.filter(is_deleted=False)

    def save_model(self, request, obj, file, change):
        super().save_model(request, obj, file, change)
        if request.POST.get('speciality_image_id'):
            img = request.POST.get('speciality_image_id')
            out1 = cloudinary.uploader.upload(img)
            objres1 = out1['url']
            data_save = Speciality.objects.get(id=obj.id)
            data_save.image = objres1
            data_save.save()

    def change_view(self, request, object_id, form_url='', extra_context=None, **kwargs):
        extra_context = extra_context or {}
        extra_context['speciality_img'] = Speciality.objects.get(id=object_id)
        return super().change_view(request, object_id, form_url,  extra_context=extra_context, **kwargs)

