/**
 * Create a map with a marker.
 * Creating or dragging the marker sets the latitude and longitude values
 * in the input fields.
 */

 ;(function($) {

  // We'll insert the map after this element:
  var prev_el_selector = '.form-row.field-latitude';

  // The input elements we'll put lat/lon into and use
  // to set the map's initial lat/lon.
  var lat_input_selector = '#id_latitude',
      lon_input_selector = '#id_longitude';
      //  District_input_selector = '#id_District';

  // If we don't have a lat/lon in the input fields,
  // this is where the map will be centered initially.
  var initial_lat = 10.8505,
      initial_lon = 76.2711;

  // Initial zoom level for the map.
  var initial_zoom = 10;

  // Initial zoom level if input fields have a location.
  var initial_with_loc_zoom = 20;

  // Global variables. Nice.
  var map, marker, $lat, $lon;
  let markers = [];

  /**
   * Create HTML elements, display map, set up event listeners.
   */
  function initMap() {
    var $prevEl = $(prev_el_selector);

    if ($prevEl.length === 0) {
      // Can't find where to put the map.
      return;
    };

    $lat = $(lat_input_selector);
    $lon = $(lon_input_selector);
    // $addrs = $( District_input_selector);

    var has_initial_loc = ($lat.val() && $lon.val());

    if (has_initial_loc) {
      // There is lat/lon in the fields, so centre the map on that.
      initial_lat = parseFloat($lat.val());
      initial_lon = parseFloat($lon.val());
      initial_zoom = initial_with_loc_zoom;
    };

    $prevEl.after( $('<div class="js-setloc-map setloc-map"></div><input id="pac-input" style="height: 32px; margin-top: 11px; width: 350px;" class="controls" type="text" placeholder="Search Box">') );

    var mapEl = document.getElementsByClassName('js-setloc-map')[0];

    map = new google.maps.Map(mapEl, {
      zoom: initial_zoom,
      center: {lat: initial_lat, lng: initial_lon}
    });

    var input = document.getElementById('pac-input');
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.bindTo('bounds', map);

    // Create but don't position the marker:
    marker = new google.maps.Marker({
      map: map,
      draggable: true,
    });
    
    var infowindow = new google.maps.InfoWindow();
    autocomplete.addListener('place_changed', function(){
      infowindow.close(); 
      marker.setVisible(false);
      var place = autocomplete.getPlace();
      if (!place.geometry){
          window.alert("Select a value from the dropdown list");
          return;
      }
      if (place.geometry.viewport){
          map.fitBounds(place.geometry.viewport);
      } else {
          map.setCenter(place.geometry.location);
          map.setZoom(12);
      }
      marker.setIcon(({
          url: place.icon,
          size: new google.maps.Size(71, 71),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(17, 34),
          scaledSize: new google.maps.Size(35, 35)
      }));
      marker.setPosition(place.geometry.location);
      marker.setVisible(true);

      var address = '';
      if (place.address_components){
          address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
          ].join(' ');
      }

      infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
      infowindow.open(map, marker);

      for (var i = 0; i < place.address_components.length; i++){
          if(place.address_components[i].types[0] == 'postal_code'){
            // alert('yessss');
              console.log(place.address_components[i].long_name);
              document.getElementById('id_pincode').value = place.address_components[i].long_name;
          }  
      }
      for (var i = 0; i < place.address_components.length; i++){
        if(place.address_components[i].types[0] == "administrative_area_level_2"){
            console.log(place.address_components[i].long_name);
            document.getElementById('id_District').value = place.address_components[i].long_name;
        }  
      }
      for (var i = 0; i < place.address_components.length; i++){
        if(place.address_components[i].types[0] == "administrative_area_level_1"){
            console.log(place.address_components[i].long_name);
            document.getElementById('id_state').value = place.address_components[i].long_name;
        }  
      }
      document.getElementById('id_longitude').value = place.geometry.location.lng();
      document.getElementById('id_latitude').value = place.geometry.location.lat();
      document.getElementById('id_Address').innerHTML = place.formatted_address;
      console.log(place);
      // document.getElementById("id_District").value = place.address_components[1].long_name;
      // document.getElementById("id_state").value = place.address_components[2].long_name;

  });



    if (has_initial_loc) {
      // There is lat/lon in the fields, so centre the marker on that.
      setMarkerPosition(initial_lat, initial_lon);
    };

    google.maps.event.addListener(map, 'click', function(ev) {
      markers.push(marker)
      setMarkerPosition(ev.latLng.lat(), ev.latLng.lng());
      console.log(ev)
      const geocoder = new google.maps.Geocoder();
      geocodeLatLng(geocoder, map,ev.latLng.lat(),ev.latLng.lng())
    });

    google.maps.event.addListener(marker, 'dragend', function() {
      setInputValues(marker.getPosition().lat(), marker.getPosition().lng());
    });
  };

  /**
   * Re-position marker and set input values.
   */
  function setMarkerPosition(lat, lon,) {
    marker.setPosition({lat: lat, lng: lon});
    setInputValues(lat, lon,);
  };

  /**
   * Set both lat and lon input values.
   */
  function setInputValues(lat, lon) {
    setInputValue($lat, lat,);
    setInputValue($lon, lon,);
  };

  /**
   * Set the value of $input to val, with the correct decimal places.
   * We work out decimal places using the <input>'s step value, if any.
   */
  function setInputValue($input, val) {
    // step should be like "0.000001".
    var step = $input.prop('step');
    var dec_places = 0;

    if (step) {
      if (step.split('.').length == 2) {
        dec_places = step.split('.')[1].length;
      };

      val = val.toFixed(dec_places);
    };

    $input.val(val);
  };
  function geocodeLatLng(geocoder, map,lat,lng) {
    
    const latlng = {
      lat: parseFloat(lat),
      lng: parseFloat(lng),
    };
    geocoder.geocode({ location: latlng }, (results, status) => {
      if (status === "OK") {
        if (results[0]) {
          map.setZoom(11);
          markers.forEach((marker) => {
          marker.setMap(null);
          // console.log(markers)
          });
          markers = [];
          marker = new google.maps.Marker({
            position: latlng,
            map: map,
          });
          markers.push(marker)
          // console.log(results);
          //  console.log(results[1].formatted_address)
          //  console.log(marker)
          for(i=0; i < results.length; i++){
            for(var j=0;j < results[i].address_components.length; j++){
                for(var k=0; k < results[i].address_components[j].types.length; k++){
                    if(results[i].address_components[j].types[k] == "postal_code"){
                        zipcode = results[i].address_components[j].short_name;
                    }
                }
            }
    }
    // console.log(zipcode);
    // console.log(results,'results.........');
          document.getElementById("id_pincode").value =zipcode;
          document.getElementById("id_District").value = results[0].address_components[2].long_name;
          document.getElementById("id_Address").value = results[1].formatted_address;
          //  document.getElementById('id_District').val = results[1].formatted_address;
          // infowindow.setContent(results[0].formatted_address);
        //  infowindow.open(map, marker);
        } else {
          window.alert("No results found");
        }
      } else {
        window.alert("Geocoder failed due to: " + status);
      }
    });
  }
  $(document).ready(function(){
    initMap();
  });

})
(django.jQuery);


