
var c;
var galleryImagesContainer = document.getElementById('galleryImages');
var imageCropFileInput = document.getElementById('id_image');
var cropperImageInitCanvas = document.getElementById('cropperImg');
var cropImageButton = document.getElementById('cropImageBtn');
// Crop Function On change
  function imagesPreview(input) {
    console.log(galleryImagesContainer,"...........................................................ss")
    console.log(imageCropFileInput,"...........................................................imageCropFileInputss")

    var cropper;
    galleryImagesContainer.innerHTML = '';
    var img = [];
    if(cropperImageInitCanvas.cropper){
      cropperImageInitCanvas.cropper.destroy();
      cropImageButton.style.display = 'none';
      cropperImageInitCanvas.width = 0;
      cropperImageInitCanvas.height = 0;
    }
    if (input.files.length) {
      var i = 0;
      var index = 0;
      for (let singleFile of input.files) {
        var reader = new FileReader();
        reader.onload = function(event) {
          var blobUrl = event.target.result;
          img.push(new Image());
          img[i].onload = function(e) {
            // Canvas Container
            var singleCanvasImageContainer = document.createElement('div');
            singleCanvasImageContainer.id = 'singleImageCanvasContainer'+index;
            singleCanvasImageContainer.className = 'singleImageCanvasContainer';
            // Canvas Close Btn
            var singleCanvasImageCloseBtn = document.createElement('button');
            var singleCanvasImageCloseBtnText = document.createTextNode('Close');
            singleCanvasImageCloseBtn.id = 'singleImageCanvasCloseBtn'+index;
            singleCanvasImageCloseBtn.className = 'singleImageCanvasCloseBtn';
            singleCanvasImageCloseBtn.onclick = function() { removeSingleCanvas(this) };
            singleCanvasImageCloseBtn.appendChild(singleCanvasImageCloseBtnText);
            singleCanvasImageContainer.appendChild(singleCanvasImageCloseBtn);
            // Image Canvas
            var canvas = document.createElement('canvas');
            canvas.id = 'imageCanvas'+index;
            canvas.className = 'imageCanvas singleImageCanvas';
            canvas.width = e.currentTarget.width;
            canvas.height = e.currentTarget.height;
            canvas.onclick = function() { cropInit(canvas.id); };
            singleCanvasImageContainer.appendChild(canvas)
            // Canvas Context
            var ctx = canvas.getContext('2d');
            ctx.drawImage(e.currentTarget,0,0);
            galleryImagesContainer.appendChild(singleCanvasImageContainer);
            while (document.querySelectorAll('.singleImageCanvas').length == input.files.length) {
              var allCanvasImages = document.querySelectorAll('.singleImageCanvas')[0].getAttribute('id');
              cropInit(allCanvasImages);
              break;
            };
            urlConversion();
            index++;
          };
          img[i].src = blobUrl;
          i++;
        }
        reader.readAsDataURL(singleFile);
      }
    }
  }
  imageCropFileInput.addEventListener("change", function(event){
    imagesPreview(event.target);
  });
// Initialize Cropper
  function cropInit(selector) {
    c = document.getElementById(selector);

    if(cropperImageInitCanvas.cropper){
        cropperImageInitCanvas.cropper.destroy();
    }
    var allCloseButtons = document.querySelectorAll('.singleImageCanvasCloseBtn');
    for (let element of allCloseButtons) {
      element.style.display = 'block';
    }
    c.previousSibling.style.display = 'none';
    // c.id = croppedImg;
    var ctx=c.getContext('2d');
    var imgData=ctx.getImageData(0, 0, c.width, c.height);
    var image = cropperImageInitCanvas;
    image.width = c.width;
    image.height = c.height;
    var ctx = image.getContext('2d');
    ctx.putImageData(imgData,0,0);
    cropper = new Cropper(image, {
      aspectRatio: 1 / 1,
      preview: '.img-preview',
      crop: function(event) {
        cropImageButton.style.display = 'block';
      }
    });

  }
// Crop Image
  function image_crop() {
    if(cropperImageInitCanvas.cropper){
      var cropcanvas = cropperImageInitCanvas.cropper.getCroppedCanvas({width: 250, height: 250});
      var ctx=cropcanvas.getContext('2d');
        var imgData=ctx.getImageData(0, 0, cropcanvas.width, cropcanvas.height);
        c.width = cropcanvas.width;
        c.height = cropcanvas.height;
        var ctx = c.getContext('2d');
        ctx.putImageData(imgData,0,0);
        cropperImageInitCanvas.cropper.destroy();
        cropperImageInitCanvas.width = 0;
        cropperImageInitCanvas.height = 0;
        cropImageButton.style.display = 'none';
        var allCloseButtons = document.querySelectorAll('.singleImageCanvasCloseBtn');
        for (let element of allCloseButtons) {
          element.style.display = 'block';
        }
        urlConversion();
    } else {
      alert('Please select any Image you want to crop');
    }
  }
  cropImageButton.addEventListener("click", function(){
    image_crop();
  });
// Image Close/Remove
  function removeSingleCanvas(selector) {
    selector.parentNode.remove();
    urlConversion();
  }

// Get Converted Url
  function urlConversion() {
    var allImageCanvas = document.querySelectorAll('.singleImageCanvas');
    var convertedUrl = '';
    for (let element of allImageCanvas) {
      convertedUrl += element.toDataURL('image/jpeg');
      convertedUrl += 'img_url';
    }
    document.getElementById('id_image_id').value = convertedUrl;
  }




    var temp;
    var imageCropFileInput;

  function load_getUserListByGroupID(elem){
    var element_id=elem.id
    var element_name=elem.name
    
    var galleryImagesContainer1 = document.createElement('div');
    galleryImagesContainer1.id = element_id+'_g';
    var cropperImageInitCanvas2=document.createElement('canvas');
    cropperImageInitCanvas2.id=element_id+'_c';
    var img_div = element_name.replace('-image','');
 
    var b=document.getElementById('can')
    var a=document.getElementById(img_div)
    b.appendChild(cropperImageInitCanvas2);
    a.appendChild(galleryImagesContainer1);
    var imageCropFileInput;
  
  var img_id=element_id
  var img_taf=img_id.concat("_taf");
  imageCropFileInput = document.getElementById(element_id);
  temp=document.getElementById(element_id).value;
  var c;
  var galleryImagesContainer = document.getElementById(element_id+'_g');
  var cropperImageInitCanvas1 = document.getElementById(element_id+'_c');
  var cropImageButton = document.getElementById('cropImageBtn1');
  
  imageCropFileInput.addEventListener("change", function(event){
    console.log(event.target.files,".........................................event.target.files")
    const format_lst=['jpg','jpeg','png','PNG','JPEG','JPG']
    for (let singleFile of event.target.files) {
      var nh= singleFile['name'].split('.')[1].pop()

        if (format_lst.includes(nh)== false){
          $("#facility_form").prepend('<p  class="errornote">Unsupported file extension, supported formates(jpg,jppeg,png..)</p>') 
          // continue; 

    }
  }
    imagesPreview2(event.target);
    elem.value='';
  });
    cropImageButton.addEventListener("click", function(){
      image_crop();
    });
  
  
    function imagesPreview2(input) {
      const format_lst=['jpg','jpeg','png']
      var cropper;
      galleryImagesContainer.innerHTML = '';
      var img = [];
      if(cropperImageInitCanvas1.cropper){
  
        cropperImageInitCanvas1.cropper.destroy();
        cropImageButton.style.display = 'none';
        cropperImageInitCanvas1.width = 0;
        cropperImageInitCanvas1.height = 0;
      }
      if (input.files.length) {
        var i = 0;
        var index = 100;
        for (let singleFile of input.files) {
          // var nh= singleFile['name'].split('.')[1]
          // if (format_lst.includes(nh)== false){
          //   alert("Unsupported file extension, supported formates()")
          //   continue; 
          // }
          // console.log(singleFile['name'].split('.')[1],"..............singleFilesingleFile###")

          var reader = new FileReader();
          reader.onload = function(event) {
  
            var blobUrl = event.target.result;
            img.push(new Image());
            img[i].onload = function(e) {
              var singleCanvasImageContainer = document.createElement('div');
              singleCanvasImageContainer.id = 'singleImageCanvasContainer'+index;
              singleCanvasImageContainer.className = 'singleImageCanvasContainer';
              var singleCanvasImageCloseBtn = document.createElement('button');
              var singleCanvasImageCloseBtnText = document.createTextNode('Close');
              singleCanvasImageCloseBtn.id = 'singleImageCanvasCloseBtn'+index;
              singleCanvasImageCloseBtn.className = 'singleImageCanvasCloseBtn';
              singleCanvasImageCloseBtn.onclick = function() { removeSingleCanvas(this) };
              singleCanvasImageCloseBtn.appendChild(singleCanvasImageCloseBtnText);
              singleCanvasImageContainer.appendChild(singleCanvasImageCloseBtn);
              var canvas = document.createElement('canvas');
              canvas.id = elem.id+index;
              canvas.className = 'imageCanvas singleImageCanvas1'+img_taf;
              canvas.width = e.currentTarget.width;
              canvas.height = e.currentTarget.height;
              canvas.onclick = function() { cropInit(canvas.id); };
              singleCanvasImageContainer.appendChild(canvas)
              var ctx = canvas.getContext('2d');
              ctx.drawImage(e.currentTarget,0,0);
              galleryImagesContainer.appendChild(singleCanvasImageContainer);

              while (document.querySelectorAll('.singleImageCanvas1'+img_taf).length == input.files.length) {
                var allCanvasImages = document.querySelectorAll('.singleImageCanvas1'+img_taf)[0].getAttribute('id');
                cropInit(allCanvasImages);
                break;
              };
              urlConversion1();
              index++;
            };
            img[i].src = blobUrl;
            i++;
          }
          reader.readAsDataURL(singleFile);
        }
      }
    }
    function cropInit(selector) {
      c = document.getElementById(selector);
    
      if(cropperImageInitCanvas1.cropper){
          cropperImageInitCanvas1.cropper.destroy();
      }
      var allCloseButtons = document.querySelectorAll('.singleImageCanvasCloseBtn');
      for (let element of allCloseButtons) {
        element.style.display = 'block';
      }
      c.previousSibling.style.display = 'none';
      var ctx=c.getContext('2d');
      var imgData=ctx.getImageData(0, 0, c.width, c.height);
      var image = cropperImageInitCanvas1;
      image.width = c.width;
      image.height = c.height;
      var ctx = image.getContext('2d');
      ctx.putImageData(imgData,0,0);
      cropper = new Cropper(image, {
        aspectRatio: 1 / 1,
        preview: '.img-preview2',
        crop: function(event) {
          cropImageButton.style.display = 'block';
        }
      });
  
    }
    function image_crop() {
      if(cropperImageInitCanvas1.cropper){
        var cropcanvas = cropperImageInitCanvas1.cropper.getCroppedCanvas({width: 250, height: 250});
        var ctx=cropcanvas.getContext('2d');
          var imgData=ctx.getImageData(0, 0, cropcanvas.width, cropcanvas.height);
          c.width = cropcanvas.width;
          c.height = cropcanvas.height;
          var ctx = c.getContext('2d');
          ctx.putImageData(imgData,0,0);
          cropperImageInitCanvas1.cropper.destroy();
          cropperImageInitCanvas1.width = 0;
          cropperImageInitCanvas1.height = 0;
          cropImageButton.style.display = 'none';
          var allCloseButtons = document.querySelectorAll('.singleImageCanvasCloseBtn');
          for (let element of allCloseButtons) {
            element.style.display = 'block';
          }
          urlConversion1();
      } else {
        console.log("else")
      }
    }
  
    function removeSingleCanvas(selector) {
      selector.parentNode.remove();
      urlConversion1();
    }
    function urlConversion1() {
      var allImageCanvas = document.querySelectorAll('.singleImageCanvas1'+img_taf);
      var convertedUrl1 = '';
      for (let element of allImageCanvas) {
        convertedUrl1 += element.toDataURL('image/jpeg');
        convertedUrl1 += 'img_url';
      }
      
      document.getElementById(img_taf).value = convertedUrl1;
    }
  }
  