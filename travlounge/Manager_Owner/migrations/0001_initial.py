# Generated by Django 3.2.12 on 2022-08-18 05:28

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('BILLING', '__first__'),
        ('IT_ADMIN', '0001_initial'),
        ('HR', '0001_initial'),
        ('INCIDENT_MANAGEMENT', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Breafing',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('time_of_completion', models.TimeField(auto_now_add=True)),
                ('breafing_full_attendence', models.BooleanField(default=False)),
                ('breafing_note', models.TextField(max_length=500)),
            ],
            options={
                'ordering': ['-created_on'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Events',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('title', models.CharField(max_length=50)),
                ('description', models.CharField(max_length=100)),
                ('mark_as_read', models.BooleanField(default=False)),
                ('time_of_completion', models.TimeField(auto_now_add=True)),
                ('date', models.DateField(null=True)),
            ],
            options={
                'ordering': ['-created_on'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Manager_reports',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('report_date', models.DateField(auto_now_add=True)),
                ('status', models.BooleanField(default=False)),
                ('approval_status', models.CharField(choices=[('pending', 'pending'), ('approved', 'approved'), ('disapproved', 'disapproved')], default='Pending', max_length=12)),
                ('manager_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='HR.employee')),
                ('shift_name', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='HR.shift_type')),
            ],
            options={
                'ordering': ['-created_on'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='report_data',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('report_type', models.CharField(choices=[(0, 'attendence'), (1, 'cleanliness'), (2, 'stock')], max_length=12)),
                ('marking', models.BooleanField(default=False)),
                ('time_of_approval', models.TimeField(auto_now=True)),
                ('facility_to_service_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='IT_ADMIN.facility_to_service')),
                ('incident_reported', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='INCIDENT_MANAGEMENT.incident_initiate')),
                ('report_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='Manager_Owner.manager_reports')),
                ('shift', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='HR.shift_type')),
            ],
            options={
                'ordering': ['-created_on'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Log_table',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('log_role', models.CharField(max_length=200)),
                ('action', models.CharField(max_length=200)),
                ('user_type', models.CharField(choices=[('User', 'User'), ('Employee', 'Employee')], max_length=20)),
                ('time', models.DateTimeField(auto_now_add=True)),
                ('action_by', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='action_by', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='incident_report',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('title', models.CharField(max_length=20)),
                ('description', models.CharField(max_length=50)),
                ('priority', models.CharField(choices=[('1', 'URGENT'), ('2', 'HIGH'), ('3', 'MEDIUM'), ('4', 'LOW')], max_length=50)),
                ('counter', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='BILLING.counter')),
                ('facilityservice', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='IT_ADMIN.facility_to_service')),
                ('initiator_id', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='HR.employee')),
            ],
            options={
                'ordering': ['-created_on'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Employee_change_req',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('request_date', models.DateField(auto_now_add=True)),
                ('reason', models.CharField(max_length=100)),
                ('employee', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='HR.employee')),
                ('owner_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['-created_on'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Breafing_services',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('rating', models.IntegerField()),
                ('remark_type', models.BooleanField(default=True)),
                ('remark', models.TextField(max_length=500)),
                ('breafing_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='Manager_Owner.breafing')),
                ('employee_remarks', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='HR.employee')),
                ('facility_to_service_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='IT_ADMIN.facility_to_service')),
            ],
            options={
                'ordering': ['-created_on'],
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='breafing',
            name='report_id',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='Manager_Owner.manager_reports'),
        ),
    ]
