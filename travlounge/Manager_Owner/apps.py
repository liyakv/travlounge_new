from django.apps import AppConfig


class ManagerOwnerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'Manager_Owner'
