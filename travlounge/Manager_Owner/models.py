from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from IT_ADMIN.models import Facility, BaseModel, Facility_to_service,Service
from INCIDENT_MANAGEMENT.models import Incident_Initiate
from HR.models import Employee,Shift_type
from BILLING.models import Counter

# Create your models here.


# Facility Manager ----------------------------------------------------------------------------------------------------------------------------------------------

APPROVAL = (
    ("pending", "pending"),
    ("approved", "approved"),
    ("disapproved", "disapproved"),
)

TYPE = (
    (0, "attendence"),
    (1, "cleanliness"),
    (2, "stock"),
)


class Manager_reports(BaseModel):
    # forign key from employee login (Facility Manager)
    manager_id = models.ForeignKey(Employee, on_delete=models.CASCADE)
    report_date = models.DateField(auto_now_add=True)
    # if shift is ongoing or not , True if shift is completed
    status = models.BooleanField(default=False)
    approval_status = models.CharField(
        max_length=12, default="Pending", choices=APPROVAL)
    shift_name = models.ForeignKey(Shift_type, on_delete=models.CASCADE)


class report_data(BaseModel):
    report_id = models.ForeignKey(Manager_reports, on_delete=models.CASCADE)
    # 1,2,3 etc for each inspection cycle
    shift = models.ForeignKey(Shift_type,on_delete = models.CASCADE) 
    report_type = models.CharField(max_length=12, choices=TYPE)
    facility_to_service_id = models.ForeignKey(Facility_to_service, on_delete=models.CASCADE)
    # true when facility manager marks approval
    marking = models.BooleanField(default=False)
    time_of_approval = models.TimeField(auto_now=True)
    incident_reported = models.ForeignKey(Incident_Initiate, on_delete=models.CASCADE, null=True)


# class cleanliness(BaseModel):
#     report_data_id=models.ForeignKey(report_data, on_delete=models.CASCADE)
#     service_id=models.ForeignKey(Service, on_delete=models.CASCADE)
#     status=models.BooleanField(default=False)
#     user_id=models.ForeignKey(Employee, on_delete=models.CASCADE)


# class report_data(BaseModel):
#     report_id = models.ForeignKey(Manager_reports,on_delete = models.CASCADE)
#     shift = models.ForeignKey(Shift_type,on_delete = models.CASCADE) 
#     report_type = models.CharField(max_length=12,choices=TYPE)
#     facility_to_service_id = models.ForeignKey(Facility_to_service,on_delete=models.CASCADE)
#     marking = models.BooleanField(default=False) #true when facility manager marks approval
#     time_of_approval = models.TimeField(auto_now_add=True)
#     incident_reported = models.ForeignKey(Incident_initiate,on_delete=models.CASCADE,null = True)


class Breafing(BaseModel):
    report_id = models.ForeignKey(Manager_reports, on_delete=models.CASCADE)
    time_of_completion = models.TimeField(auto_now_add=True)
    breafing_full_attendence = models.BooleanField(
        default=False)  # true if checked
    breafing_note = models.TextField(max_length=500)


class Breafing_services(BaseModel):
    breafing_id = models.ForeignKey(Breafing, on_delete=models.CASCADE)
    # forign key that mapped a facility to a service
    facility_to_service_id = models.ForeignKey(
        Facility_to_service, on_delete=models.CASCADE)
    rating = models.IntegerField()
    # forign key from employee login table
    employee_remarks = models.ForeignKey(Employee, on_delete=models.CASCADE)
    remark_type = models.BooleanField(
        default=True)  # if remark positive or not
    remark = models.TextField(max_length=500)


class Events(BaseModel):
    title = models.CharField(max_length=50)
    description = models.CharField(max_length=100)
    mark_as_read = models.BooleanField(default=False)
    time_of_completion = models.TimeField(auto_now_add=True)
    date = models.DateField(null=True)


# //////////////////////////////////////////////////// Facility owner ////////////////////////////////////////////////////

class Employee_change_req(BaseModel):
    # forign key from employee table
    employee = models.ForeignKey(Employee, on_delete=models.CASCADE)
    # forign key from user table
    owner_id = models.ForeignKey(User, on_delete=models.CASCADE)
    request_date = models.DateField(auto_now_add=True)
    reason = models.CharField(max_length=100)


# ///////////////////////////////////////////////// logtable ////////////////////////////////////////////////////

TYPE_CHOICES = (("User", "User"), ("Employee", "Employee"))


class Log_table(models.Model):
    log_role = models.CharField(max_length=200)
    action = models.CharField(max_length=200)
    user_type = models.CharField(max_length=20, choices=TYPE_CHOICES)
    action_by = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="action_by", null=True)
    time = models.DateTimeField(auto_now_add=True)

# /////////////////////////////////////////////////////////////////////////////////////////////////


INCIDENT_PRIORITY = (
    ("1", "URGENT"),
    ("2", "HIGH"),
    ("3", "MEDIUM"),
    ("4", "LOW")
)


class incident_report(BaseModel):
    counter = models.ForeignKey(
        Counter, on_delete=models.CASCADE, null=True)
    facilityservice = models.ForeignKey(
        Facility_to_service, on_delete=models.CASCADE, null=True)
    title = models.CharField(max_length=20)
    description = models.CharField(max_length=50)
    priority = models.CharField(max_length=50, choices=INCIDENT_PRIORITY)
    initiator_id = models.ForeignKey(
        Employee, on_delete=models.CASCADE, null=True)
