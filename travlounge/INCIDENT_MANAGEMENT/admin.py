from django.contrib import admin
from django.http import HttpResponseRedirect
# Register your models here.
from django.contrib import admin
from .models import Incident_Initiate, Incident_Tickets, Incident_documents, Incident_Closing, Incident_Assignment
from IT_ADMIN.models import Custom_User
from HR.models import Employee
from django.urls import reverse
from django.shortcuts import redirect, render
from django import forms
from django.db.models import Q
from django.contrib.admin import SimpleListFilter
from django.utils.translation import ugettext_lazy as _
from django.contrib import messages

# -----------------------To Initiate Incident created by facility manager--------------------------------------

# class Incident_InitiateAdmin(admin.ModelAdmin):
#     model = Incident_Initiate
#     exclude = ('is_deleted','incident_status')
#     def get_model_perms(self, request):
#         return {}
# admin.site.register(Incident_Initiate,Incident_InitiateAdmin)

# -----------------------To Initiate Incident proxy created by facility manager---------------------------------


class Incident_InitiateProxy(Incident_Initiate):
    class Meta:
        proxy = True
        verbose_name = 'Incident Initiate list'  # object name here
        verbose_name_plural = 'Incident Initiate list'  # plural object names here


@admin.register(Incident_InitiateProxy)
class Incident_InitiateviewProxy(admin.ModelAdmin):
    exclude = ('is_deleted', 'incident_status')
    list_display = ('title', 'initiated_at', 'priority', 'initiator_id',)
    list_filter = (
        ('priority'),
    )
    list_per_page = 20
    change_form_template = "incidentinitiate.html"

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj=None):
        return False

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        p = request.user.id
        c1 = list(Custom_User.objects.filter(
            user=p).values_list("Facilities__id", flat=True))
        return queryset.filter(incident_status=False).filter(initiator_id__facility_to_service__facility__in=c1)

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        context.update({
            'show_save_and_continue': False,
            'show_save': False,
        })
        return super().render_change_form(request, context, add, change, form_url, obj)

    def response_change(self, request, obj):
        if "apply2" in request.POST:
            id = obj.id
            request.session['tickets'] = id
            return redirect('/admin/INCIDENT_MANAGEMENT/incident_tickets/add')
        return super().response_change(request, obj)


class Incident_Tickets_Form(forms.ModelForm):
    class Meta:
        model = Incident_Tickets
        fields = "__all__"

    def __init__(self, *args, **kwargs):
        super(Incident_Tickets_Form, self).__init__(*args, **kwargs)
        self.fields['attachments'] = forms.FileField(
            widget=forms.ClearableFileInput(attrs={'multiple': True}),
        )


# class Incident_TicketsAdmin(admin.ModelAdmin):
#     form = Incident_Tickets_Form
#     exclude = ('is_deleted','status')
#     def get_model_perms(self, request):
#         return {}
#     def save_model(self, request, obj, form, change):
#         for afile in request.FILES.getlist('image_id'):
#             data_save=Photos_facility(facility_id=obj,photo=afile)
#             data_save.save()
# admin.site.register(Incident_Tickets,Incident_TicketsAdmin)

class Incident_TicketsAdmin(admin.ModelAdmin):
    # model = Incident_Tickets
    form = Incident_Tickets_Form
    exclude = ('is_deleted', 'status', 'incident_id',)

    def get_model_perms(self, request):
        return {}

    def save_model(self, request, obj, form, change):
        id2 = request.session['tickets']
        incid = Incident_Initiate.objects.get(id=id2)
        print(incid.id, "idddddddddddddddddddddddddddddddddddddddd")
        obj.incident_id_id = incid.id
        id = obj.incident_id.id
        obj.status = 1
        print(obj.status)
        obj.save()
        del request.session['tickets']
        # ////////////////////////////////////////////////////////////////////
        # if request.FILES:
        #     for afile in request.FILES.getlist('attachments'):
        #         data_save=Incident_documents(Incident_Tickets_id=obj,Docs=afile)
        #         data_save.save()
        # /////////////////////////////////////////////////////////////////////////

        print("updateddddddddddddddddd")
        Incident_Initiate.objects.filter(id=id).update(incident_status=True)
        self.message_user(request, "Incident add as Ticket")

    def response_add(self, request, obj):
        return redirect('/admin/INCIDENT_MANAGEMENT/incident_ticketsproxy')


admin.site.register(Incident_Tickets, Incident_TicketsAdmin)


# -----------------------To Initiate Incident proxy created by facility manager---------------------------------
# MAKING A CUSTOM FILTER TO ONLY SHOW RUNNING AND STOPPED FACILITIES
class RunningFilter(admin.SimpleListFilter):
    title = _('Ticket status')
    parameter_name = 'status'

    def lookups(self, request, model_admin):
        return (
            (1, "pending"),
            (3, "assigned"),
            (4, "re_assigned"),
        )

    def queryset(self, request, queryset):
        if self.value() is None:
            self.used_parameters[self.parameter_name] = 2
            return queryset.filter(Q(status='1') | Q(status='3') | Q(status='4'))
        else:
            self.used_parameters[self.parameter_name] = int(self.value())
        if self.value() == 1:
            return queryset.filter(status="1")
        elif self.value() == 3:
            return queryset.filter(status="3")
        else:
            return queryset.filter(status="4")


class Incident_TicketsProxy(Incident_Tickets):
    class Meta:
        proxy = True
        verbose_name = 'Incident ticket list'  # object name here
        verbose_name_plural = 'Incident ticket list'  # plural object names here


@admin.register(Incident_TicketsProxy)
class Incident_TicketsviewProxy(admin.ModelAdmin):
    exclude = ('is_deleted', 'status')

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        p = request.user.id
        our_user = list(Custom_User.objects.filter(
            user=p).values_list("Facilities__id", flat=True))
        return queryset.filter(is_deleted=False).filter(incident_id__initiator_id__facility_to_service__facility__in=our_user)

    list_filter = (RunningFilter,)
    list_per_page = 20
    list_display = ('incident_id', 'status', 'work_progress',)

    def work_progress(self, obj):
        z = obj.id
        late = Incident_Assignment.objects.filter(
            ticket_id=z).latest('created_on')
        asgsts = late.assignee_status
        print(asgsts)
        p = obj.status
        if p == "1":
            obj.work_progress = "Not assigned"
            return obj.work_progress
        elif p == "3":
            if asgsts == False:
                obj.work_progress = "Progress"
                return obj.work_progress
            else:
                obj.work_progress = "complete"
                return obj.work_progress
        else:
            if asgsts == False:
                obj.work_progress = "Progress"
                return obj.work_progress
            else:
                obj.work_progress = "complete"
                return obj.work_progress

    #     return obj
    change_form_template = "incidentticket.html"

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj=None):
        return False
# def get_queryset(self, request):
    #     queryset = super().get_queryset(request)
    #     return queryset.filter(incident_status=False)

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        context.update({
            'show_save_and_continue': False,
            'show_save': False,
        })
        return super().render_change_form(request, context, add, change, form_url, obj)

    def response_change(self, request, obj, post_url_continue=None):
        if "apply0" in request.POST:
            id = obj.id
            request.session['apply0'] = id
            return redirect('/admin/INCIDENT_MANAGEMENT/incident_assignment/add')
    
        if "apply3" in request.POST:
            id = obj.id
            request.session['apply3'] = id
            return redirect('/admin/INCIDENT_MANAGEMENT/incident_assignment/add')
            
        if "apply4" in request.POST:
            id = obj.id
            # ////////////////////////////////////////////////////////////////////////////////
            ticket = Incident_Tickets.objects.get(id=id)
            x = Incident_Assignment.objects.filter(
                ticket_id=ticket.id).latest('created_on')
            # ////////////////////////////////////////////////////////////////////////////////
            print(x.assignee_status, "xxxxxxxxxxxxxxxxxxxxxxxxxx")
            assignsts = x.assignee_status
            if assignsts == False:
                messages.error(
                    request, "Ticket is not possible to close, because this ticket is not complete")
                return HttpResponseRedirect("../")
            else:
                request.session['close'] = id
                return redirect('/admin/INCIDENT_MANAGEMENT/incident_closing/add')
        return super().response_change(request, obj)

    # function used to check if html passes button to stop or restart facility
    def change_view(self, request, object_id, form_url='', extra_context=None):
        check_facility = Incident_Tickets.objects.filter(id=object_id)
        check_status = check_facility[0].status
        p = str(check_status)
        print(p)
        extra_context = extra_context or {}
        extra_context['status'] = p
        # docum = Incident_Tickets.objects.filter(id=object_id)
        return super().change_view(request, object_id, form_url, extra_context=extra_context)

# //////////////////////////////////////////////////////////////////////////////////////////////////


class Incident_AssignmentAdmin(admin.ModelAdmin):
    model = Incident_Assignment
    # exclude = ('is_deleted',)
    exclude = ('is_deleted', 'ticket_id', 'assign_status', 'assignee_status',)

    def get_model_perms(self, request):
        return {}

    def save_model(self, request, obj, form, change):
        if 'apply0' in request.session:
            id1=request.session['apply0']
        elif 'apply3' in request.session:
            id1=request.session['apply3']
        else:
            id1=request.session['reopen']

        ticket = Incident_Tickets.objects.get(id=id1)
        # ///////////////////////////////////////////////////////////////////////////////////////////////
        try:
            x = Incident_Assignment.objects.filter(
                ticket_id=ticket.id).latest('created_on')
            y = x.id
            Incident_Assignment.objects.filter(
                id=y).update(assignee_status=True)
        except:
            pass

        # /////////////////////////////////////////////////////////////////////////////////////////////////
        obj.ticket_id_id = ticket.id
        if 'apply0' in request.session:
            obj.assign_status = "1"
            del request.session['apply0']
        if 'apply3' in request.session:            
            obj.assign_status = "2"
            del request.session['apply3']
        if 'reopen' in request.session:
            obj.assign_status = "2"
            del request.session['reopen']

        obj.save()
        id2 = obj.ticket_id.id
        tkt = Incident_Tickets.objects.get(id=id2)
        sts = tkt.status
        if(sts == "3" or "4" or "2"):
            Incident_Tickets.objects.filter(id=id2).update(status="4")
        if(sts == "1"):
            Incident_Tickets.objects.filter(id=id2).update(status="3")

    def response_add(self, request, obj):
        return redirect('/admin/INCIDENT_MANAGEMENT/incident_ticketsproxy')


admin.site.register(Incident_Assignment, Incident_AssignmentAdmin)

# //////////////////////////////////////////////////////////////////////////////////////////////////


class Incident_ClosingAdmin(admin.ModelAdmin):
    model = Incident_Closing
    exclude = ('is_deleted', 'ticket_id',)

    def get_model_perms(self, request):
        return {}
    # def formfield_for_foreignkey(self, db_field, request, **kwargs):
    #     if db_field.name == "closed_by":
    #         id=request.user.id
    #         our_user = Custom_User.objects.filter(user=id).values_list("Facilities")
    #         # Custom_User.objects.filter()
    #         print(our_user,"kkkkkkkkkkkkkkkkkkkkkkkk")
    #         kwargs["queryset"] = Custom_User.objects.filter(Facilities__in=our_user)
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def save_model(self, request, obj, form, change):
        id0 = request.session['close']
        clstkt = Incident_Tickets.objects.get(id=id0)
        obj.ticket_id_id = clstkt.id
        obj.save()
        del request.session['close']
        id = obj.ticket_id.id
        Incident_Tickets.objects.filter(id=id).update(status="2")
        self.message_user(request, "Ticket Closed")

    def response_add(self, request, obj):
        return redirect('/admin/INCIDENT_MANAGEMENT/incident_closingproxy')


admin.site.register(Incident_Closing, Incident_ClosingAdmin)


class Incident_ClosingProxy(Incident_Closing):
    class Meta:
        proxy = True
        verbose_name = 'Incident closing list'  # object name here
        verbose_name_plural = 'Incident closing list'  # plural object names here


@admin.register(Incident_ClosingProxy)
class Incident_ClosingviewProxy(admin.ModelAdmin):
    exclude = ('is_deleted',)
    # list_filter = (
    #     ('status'),
    # )
    list_display = ('ticket_id', 'closed_by', 'closed_at',)
    list_per_page = 20
    change_form_template = "incidentclose.html"

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj=None):
        return False

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        p = request.user.id
        our_user = list(Custom_User.objects.filter(
            user=p).values_list("Facilities__id", flat=True))
        return queryset.filter(is_deleted=False).filter(ticket_id__incident_id__initiator_id__facility_to_service__facility__in=our_user)

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        context.update({
            'show_save_and_continue': False,
            'show_save': False,
        })
        return super().render_change_form(request, context, add, change, form_url, obj)

    def response_change(self, request, obj):
        if "reopen" in request.POST:
            id = obj.ticket_id.id
            request.session['reopen'] = id
            Incident_Tickets.objects.filter(id=id).update(status="4")
            self.message_user(request, "Ticket Reopened")
            return redirect('/admin/INCIDENT_MANAGEMENT/incident_assignment/add')
        return super().response_change(request, obj)


# ////////////////////////////////////////////////////////////////////////////////////////////////////////


class Incident_AssignmentProxy(Incident_Assignment):
    class Meta:
        proxy = True
        verbose_name = 'Incident assignment history'  # object name here
        verbose_name_plural = 'Incident assignment history'  # plural object names here


@admin.register(Incident_AssignmentProxy)
class Incident_AssignmentviewProxy(admin.ModelAdmin):
    exclude = ('is_deleted', 'assignee_status',)

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        p = request.user.id
        our_user = list(Custom_User.objects.filter(
            user=p).values_list("Facilities__id", flat=True))
        return queryset.filter(is_deleted=False).filter(ticket_id__incident_id__initiator_id__facility_to_service__facility__in=our_user)
    list_display = ('ticket_id', 'assign_date', 'assign_status', 'assign_to',)
    list_per_page = 20
    list_filter = (
        ('assign_to'),
    )

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj=None):
        return False

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        context.update({
            'show_save_and_continue': False,
            'show_save': False,
        })
        return super().render_change_form(request, context, add, change, form_url, obj)