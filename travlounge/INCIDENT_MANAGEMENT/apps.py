from django.apps import AppConfig


class IncidentManagementConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'INCIDENT_MANAGEMENT'
    verbose_name="Incident Management"
