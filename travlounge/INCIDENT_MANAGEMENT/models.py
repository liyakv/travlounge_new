from django.db import models

# Create your models here.
from django.db import models
from django.db.models.fields import BooleanField
from IT_ADMIN.models import Custom_User, Facility, BaseModel, Facility_to_service
from HR.models import Employee
from django.contrib.auth.models import User, Group
import cloudinary
from cloudinary.models import CloudinaryField

# ------------------------------------------------------------INCIDENT_MANAGEMENT-------------------------------------------------------------------------------------------------------------
INCIDENT_PRIORITY = (
    ("URGENT", "URGENT"),
    ("HIGH", "HIGH"),
    ("MEDIUM", "MEDIUM"),
    ("LOW", "LOW")
)


class Incident_Initiate(BaseModel):
    title = models.CharField(max_length=50)
    description = models.CharField(max_length=100, null=True)
    # initiated_at=models.DateTimeField(null=True)
    initiated_at = models.DateTimeField(auto_now_add=True, null=True)
    priority = models.CharField(max_length=50, choices=INCIDENT_PRIORITY)
    # employee id of the employee who raise the incident
    initiator_id = models.ForeignKey(
        Employee, on_delete=models.CASCADE, related_name='initiator_id')
    incident_status = BooleanField(default=False)
    # requister_id= models.ForeignKey(User,on_delete=models.CASCADE, related_name='requister_id')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Incident Initiate'
        verbose_name_plural = 'Incident Initiate'

# ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


INCIDENT_STATUS = (
    ("1", "pending"),
    ("2", "closed"),
    ("3", "assigned"),
    ("4", "re_assigned"),
)


class Incident_Tickets(BaseModel):
    incident_id = models.ForeignKey(
        Incident_Initiate, on_delete=models.CASCADE, verbose_name='Ticket incident')
    incident_type = models.CharField(max_length=50)
    attachments = CloudinaryField('Incident_Tickets')
    note = models.TextField(max_length=300)
    status = models.CharField(max_length=50, choices=INCIDENT_STATUS)

    def __str__(self):
        return self.incident_id.title

    class Meta:
        verbose_name = 'Incident Ticket'
        verbose_name_plural = 'Incident Tickets'


class Incident_documents(BaseModel):
    Incident_Tickets_id = models.ForeignKey(
        Incident_Tickets, on_delete=models.CASCADE, null=True)
    Docs = CloudinaryField('Docs/Incident_documents')

    class Meta:
        verbose_name = 'Incident Document '
        verbose_name_plural = 'Incident Documents'

# //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


ASSIGN_STATUS = (
    ("1", "assigned"),
    ("2", "re_assigned"),
)
ASSIGN_TO_CHOICES = (
    ("HR", "HR"),
    ("CRM", "CRM"),
)


class Incident_Assignment(BaseModel):
    ticket_id = models.ForeignKey(Incident_Tickets, on_delete=models.CASCADE)
    assign_date = models.DateTimeField(auto_now=True)
    assign_status = models.CharField(max_length=50, choices=ASSIGN_STATUS)
    # assign_to=models.ForeignKey(Employee,on_delete=models.CASCADE,related_name='assign_to')
    assign_to = models.CharField(max_length=5, choices=ASSIGN_TO_CHOICES)
    assignee_status = BooleanField(default=False)  # if completed is True

    def __str__(self):
        return self.ticket_id.incident_id.title

    class Meta:
        verbose_name = 'Incident Assignment '
        verbose_name_plural = 'Incident Assignment'


class Incident_Closing(BaseModel):
    ticket_id = models.ForeignKey(Incident_Tickets, on_delete=models.CASCADE)
    closed_by = models.ForeignKey(Custom_User, on_delete=models.CASCADE)
    closed_at = models.DateTimeField(null=True, blank=True)
    reason = models.CharField(max_length=50)
    note1 = models.CharField(max_length=50)
    note2 = models.CharField(max_length=50)

    def __str__(self):
        return self.ticket_id.incident_id.title

    class Meta:
        verbose_name = 'Incident Closing '
        verbose_name_plural = 'Incident Closing'
