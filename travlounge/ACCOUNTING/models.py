from django.db import models
from IT_ADMIN.models import Facility, BaseModel, Facility_to_service
from WAREHOUSE.models import Vendor
from django.contrib.auth.models import User

# Create your models here.


# Accounting ---------------------------------------------------------------------------------------------------------------------------------------------------

class company(BaseModel):
    facility_to_service_id = models.ForeignKey(
        Facility_to_service, on_delete=models.CASCADE, blank=True, null=True)
    facility = models.ForeignKey(
        Facility, on_delete=models.CASCADE, blank=True, null=True)
    total_opening_balace_cr = models.FloatField(
        null=True, blank=True, default=0.0)
    total_opening_balace_db = models.FloatField(
        null=True, blank=True, default=0.0)
    finacil_year = models.DateField(null=True, blank=True)
    book_year = models.DateField(null=True, blank=True)


Under = (
    ("Asset", "Asset"),
    ("Liability", "Liability"),
    ("Income", "income"),
    ("Expense", "Expense")
)


class primary_group(BaseModel):
    name = models.CharField(max_length=60)
    alias = models.CharField(max_length=60)
    under = models.CharField(max_length=20, choices=Under)

    def __str__(self):
        return self.name


class primary_and_company(BaseModel):
    company = models.ForeignKey(
        company, on_delete=models.CASCADE, blank=True, null=True)
    primary = models.ForeignKey(
        primary_group, on_delete=models.CASCADE, blank=True, null=True)


Under = (
    ("not applicable", "Asset"),
    ("appropriate by qty", "appropriate by qty"),
    ("appropriate by value", "appropriate by value")
)


class Main_group(BaseModel):
    group_name = models.CharField(max_length=60)
    behave_ledger = models.BooleanField()  # if behave as sub ledger or not
    nett_debit_cerdit_balance = models.BooleanField()
    used_for_calculation = models.BooleanField()
    purchase_in_voice_method = models.CharField(max_length=20, choices=Under)
    under = models.ForeignKey(primary_and_company, on_delete=models.CASCADE)


Regitration_typ = (("Unknown", "Unknown"), ("Regular", "Regular"), ("Consumer",
                   "Consumer"), ("composition", "composition"), ("Unregistered", "Unregistered"))
modes = (("Credit", "Credit"), ("Debit", "Debit"))


class Main_ledger(BaseModel):
    ledger_name = models.CharField(max_length=60)
    alias = models.CharField(max_length=60)
    opening_bal = models.DecimalField(max_digits=6, decimal_places=2)
    mode = models.CharField(max_length=20, choices=modes)
    description = models.TextField(max_length=500)
    bank = models.BooleanField()
    pan_it_num = models.CharField(max_length=20, blank=True, null=True)
    gstin_uin = models.CharField(max_length=20, blank=True, null=True)
    registration_typ = models.CharField(
        max_length=20, choices=Regitration_typ, blank=True, null=True)
    under_p = models.ForeignKey(
        Main_group, on_delete=models.CASCADE, blank=True, null=True)
    under_m = models.ForeignKey(
        primary_and_company, on_delete=models.CASCADE, blank=True, null=True)
    vendor_name = models.ForeignKey(
        Vendor, on_delete=models.CASCADE, blank=True, null=True)


Gst_typ = (("Applicable", "Applicable"), ("Not Applicable",
           "Not Applicable"), ("Undefined", "Undefined"))
Type_of_supply = (("Goods", "Goods"), ("Services", "Services"))


class Ledger_satutary_details(BaseModel):
    main_ledger = models.ForeignKey(
        Main_ledger, on_delete=models.CASCADE, blank=True, null=True)
    is_gst_aplicable = models.CharField(
        max_length=20, choices=Gst_typ, blank=True, null=True)
    set_alter_gst_details = models.BooleanField()
    type_of_supply = models.CharField(
        max_length=20, choices=Type_of_supply, blank=True, null=True)


Typbank = (("Cheque", "Cheque"), ("Others", "Others"),
           ("e fund transfer", "e fund transfer"))


class Ledger_banking_details(BaseModel):
    main_ledger = models.ForeignKey(
        Main_ledger, on_delete=models.CASCADE, blank=True, null=True)
    transaction_typ = models.CharField(
        max_length=50, choices=Typbank, blank=True, null=True)
    cross_using = models.CharField(max_length=50, blank=True, null=True)
    account_num = models.CharField(max_length=50, blank=True, null=True)
    bank = models.CharField(max_length=50, blank=True, null=True)
    bank_name = models.CharField(max_length=50, blank=True, null=True)


class Voucher(BaseModel):
    v_type = (
        ("Payment", "Payment"),
        ("Receipt", "Receipt"),
        ("Contra", "Contra"),
        ("Journal", "Journal"),
    )
    voucher_type = models.CharField(max_length=12,  # define if tag shows under best worst or normal
                                    choices=v_type,
                                    default=False)
    under = models.ForeignKey(Main_ledger, on_delete=models.CASCADE)
    note = models.TextField(max_length=500)
    date = models.DateField(auto_now_add=True)


class Particulars(models.Model):
    voucher = models.ForeignKey(Voucher, on_delete=models.CASCADE)
    amount = models.DecimalField(max_digits=6, decimal_places=2)
    main_ledger = models.ForeignKey(
        Main_ledger, on_delete=models.CASCADE, blank=True, null=True)
    Total = models.DecimalField(max_digits=6, decimal_places=2)
    date = models.DateField()

# class items(models.Model):
#     voucher = models.ForeignKey(Voucher,on_delete = models.CASCADE)
#     name_item = models.CharField(max_length=50,blank=True,null=True)
#     amount = models.DecimalField(max_digits=6, decimal_places=2)
#     rate_per_amount = models.DecimalField(max_digits=6, decimal_places=2)


class Log_table_accounting(models.Model):
    company = models.ForeignKey(company, on_delete=models.CASCADE)
    task = models.CharField(max_length=200)
    task_by = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    time = models.DateTimeField(auto_now_add=True)
