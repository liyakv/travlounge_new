from typing import Counter
from django.db import models
# from django.core.validators import FileExtensionValidator
from IT_ADMIN.models import BaseModel, Facility, Facility_to_service, Service_type, Service,FacilitySleepingPod
# from STOCK_MANAGEMENT.models import InternalStock
from CORPORATE.models import Service_payment
from HR.models import Employee
from datetime import datetime
from django.utils import timezone
from django.core.validators import MaxValueValidator


# from django.core.validators import MaxValueValidator
# from HR.models import Employee



# Create your models here.
#


class Counter(BaseModel):
    facility_id = models.ForeignKey(Facility, on_delete=models.CASCADE, unique=True)
    counter_number=models.IntegerField()


class Cash_Collection(BaseModel):  # always updated during cash inflow and widrawal
    counter = models.ForeignKey(
        Counter, on_delete=models.CASCADE, unique=True)
    current_cash_in_counter = models.FloatField()
    current_cash_in_bank = models.FloatField()


class Cash_clearing(BaseModel):  # added each row for each widrawal
    counter = models.ForeignKey(
        Counter, on_delete=models.CASCADE)
    date_time = models.DateTimeField(auto_now_add=True)
    cash_in_counter = models.FloatField(null=True)
    cash_widrawal_amount = models.FloatField()
    note = models.CharField(max_length=30, null=True)


class log(BaseModel):
    counter = models.ForeignKey(
        Counter, related_name="counter_r", on_delete=models.CASCADE)
    logged_by = models.ForeignKey(
        Employee, related_name="logged_r", on_delete=models.CASCADE)
    login_date_time = models.CharField(max_length=30,null=True)
    logout_date_time = models.CharField(max_length=30,null=True)
    opening_balance = models.FloatField(null=True,default=0)
    closing_balance = models.FloatField(null=True,default=0)
    is_verified= models.BooleanField(default=False)



class Bill(BaseModel):
    counter = models.ForeignKey(
        Counter, on_delete=models.CASCADE)
    customer_name = models.CharField(max_length=30, null=True)
    customer_phone = models.CharField(max_length=30, null=True)
    total_bill_amount = models.FloatField()
    date_time = models.DateTimeField()
    # true if its through Bank , false if via cash
    mode_of_payment = models.BooleanField(default=False)


class Bill_items(BaseModel):
    bill = models.ForeignKey(Bill, on_delete=models.CASCADE)
    service = models.ForeignKey(Service, on_delete=models.CASCADE)
    no_of_tickets = models.PositiveIntegerField(default=1)
    price = models.FloatField()


mode_choice = (
    ("bank", "bank"),
    ("inhand", "inhand")
)


class Bill_refund(BaseModel):
    counter = models.ForeignKey(
        Counter, on_delete=models.CASCADE)
    reason = models.CharField(max_length=50, null=True)
    mode_of_payment = models.CharField(max_length=50, choices=mode_choice)
    bill_item_id = models.ForeignKey(Bill_items, on_delete=models.CASCADE)
    service = models.ForeignKey(Service, on_delete=models.CASCADE)
    no_of_tickets = models.PositiveIntegerField()


# class Bill_products(BaseModel):
#     bill_id = models.ForeignKey(Bill, on_delete=models.CASCADE)
#     product_id = models.ForeignKey(InternalStock, on_delete=models.CASCADE)
#     quantity = models.PositiveIntegerField(default=0)



class ExtUsers(BaseModel):
    customer_id = models.IntegerField(primary_key=True)
    username = models.CharField(max_length=60,null=True,blank=True)
    # phone_number = models.BigIntegerField(unique=True, validators=[MaxValueValidator(9999999999)])
    phone_number = models.CharField(max_length=15)
    image = models.ImageField(null=True,blank=True)
    is_registered = models.BooleanField(default=False)



class TolooBooking(BaseModel):
    user_id = models.ForeignKey(ExtUsers,on_delete=models.CASCADE)
    emp_id = models.ForeignKey(Employee, on_delete=models.CASCADE)
    # is_booked= models.BooleanField(default=False)

class SleepingPodBooking(BaseModel):
    sleep_id = models.IntegerField(primary_key=True)
    start_time = models.DateTimeField(default=datetime.now)
    end_time = models.DateTimeField(default=datetime.now)
    emp_id = models.ForeignKey(Employee, on_delete=models.CASCADE,null=True,blank=True)
    user_id = models.ForeignKey(ExtUsers, on_delete=models.CASCADE)
    facility_sleeping_id =  models.ManyToManyField(FacilitySleepingPod)

# class Podbook(BaseModel):
#     sleeping_pod = models.ForeignKey(SleepingPodBooking,on_delete=models.CASCADE)
#     fac_pod = models.ForeignKey(FacilitySleepingPod,on_delete=models.CASCADE)
#     is_booked= models.BooleanField(default=False)



class ServiceBooking(BaseModel):
    selected_type = models.ForeignKey(Service, on_delete=models.CASCADE)
    pod_bookingtable_id = models.ForeignKey(SleepingPodBooking, on_delete=models.CASCADE,null=True,blank=True)
    toloo_bookingtable_id = models.ForeignKey(TolooBooking, on_delete=models.CASCADE,null=True,blank=True)
    user_id = models.ForeignKey(ExtUsers,on_delete=models.CASCADE)
    quatity =  models.IntegerField(default=0)
    rate=models.ForeignKey(Service_payment,on_delete=models.CASCADE)
    amount= models.FloatField(default=0)
    counter = models.ForeignKey(Counter, on_delete=models.CASCADE)
    is_booked= models.BooleanField(default=False)


