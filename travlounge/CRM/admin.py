# from IT_ADMIN.models import Custom_User, Facility
# from django.contrib import admin
# from django import forms
# from CRM.models import Tags, Report
# from django_summernote.admin import SummernoteModelAdmin
# from django_summernote.models import Attachment
# from INCIDENT_MANAGEMENT.models import Incident_Assignment, Incident_Tickets


# class SummerAdmin(SummernoteModelAdmin):
#     summernote_fields = "content"
#     exclude = ('is_deleted', 'created_by')
#     list_per_page = 10


# class facility_Form(forms.ModelForm):
#     class Meta:
#         model = Report
#         fields = "__all__"


# admin.site.register(Report, SummerAdmin)
# admin.site.unregister(Attachment)


# class TagAdmin(admin.ModelAdmin):
#     list_display = ('facility', 'tag_name', 'tag_type')
#     list_filter = ('facility', 'tag_name', 'tag_type')
#     exclude = ('is_deleted', 'created_by')
#     list_per_page = 10

#     def get_queryset(self, request):
#         queryset = super().get_queryset(request)
#         our_user = list(Custom_User.objects.filter(
#             user=request.user.id).values_list("Facilities", flat=True))
#         return queryset.filter(is_deleted=False).filter(facility__in=our_user)

#     def formfield_for_foreignkey(self, db_field, request, **kwargs):
#         if db_field.name == "facility":
#             our_user = list(Custom_User.objects.filter(
#                 user=request.user.id).values_list("Facilities", flat=True))
#             kwargs["queryset"] = Facility.objects.filter(
#                 is_deleted=False).filter(id__in=our_user)
#         return super().formfield_for_foreignkey(db_field, request, **kwargs)

#     def save_model(self, request, obj, form, change):
#         obj.created_by = request.user
#         super(TagAdmin, self).save_model(request, obj, form, change)


# admin.site.register(Tags, TagAdmin)

# # from django.contrib import admin
# # from django import forms
# # from CRM.models import Tags,Report
# # from django_summernote.admin import SummernoteModelAdmin
# # from django_summernote.models import Attachment
# # from INCIDENT_MANAGEMENT.models import Incident_Assignment,Incident_Tickets


# # class SummerAdmin(SummernoteModelAdmin):
# #     summernote_fields="content"
# #     exclude=('is_deleted',)
# #     list_per_page=5
# #     list_filter=('created_on',)
# # class facility_Form(forms.ModelForm):
# #     class Meta:
# #         model = Report
# #         fields = "__all__"

# # admin.site.register(Report,SummerAdmin)
# # admin.site.unregister(Attachment)


# # class TagAdmin(admin.ModelAdmin):
# #     list_display = ('facility','tag_name','tag_type')
# #     list_filter=('facility','tag_name','tag_type')
# #     exclude=('is_deleted',)
# #     list_per_page=5
# # admin.site.register(Tags,TagAdmin)

# class IncidentViewProxy(Incident_Assignment):
#     class Meta:
#         proxy = True
#         verbose_name = 'Incidents reported'  # object name here
#         verbose_name_plural = 'Incidents'  # plural object names here


# @admin.register(IncidentViewProxy)
# class IncidentViewProxyAdmin(admin.ModelAdmin):
#     list_display = ('ticket_id', 'assign_date',)
#     list_per_page = 10
#     exclude = ('is_deleted', 'assign_to', 'assignee_status',)
#     readonly_fields = (('ticket_id'), ('assign_date'), ('assign_status'),)
#     change_form_template = "incidents.html"

#     def has_delete_permission(self, request, obj=None):
#         return False

#     def has_add_permission(self, request, obj=None):
#         return False

#     def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
#         context.update({
#             'show_save_and_continue': False,
#             'show_save': False,
#         })
#         return super().render_change_form(request, context, add, change, form_url, obj)

#     def get_queryset(self, request):
#         queryset = super().get_queryset(request)
#         return queryset.filter(assign_to='CRM').filter(is_deleted=False).filter(assignee_status=False)

#     def change_view(self, request, object_id, form_url='', extra_context=None):
#         ticket = Incident_Assignment.objects.filter(id=object_id).values("ticket_id__incident_id__title", "ticket_id__incident_id__description",
#                                                                          "ticket_id__incident_id__initiated_at", "ticket_id__incident_id__priority", "ticket_id__incident_id__initiator_id__first_name", "ticket_id__incident_id__initiator_id__work_title",
#                                                                          "ticket_id__incident_id__initiator_id__phonenumber", "ticket_id__incident_id__initiator_id__facility_to_service__facility__facility_name")
#         extra_context = extra_context or {}
#         extra_context['details'] = ticket
#         return super(IncidentViewProxyAdmin, self).change_view(request, object_id, form_url, extra_context=extra_context)

#     def response_change(self, request, obj):
#         Incident_Assignment.objects.filter(
#             id=obj.id).update(assignee_status=True)
#         return super().response_change(request, obj)
