# from django.contrib import admin
from IT_ADMIN.models import Facility, Facility_docs, Facility_to_service
from HR.models import Employee
from CORPORATE.models import Notifications, Mark_as_read, Service_payment
from django.shortcuts import render, redirect
from django.db import models as md
from django.urls import path
from django.db.models import Count
from IT_ADMIN.models import Custom_User
from django.http import HttpResponseRedirect
from HR.models import leave_request, Attendance, Assign_leave
from ACCOUNTING.models import company, Particulars
from django.db.models import Sum
from django.contrib import admin
from django import forms


# Register your models here.
# --------------------------------General settings---------------------------
class ServicePaymentForm(forms.ModelForm):
     amount =forms.DecimalField( min_value=1,
        widget=forms.NumberInput(attrs={'step': '0.1'}))
        
@admin.register(Service_payment)
class Service_paymentAdmin(admin.ModelAdmin):
    model = Service_payment
    exclude = ('is_deleted',)
    form= ServicePaymentForm
    list_per_page =10

# --------------------------------To View Facility Details----------------------------------


class FacilityDetailProxy(Facility):
    class Meta:
        proxy = True
        verbose_name = 'facility'
        verbose_name_plural = 'Facility Details'


@admin.register(FacilityDetailProxy)
class FacilityDetailProxyAdmin(admin.ModelAdmin):
    list_filter = (('facility_status'),)
    list_display = ('facility_name', 'facility_status',
                    'state', 'District', 'created_on',)
    exclude = ('is_deleted', 'sts', 'image_id')
    readonly_fields = [('facility_name'), ('facility_status'), ('state'),
                       ('District'), ('pincode'), ('longitude'), ('latitude'), ('Address'), ]
    change_form_template = "change_form4.html"
    list_per_page=10

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj=None):
        return False

    def change_view(self, request, object_id, form_url='', extra_context=None):
        check_docs = list(Facility_docs.objects.filter(facility_id=object_id))
        get_fs = list(Facility_to_service.objects.filter(
            facility=object_id).values_list('id', flat=True))
        check_emp = list(Employee.objects.filter(
            facility_to_service__in=get_fs))
        extra_context = extra_context or {}
        extra_context['documents'] = check_docs
        extra_context['employee'] = check_emp
        return super().change_view(request, object_id, form_url, extra_context=extra_context)

# ---------------------------------------Notifications----------------------------------------------------------


# class Mark_as_readAdmin(admin.StackedInline):
#     model = Mark_as_read
#     filter_horizontal = ('recipient',)
#     extra = 0
#     min_num = 1
#     max_num = 1
#     exclude = ('is_deleted', 'read_status',)
#     verbose_name = "SEND TO"

#     def has_change_permission(self, request, obj=None):
#         return False


# @admin.register(Notifications)
# class NotificationAdmin(admin.ModelAdmin):
#     model = Notifications
#     list_display = ('title', 'created_on')
#     exclude = ('is_deleted',)
#     inlines = (Mark_as_readAdmin,)

#     def has_change_permission(self, request, obj=None):
#         return False

# ----------------------------------------SUMMARY------------------------------------------------------------------


class SummaryModel(md.Model):  # empty dummy model
    class Meta:
        verbose_name_plural = 'Summary'
        app_label = 'CORPORATE'


def my_custom_view(request):
    counter = Facility.objects.values('facility_status').annotate(
        count=Count('pk')).order_by('count')
    emploees = Employee.objects.filter(is_deleted=False).values(
        'facility_to_service__facility__facility_name').annotate(count=Count('pk')).order_by('count')
    return render(request, 'summary.html', context={"all_facilities": counter, "all_employees": emploees})


class SummaryModelAdmin(admin.ModelAdmin):  # model to represent custom template
    model = SummaryModel

    def get_urls(self):
        view_name = '{}_{}_changelist'.format(
            self.model._meta.app_label, self.model._meta.model_name)
        return [
            path('my_admin_path/', my_custom_view, name=view_name),
        ]


admin.site.register(SummaryModel, SummaryModelAdmin)


# /////////////////////////////////////////////////////// HR Leave_request Approval ////////////////////////////////////////////

class HRLeave_requestProxy(leave_request):
    class Meta:
        proxy = True
        verbose_name = 'HR leave request Approval'  # object name here
        verbose_name_plural = 'HR leave request Approval'  # plural object names here


@admin.register(HRLeave_requestProxy)
class HRLeave_requestProxyAdmin(admin.ModelAdmin):
    list_display = ('leave_from_date', 'leave_to_date', 'Reason', 'status')
    fields = [('leave_from_date'), ('leave_to_date'), ('Reason'), ]
    list_per_page = 10
    change_form_template = "hrleave.html"

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        p = request.user.id
        # return queryset.filter(is_deleted=False).filter(ordinary_employee=False)
        return queryset.filter(is_deleted=False).filter(ordinary_employee=False).filter(status=2)

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj=None):
        return False

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        context.update({
            'show_save_and_continue': False,
            'show_save_and_add_another': False,
            'show_save': False,
        })
        return super().render_change_form(request, context, add, change, form_url, obj)

    def response_change(self, request, obj):
        if "test" in request.POST:
            obj.user = request.user
            id = obj.request_id
            reason = ""
            reasons = request.POST.get("test")
            self.model.objects.filter(request_id=id).update(
                reason_reject=reasons, status=0)
            # self.message_user(request, "Leave rejected")
        if "_make-unique" in request.POST:
            obj.user = request.user
            l = obj.user
            id = obj.request_id
            self.model.objects.filter(request_id=id).update(
                status=1, reason_reject="")
            # self.message_user(request, "Leave approved")
            # return HttpResponseRedirect("../")
        return super().response_change(request, obj)

# .................................................................starting of this code
# --------------------------------To View service wise profit and loss----------------------------------

# class ServiceprofitlossProxy(company):
#     class Meta:
#         proxy = True
#         verbose_name = 'Services'
#         verbose_name_plural = 'Service wise Profit and loss'


# @admin.register(ServiceprofitlossProxy)
# class ServiceprofitlossProxyAdmin(admin.ModelAdmin):
#     list_display = ('facility_to_service_id',)
#     fields = [('facility_to_service_id',)]
#     readonly_fields = [('facility_to_service_id')]
#     change_form_template = "serviceprofitloss.html"
#     list_per_page = 20
#     date_from = None
#     date_to = None

#     def get_queryset(self, request):
#         queryset = super().get_queryset(request)
#         return queryset.filter(facility__isnull=True)

#     def has_delete_permission(self, request, obj=None):
#         return False

#     def has_add_permission(self, request, obj=None):
#         return False

#     def response_change(self, request, obj):
#         if "test1" in request.POST or "test2" in request.POST:
#             from_date = request.POST["test1"]
#             to_date = request.POST["test2"]
#             if(from_date == "" or to_date == ""):
#                 self.date_from = None
#                 self.date_to = None
#                 return redirect(f'/admin/CORPORATE/serviceprofitlossproxy/{obj.id}/change/')
#             else:
#                 self.date_from = from_date
#                 self.date_to = to_date
#                 return redirect(f'/admin/CORPORATE/serviceprofitlossproxy/{obj.id}/change/')
#         return super().response_change(request, obj)

#     def change_view(self, request, object_id, form_url='', extra_context=None):
#         start_date = self.date_from
#         end_date = self.date_to
#         print(start_date, "dateeeeeeeeeeeeeeee")
#         if start_date == None or end_date == None:
#             extra_context = extra_context or {}
#             extra_context['facilityprofit'] = None
#             extra_context['facilityloss'] = None
#             fac = company.objects.get(id=object_id)
#             extra_context['facility'] = fac
#         else:
#             v1 = Particulars.objects.filter(
#                 voucher__under__under_m__company=object_id)
#             v2 = Particulars.objects.filter(
#                 voucher__under__under_p__under__company=object_id)
#             h = v1 | v2
#             print(h)
#             expense_m = Particulars.objects.filter(
#                 voucher__under__under_m__primary__under="Expense")
#             expense_p = Particulars.objects.filter(
#                 voucher__under__under_p__under__primary__under="Expense")
#             income_m = Particulars.objects.filter(
#                 voucher__under__under_m__primary__under="Income")
#             income_p = Particulars.objects.filter(
#                 voucher__under__under_p__under__primary__under="Income")
#             vouchersprofit = income_m | income_p
#             vouchersloss = expense_m | expense_p
#             profit = vouchersprofit.filter(date__range=(start_date, end_date))
#             vouchersprofitcom = profit.intersection(h)
#             loss = vouchersloss.filter(date__range=(start_date, end_date))
#             voucherslosscom = loss.intersection(h)
#             total1 = 0
#             total2 = 0
#             for instance in vouchersprofitcom:
#                 total1 += instance.Total
#             print(total1)
#             for instance in voucherslosscom:
#                 total2 += instance.Total
#             print(total2)
#             extra_context = extra_context or {}
#             extra_context['facilityprofit'] = total1
#             extra_context['facilityloss'] = total2
#             fac = company.objects.get(id=object_id)
#             extra_context['facility'] = fac
#             self.date_from = None
#             self.date_to = None
#         return super(ServiceprofitlossProxyAdmin, self).change_view(request, object_id, form_url, extra_context=extra_context)


# ......................................................................................end of this code


# --------------------------------To View Facility wise profit and loss----------------------------------


# class FacilityprofitlossProxy(company):
#     class Meta:
#         proxy = True
#         verbose_name = 'Facility'
#         verbose_name_plural = 'Facility wise Profit and loss'


# @admin.register(FacilityprofitlossProxy)
# class FacilityprofitlossProxyAdmin(admin.ModelAdmin):
#     list_display = ('facility',)
#     fields = [('facility',)]
#     # list_filter = (('facility_status'),)
#     readonly_fields = [('facility')]
#     change_form_template = "facilityprofitloss.html"
#     list_per_page = 20
#     date_from = None
#     date_to = None

#     def get_queryset(self, request):
#         queryset = super().get_queryset(request)
#         return queryset.filter(facility_to_service_id__isnull=True)

#     def has_delete_permission(self, request, obj=None):
#         return False

#     def has_add_permission(self, request, obj=None):
#         return False

#     def response_change(self, request, obj):
#         if "test1" in request.POST or "test2" in request.POST:
#             from_date = request.POST["test1"]
#             to_date = request.POST["test2"]
#             if(from_date == "" or to_date == ""):
#                 self.date_from = None
#                 self.date_to = None
#                 return redirect(f'/admin/CORPORATE/facilityprofitlossproxy/{obj.id}/change/')
#             else:
#                 self.date_from = from_date
#                 self.date_to = to_date
#                 return redirect(f'/admin/CORPORATE/facilityprofitlossproxy/{obj.id}/change/')
#         return super().response_change(request, obj)

#     def change_view(self, request, object_id, form_url='', extra_context=None):
#         start_date = self.date_from
#         end_date = self.date_to
#         print(start_date, "dateeeeeeeeeeeeeeee")
#         if start_date == None or end_date == None:
#             extra_context = extra_context or {}
#             extra_context['facilityprofit'] = None
#             extra_context['facilityloss'] = None
#             fac = company.objects.get(id=object_id)
#             extra_context['facility'] = fac
#         else:
#             v1 = Particulars.objects.filter(
#                 voucher__under__under_m__company=object_id)
#             v2 = Particulars.objects.filter(
#                 voucher__under__under_p__under__company=object_id)
#             h = v1 | v2
#             print(h)
#             expense_m = Particulars.objects.filter(
#                 voucher__under__under_m__primary__under="Expense")
#             expense_p = Particulars.objects.filter(
#                 voucher__under__under_p__under__primary__under="Expense")
#             income_m = Particulars.objects.filter(
#                 voucher__under__under_m__primary__under="Income")
#             income_p = Particulars.objects.filter(
#                 voucher__under__under_p__under__primary__under="Income")
#             vouchersprofit = income_m | income_p
#             vouchersloss = expense_m | expense_p
#             profit = vouchersprofit.filter(date__range=(start_date, end_date))
#             vouchersprofitcom = profit.intersection(h)
#             loss = vouchersloss.filter(date__range=(start_date, end_date))
#             voucherslosscom = loss.intersection(h)
#             total1 = 0
#             total2 = 0
#             for instance in vouchersprofitcom:
#                 total1 += instance.Total
#             print(total1)
#             for instance in voucherslosscom:
#                 total2 += instance.Total
#             print(total2)
#             extra_context = extra_context or {}
#             extra_context['facilityprofit'] = total1
#             extra_context['facilityloss'] = total2
#             fac = company.objects.get(id=object_id)
#             extra_context['facility'] = fac
#             self.date_from = None
#             self.date_to = None
#         return super(FacilityprofitlossProxyAdmin, self).change_view(request, object_id, form_url, extra_context=extra_context)
