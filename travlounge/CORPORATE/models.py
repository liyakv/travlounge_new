from django.db import models
from IT_ADMIN.models import BaseModel
from HR.models import Employee
from IT_ADMIN.models import Service
# Create your models here.


class Notifications(BaseModel):
    title = models.CharField(max_length=60)
    description = models.TextField(max_length=300)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Notification'
        verbose_name_plural = 'Notifications'


READ_STATUS = (("read", "read"),
               ("unread", "unread"),
               )


class Mark_as_read(BaseModel):
    Notification = models.ForeignKey(Notifications, on_delete=models.CASCADE)
    read_status = models.CharField(max_length=8, choices=READ_STATUS)
    recipient = models.ManyToManyField(Employee)


UNIT_CHOICES = (("per day", "per day"), ("per usage",
                "per usage"), ("per hour", "per hour"))


class Service_payment(BaseModel):
    service = models.ForeignKey(Service, on_delete=models.CASCADE)
    unit = models.CharField(
        max_length=20, default='running', choices=UNIT_CHOICES)
    amount = models.FloatField()

    def __str__(self):
        return self.service.service_name
    class Meta:
        verbose_name = 'Service payment'
        verbose_name_plural = 'Service payments'
