import firebase_admin
from firebase_admin import credentials, messaging
from IT_ADMIN .models import token_get,Custom_User
cred = credentials.Certificate("templates/admin/static/service.json")
firebase_admin.initialize_app(cred)
def sendPush(title, msg, registration_token, dataObject=None):
    print(registration_token,'in send push')
    print(msg)
    print(title)
    # See documentation on defining a message payload.
    message = messaging.MulticastMessage(
        notification=messaging.Notification(
            title=title,
            body=msg
        ),
        data=dataObject,
        tokens=registration_token,
    )
    # Send a message to the device corresponding to the provided
    # registration token.
    response = messaging.send_multicast(message)
    # Response is a message ID string.
    print('Successfully sent message:', response)
# def gettokens(request,role):
#     current_user = request.user
#     user_id=current_user.id
#     facilities = Custom_User.objects.value_list('Facilities').filter(user=user_id)