importScripts("https://www.gstatic.com/firebasejs/8.8.1/firebase-app.js");
importScripts(
    "https://www.gstatic.com/firebasejs/8.8.1/firebase-messaging.js",
);
importScripts(
    "https://www.gstatic.com/firebasejs/8.8.1/firebase-analytics.js",
);
firebase.initializeApp({
    messagingSenderId: "513942618301",
    apiKey: "AIzaSyAQr6t4B6eVlS-1gzWiE_acg7SGljt0Iu4",
    projectId: "inbound-guru-276002",
    appId: "1:513942618301:web:3f1b1ca1abc0feb5f4b7d4",
});
const messaging = firebase.messaging();
messaging.setBackgroundMessageHandler(function(payload) {
    console.log(
        "[firebase-messaging-sw.js] Received background message ",
        payload,
    );
    const notificationTitle = "Background Message Title";
    const notificationOptions = {
        body: "Background Message body.",
        icon: "/itwonders-web-logo.png",
    };
    return self.registration.showNotification(
        notificationTitle,
        notificationOptions,
    );
});