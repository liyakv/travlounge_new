# Generated by Django 3.2.12 on 2022-08-18 05:03

import cloudinary.models
import datetime
from django.conf import settings
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        # ('auth', '0013_role'),
        ('IT_ADMIN', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='GRN',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('date_recived', models.DateField()),
                ('note', models.TextField(max_length=300)),
                ('grn_bill', cloudinary.models.CloudinaryField(max_length=255, verbose_name='docs/GRN_bills')),
                ('created_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'GRN',
                'verbose_name_plural': 'GRN',
            },
        ),
        migrations.CreateModel(
            name='GRN_products',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('qty_recived', models.PositiveIntegerField()),
                ('clearance_status', models.BooleanField(default=False)),
            ],
            options={
                'verbose_name': "Products in GRN'",
            },
        ),
        migrations.CreateModel(
            name='GSN',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('send_date', models.DateField(default=datetime.date.today)),
                ('reciving_status', models.BooleanField(default=False)),
                ('recived_date', models.DateField(null=True)),
                ('bill', cloudinary.models.CloudinaryField(max_length=255, verbose_name='docs/GSN_bills')),
                ('created_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
                ('facility', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='IT_ADMIN.facility')),
            ],
            options={
                'verbose_name': 'GSN',
                'verbose_name_plural': 'GSN',
            },
        ),
        migrations.CreateModel(
            name='HSN_to_tax',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('product_hsn_code', models.PositiveIntegerField()),
                ('SGST', models.PositiveIntegerField()),
                ('CGST', models.PositiveIntegerField()),
                ('IGST', models.PositiveIntegerField(null=True)),
                ('State_tax', models.PositiveIntegerField(blank=True, null=True)),
                ('created_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'HSN details',
                'verbose_name_plural': 'HSN & Taxes',
            },
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('product_name', models.CharField(max_length=60)),
                ('selling_price', models.DecimalField(blank=True, decimal_places=2, max_digits=8, null=True)),
                ('discount_amount', models.DecimalField(blank=True, decimal_places=2, max_digits=8, null=True)),
                ('unit', models.CharField(choices=[('kilogram', 'kilogram'), ('set', 'set'), ('piece', 'piece'), ('item', 'item')], max_length=10)),
                ('HSN', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='WAREHOUSE.hsn_to_tax')),
                ('created_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['-created_on'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Purchase',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('purchase_order_date', models.DateField()),
                ('total_price', models.DecimalField(decimal_places=2, max_digits=8)),
                ('order_delivery_date', models.DateField()),
                ('purchase_bill', cloudinary.models.CloudinaryField(max_length=255, verbose_name='docs/purchase_bills')),
                ('status', models.BooleanField(default=False)),
                ('created_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['-created_on'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Warehouse_to_role',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('no_of_role', models.PositiveIntegerField()),
                ('role', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='auth.group')),
            ],
            options={
                'verbose_name': 'Roles in warehouse',
            },
        ),
        migrations.CreateModel(
            name='Vendor',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('vendor_name', models.CharField(max_length=60)),
                ('state', models.CharField(max_length=60)),
                ('district', models.CharField(max_length=60)),
                ('address', models.TextField(max_length=300)),
                ('email', models.EmailField(max_length=60, null=True, unique=True)),
                ('phonenumber', models.BigIntegerField(unique=True, validators=[django.core.validators.MaxValueValidator(99999999999)])),
                ('additional_phonenumber', models.BigIntegerField(blank=True, null=True, unique=True, validators=[django.core.validators.MaxValueValidator(99999999999)])),
                ('bank_name', models.CharField(max_length=60)),
                ('branch_name', models.CharField(max_length=60)),
                ('branch_address', models.TextField(max_length=300)),
                ('beneficiary_name', models.CharField(max_length=60)),
                ('account_number', models.CharField(max_length=100)),
                ('ifsc_number', models.CharField(max_length=60)),
                ('micr_code', models.CharField(max_length=60)),
                ('rtgs_neft_number', models.CharField(max_length=60)),
                ('gst_no', models.CharField(max_length=60)),
                ('pan_no', models.CharField(max_length=60)),
                ('EPF_Reg_No', models.CharField(max_length=60)),
                ('created_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['-created_on'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Taxtype',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('tax_name', models.CharField(max_length=60)),
                ('tax_percent', models.PositiveIntegerField()),
                ('HSN_reference', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='WAREHOUSE.hsn_to_tax')),
            ],
            options={
                'verbose_name': 'ADD NEW TAX TYPE',
            },
        ),
        migrations.CreateModel(
            name='Stock_batch',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('batch_no', models.CharField(max_length=20)),
                ('stock_quantity', models.PositiveIntegerField()),
                ('batch_expiry', models.DateField()),
                ('created_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
                ('grn_prod', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='WAREHOUSE.grn_products')),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='WAREHOUSE.product')),
            ],
            options={
                'ordering': ['-created_on'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Purchase_products',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('quantity', models.PositiveIntegerField()),
                ('price_per_unit', models.DecimalField(decimal_places=2, max_digits=8)),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='WAREHOUSE.product')),
                ('purchase_order', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='WAREHOUSE.purchase')),
            ],
            options={
                'verbose_name': 'Product Purchase',
            },
        ),
        migrations.AddField(
            model_name='purchase',
            name='vendor',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='WAREHOUSE.vendor'),
        ),
        migrations.CreateModel(
            name='GSN_products',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('send_qty', models.PositiveIntegerField()),
                ('gsn', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='WAREHOUSE.gsn')),
                ('instock_products', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='WAREHOUSE.stock_batch')),
            ],
            options={
                'verbose_name': "Products in GSN'",
            },
        ),
        migrations.AddField(
            model_name='grn_products',
            name='Product',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='WAREHOUSE.product'),
        ),
        migrations.AddField(
            model_name='grn_products',
            name='grn',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='WAREHOUSE.grn'),
        ),
        migrations.AddField(
            model_name='grn',
            name='order_no',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='WAREHOUSE.purchase'),
        ),
    ]
