from django.contrib.auth.models import User, Group
from django.db.models.base import Model
# from STOCK_MANAGEMENT.models import Urgentpurchase
from django.db import models
from IT_ADMIN.models import BaseModel, Facility, STATUS
from django.core.validators import MaxValueValidator
import cloudinary
from cloudinary.models import CloudinaryField
import datetime
# from django.contrib.auth.models import Group,User
# import IT_ADMIN.models
# Create your models here.


class Vendor(BaseModel):  # add vendor details
    vendor_name = models.CharField(max_length=60)
    state = models.CharField(max_length=60)
    district = models.CharField(max_length=60)
    address = models.TextField(max_length=300)
    email = models.EmailField(max_length=60, null=True, unique=True)
    phonenumber = models.BigIntegerField(
        unique=True, validators=[MaxValueValidator(99999999999)])
    additional_phonenumber = models.BigIntegerField(
        unique=True, validators=[MaxValueValidator(99999999999)], null=True, blank=True)
    bank_name = models.CharField(max_length=60)
    branch_name = models.CharField(max_length=60)
    branch_address = models.TextField(max_length=300)
    beneficiary_name = models.CharField(max_length=60)
    account_number = models.CharField(max_length=100)
    ifsc_number = models.CharField(max_length=60)
    micr_code = models.CharField(max_length=60)
    rtgs_neft_number = models.CharField(max_length=60)
    gst_no = models.CharField(max_length=60)
    pan_no = models.CharField(max_length=60)
    EPF_Reg_No = models.CharField(max_length=60)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)
    # warehouse = models.ForeignKey(Warehouse_details, on_delete=models.CASCADE)

    def __str__(self):
        return self.vendor_name + " - " + self.district
# -------------------------------------------HSA CODE AND TAXES---------------------------------------------------------------------


class HSN_to_tax(BaseModel):
    product_hsn_code = models.PositiveIntegerField()
    SGST = models.PositiveIntegerField()
    CGST = models.PositiveIntegerField()
    IGST = models.PositiveIntegerField(null=True)
    State_tax = models.PositiveIntegerField(blank=True, null=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.product_hsn_code)

    class Meta:
        verbose_name = 'HSN details'
        verbose_name_plural = 'HSN & Taxes'


class Taxtype(BaseModel):
    tax_name = models.CharField(max_length=60)
    tax_percent = models.PositiveIntegerField()
    HSN_reference = models.ForeignKey(HSN_to_tax, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "ADD NEW TAX TYPE"


# ----------------------------------------------DEFINE A NEW PRODUCT-----------------------------------------------------------------
UNIT_CHOICES = (("kilogram", "kilogram"), ("set", "set"),
                ("piece", "piece"), ("item", "item"))


class Product(BaseModel):
    product_name = models.CharField(max_length=60)
    selling_price = models.DecimalField(
        max_digits=8, decimal_places=2, blank=True, null=True)
    HSN = models.ForeignKey(HSN_to_tax, on_delete=models.CASCADE)
    discount_amount = models.DecimalField(
        max_digits=8, decimal_places=2, blank=True, null=True)
    unit = models.CharField(max_length=10, choices=UNIT_CHOICES)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)
    # warehouse = models.ForeignKey(Warehouse_details, on_delete=models.CASCADE)

    def __str__(self):
        return self.product_name
# -----------------------------------------------FOR PLACING A PURCHASE ORDER---------------------------------------------------------


class Purchase(BaseModel):
    vendor = models.ForeignKey(Vendor, on_delete=models.CASCADE)
    purchase_order_date = models.DateField()
    total_price = models.DecimalField(max_digits=8, decimal_places=2)
    order_delivery_date = models.DateField()
    purchase_bill = CloudinaryField('docs/purchase_bills')
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)
    # warehouse = models.ForeignKey(Warehouse_details, on_delete=models.CASCADE)
    status = models.BooleanField(default=False)

    def __str__(self):
        return str(self.id)


class Purchase_products(BaseModel):
    purchase_order = models.ForeignKey(Purchase, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField()
    price_per_unit = models.DecimalField(max_digits=8, decimal_places=2)

    class Meta:
        verbose_name = "Product Purchase"
# -------------------------------------------------GRN--------------------------------------------------------------------------------


class GRN(BaseModel):
    order_no = models.ForeignKey(Purchase, on_delete=models.CASCADE)
    date_recived = models.DateField()
    note = models.TextField(max_length=300)
    grn_bill = CloudinaryField('docs/GRN_bills')
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)
    # warehouse = models.ForeignKey(Warehouse_details, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.id)

    class Meta:
        verbose_name = 'GRN'
        verbose_name_plural = 'GRN'


class GRN_products(BaseModel):
    grn = models.ForeignKey(GRN, on_delete=models.CASCADE)
    Product = models.ForeignKey(Product, on_delete=models.CASCADE)
    qty_recived = models.PositiveIntegerField()
    clearance_status = models.BooleanField(default=False)

    def __str__(self):
        return self.Product.product_name+" : "+str(self.grn)

    class Meta:
        verbose_name = "Products in GRN'"
# -------------------------------------------- Internal stock----------------------------------------------------------------------------


class Stock_batch(BaseModel):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    grn_prod = models.ForeignKey(GRN_products, on_delete=models.CASCADE)
    batch_no = models.CharField(max_length=20)
    stock_quantity = models.PositiveIntegerField()
    batch_expiry = models.DateField()
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)
    # warehouse = models.ForeignKey(Warehouse_details, on_delete=models.CASCADE)

    def __str__(self):
        return self.product.product_name + " batch:" + str(self.batch_no)
# -------------------------------------------------GSN--------------------------------------------------------------------------------


class GSN(BaseModel):
    facility = models.ForeignKey(Facility, on_delete=models.CASCADE)
    send_date = models.DateField(default=datetime.date.today)
    reciving_status = models.BooleanField(default=False)
    recived_date = models.DateField(null=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)
    bill = CloudinaryField('docs/GSN_bills')
    # warehouse = models.ForeignKey(Warehouse_details, on_delete=models.CASCADE)

    def __str__(self):
        return self.facility.facility_name+" : "+str(self.send_date)

    class Meta:
        verbose_name = 'GSN'
        verbose_name_plural = 'GSN'


class GSN_products(BaseModel):
    gsn = models.ForeignKey(GSN, on_delete=models.CASCADE)
    instock_products = models.ForeignKey(Stock_batch, on_delete=models.CASCADE)
    send_qty = models.PositiveIntegerField()

    def __str__(self):
        return str("product id")+" : "+str(self.instock_products.product.id)

    class Meta:
        verbose_name = "Products in GSN'"


# class GSN_urgent_req(BaseModel):
#     gsn = models.ForeignKey(GSN, on_delete=models.CASCADE)
#     urgent_req = models.ForeignKey(
#         Urgentpurchase, on_delete=models.CASCADE, null=True)

#     class Meta:
#         verbose_name = "Considered Urgent Request"


class Warehouse_to_role(BaseModel):  # Mapping roles to service types
    # warehouse = models.ForeignKey(
        # Warehouse_details, on_delete=models.CASCADE, null=True)
    role = models.ForeignKey(Group, on_delete=models.CASCADE, null=True)
    no_of_role = models.PositiveIntegerField()

    class Meta:
        verbose_name = 'Roles in warehouse'

# ===============================warehouse models before edit=======================================
# from django.contrib.auth.models import User
# from django.db.models.base import Model
# from STOCK_MANAGEMENT.models import Urgentpurchase
# from django.db import models
# from IT_ADMIN.models import BaseModel,Facility
# from django.core.validators import MaxValueValidator
# import cloudinary
# from cloudinary.models import CloudinaryField
# import datetime
# # Create your models here.
# class Vendor(BaseModel): #add vendor details
#     vendor_name = models.CharField(max_length=60)
#     state = models.CharField(max_length=60)
#     district = models.CharField(max_length=60)
#     address = models.TextField(max_length=300)
#     email = models.EmailField(max_length=60,null=True,unique=True)
#     phonenumber = models.BigIntegerField(unique=True,validators=[MaxValueValidator(99999999999)])
#     additional_phonenumber = models.BigIntegerField(unique=True,validators=[MaxValueValidator(99999999999)],null=True,blank=True)
#     bank_name = models.CharField(max_length=60)
#     branch_name = models.CharField(max_length=60)
#     branch_address = models.TextField(max_length=300)
#     beneficiary_name = models.CharField(max_length=60)
#     account_number = models.CharField(max_length=100)
#     ifsc_number = models.CharField(max_length=60)
#     micr_code = models.CharField(max_length=60)
#     rtgs_neft_number = models.CharField(max_length=60)
#     gst_no = models.CharField(max_length=60)
#     pan_no = models.CharField(max_length=60)
#     EPF_Reg_No = models.CharField(max_length=60)
#     created_by = models.ForeignKey(User,on_delete=models.CASCADE)
#     def __str__(self):
#         return self.vendor_name+ " - " + self.district

# #-------------------------------------------HSA CODE AND TAXES---------------------------------------------------------------------

# class HSN_to_tax(BaseModel):
#     product_hsn_code = models.PositiveIntegerField()
#     SGST = models.PositiveIntegerField()
#     CGST = models.PositiveIntegerField()
#     IGST = models.PositiveIntegerField(null=True)
#     State_tax = models.PositiveIntegerField(blank=True,null=True)
#     created_by = models.ForeignKey(User,on_delete=models.CASCADE)
#     def __str__(self):
#         return str(self.product_hsn_code)
#     class Meta:
#         verbose_name = 'HSN details'
#         verbose_name_plural = 'HSN & Taxes'

# class Taxtype(BaseModel):
#     tax_name = models.CharField(max_length=60)
#     tax_percent = models.PositiveIntegerField()
#     HSN_reference = models.ForeignKey(HSN_to_tax,on_delete=models.CASCADE)
#     class Meta:
#         verbose_name = "ADD NEW TAX TYPE"
# #----------------------------------------------DEFINE A NEW PRODUCT-----------------------------------------------------------------
# UNIT_CHOICES = (("kilogram","kilogram"),("set","set"),("piece","piece"),("item","item"))

# class Product(BaseModel):
#     product_name = models.CharField(max_length=60)
#     selling_price = models.DecimalField(max_digits=8 , decimal_places=2,blank=True,null=True)
#     HSN = models.ForeignKey(HSN_to_tax,on_delete=models.CASCADE)
#     discount_amount = models.DecimalField(max_digits=8 , decimal_places=2, blank=True,null=True)
#     unit = models.CharField(max_length=10,choices=UNIT_CHOICES)
#     created_by = models.ForeignKey(User,on_delete=models.CASCADE)
#     def __str__(self):
#         return self.product_name


# #-----------------------------------------------FOR PLACING A PURCHASE ORDER---------------------------------------------------------

# class Purchase(BaseModel):
#     vendor = models.ForeignKey(Vendor,on_delete=models.CASCADE)
#     purchase_order_date = models.DateField()
#     total_price = models.DecimalField(max_digits=8 , decimal_places=2)
#     order_delivery_date = models.DateField()
#     purchase_bill = CloudinaryField('docs/purchase_bills')
#     created_by = models.ForeignKey(User,on_delete=models.CASCADE)
#     def __str__(self):
#         return str(self.id)

# class Purchase_products(BaseModel):
#     purchase_order = models.ForeignKey(Purchase,on_delete=models.CASCADE)
#     product = models.ForeignKey(Product,on_delete=models.CASCADE)
#     quantity = models.PositiveIntegerField()
#     price_per_unit = models.DecimalField(max_digits=8 , decimal_places=2)
#     class Meta:
#         verbose_name = "Product Purchase"
# #-------------------------------------------------GRN--------------------------------------------------------------------------------

# class GRN(BaseModel):
#     order_no = models.ForeignKey(Purchase,on_delete=models.CASCADE)
#     date_recived = models.DateField()
#     note = models.TextField(max_length=300)
#     grn_bill = CloudinaryField('docs/GRN_bills')
#     created_by = models.ForeignKey(User,on_delete=models.CASCADE)

#     def __str__(self):
#         return str(self.id)
#     class Meta:
#         verbose_name = 'GRN'
#         verbose_name_plural = 'GRN'

# class GRN_products(BaseModel):
#     grn = models.ForeignKey(GRN,on_delete=models.CASCADE)
#     Product = models.ForeignKey(Product,on_delete=models.CASCADE)
#     qty_recived = models.PositiveIntegerField()
#     clearance_status = models.BooleanField(default=False)
#     def __str__(self):
#         return self.Product.product_name+" : "+str(self.grn)
#     class Meta:
#         verbose_name = "Products in GRN'"
# #-------------------------------------------- Internal stock----------------------------------------------------------------------------
# class Stock_batch(BaseModel):
#     product = models.ForeignKey(Product,on_delete=models.CASCADE)
#     grn_prod = models.ForeignKey(GRN_products,on_delete=models.CASCADE)
#     batch_no = models.CharField(max_length=20)
#     stock_quantity = models.PositiveIntegerField()
#     batch_expiry = models.DateField()
#     created_by = models.ForeignKey(User,on_delete=models.CASCADE)
#     def __str__(self):
#         return self.product.product_name+ " batch:" +str(self.batch_no)
# #-------------------------------------------------GSN--------------------------------------------------------------------------------
# class GSN(BaseModel):
#     facility = models.ForeignKey(Facility,on_delete=models.CASCADE)
#     send_date = models.DateField(default=datetime.date.today)
#     reciving_status = models.BooleanField(default=False)
#     recived_date = models.DateField(null=True)
#     created_by = models.ForeignKey(User,on_delete=models.CASCADE)
#     bill = CloudinaryField('docs/GSN_bills')

#     def __str__(self):
#         return self.facility.facility_name+" : "+str(self.send_date)

#     class Meta:
#         verbose_name = 'GSN'
#         verbose_name_plural = 'GSN'

# class GSN_products(BaseModel):
#     gsn = models.ForeignKey(GSN,on_delete=models.CASCADE)
#     instock_products = models.ForeignKey(Stock_batch,on_delete=models.CASCADE)
#     send_qty = models.PositiveIntegerField()

#     def __str__(self):
#         return str("product id")+" : "+str(self.instock_products.product.id)

#     class Meta:
#         verbose_name = "Products in GSN'"

# class GSN_urgent_req(BaseModel):
#     gsn = models.ForeignKey(GSN,on_delete=models.CASCADE)
#     urgent_req = models.ForeignKey(Urgentpurchase,on_delete=models.CASCADE,null=True)

#     class Meta:
#         verbose_name = "Considered Urgent Request"
