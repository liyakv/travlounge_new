# from django.contrib.auth.models import User
# # from travlounge.HR.models import Employee
# from django.contrib import admin
# from WAREHOUSE.models import Warehouse_to_role, GSN_urgent_req, GSN, GSN_products, Stock_batch, Product, Vendor, HSN_to_tax, Taxtype, Purchase, Purchase_products, GRN, GRN_products
# from IT_ADMIN.models import Facility
# from django.forms.models import BaseInlineFormSet
# from django.shortcuts import render, redirect
# from django.contrib import messages
# from STOCK_MANAGEMENT.models import Urgentpurchase, Urgentpurchase_products, InternalStock
# import nested_admin
# from HR.models import Employee
# from IT_ADMIN.models import Custom_User
# from ACCOUNTING.models import Main_ledger, Particulars
# from django.db.models import Q
# # from IT_ADMIN.admin import RequiredInlineFormSet,Service_to_roleAdmin

# # Register your models here.
# # --------------------------------General settings---------------------------


# @admin.action(description='Delete')
# def delete_action(modeladmin, request, queryset):
#     queryset.update(is_deleted=True)
# # -------------------------------create warehouse----------------------------------


# class RequiredInlineFormSet(BaseInlineFormSet):
#     def _construct_form(self, i, **kwargs):
#         form = super(RequiredInlineFormSet, self)._construct_form(i, **kwargs)
#         form.empty_permitted = False
#         return form


# class warehouse_to_roleAdmin(admin.StackedInline):
#     model = Warehouse_to_role
#     fields = [('role', 'no_of_role')]
#     list_display = ('id',)
#     formset = RequiredInlineFormSet


# @admin.register(Warehouse_details)
# class WarehouseAdmin(admin.ModelAdmin):
#     list_display = ('id', 'name', 'cash')
#     exclude = ('is_deleted', 'cash')
#     actions = [delete_action]
#     inlines = [warehouse_to_roleAdmin, ]

# # ---------------------------Vendors------------------------------------------


# @admin.register(Vendor)
# class VendorAdmin(admin.ModelAdmin):
#     list_display = ('vendor_name', 'state', 'district', 'address',)
#     exclude = ('is_deleted',)
#     actions = [delete_action]
#     change_form_template = 'cash_details.html'

#     def save_model(self, request, obj, form, change):
#         if "cash" in request.POST:
#             pass
#         else:
#             wHouseId = Employee.objects.get(user=request.user.id)
#             obj.created_by = request.user
#             obj.warehouse = wHouseId.warehouse
#             super().save_model(request, obj, form, change)

#     def has_delete_permission(self, request, obj=None):
#         return False

#     def get_queryset(self, request):
#         queryset = super(VendorAdmin, self).get_queryset(request)
#         our_user = request.user.id
#         wHouseId = Employee.objects.get(user=our_user)
#         objWareouse = wHouseId.warehouse
#         return queryset.filter(is_deleted=False).filter(warehouse=objWareouse)

#     def response_change(self, request, obj):
#         if "cash" in request.POST:
#             vender_id = obj.id
#             sundry_creditors = Main_ledger.objects.filter(vendor_name=vender_id).filter(
#                 under_m__primary__name__iexact="sundry creditors")
#             sundry_debtors = Main_ledger.objects.filter(vendor_name=vender_id).filter(
#                 under_m__primary__name__iexact="sundry debtors")
#             return render(request, 'cashDetails.html', context={"sundry_creditors": sundry_creditors, "sundry_debtors": sundry_debtors})
#         return super().response_change(request, obj)

#     def change_view(self, request, object_id, form_url='', extra_context=None, **kwargs):
#         extra_context = extra_context or {}
#         a = ''
#         if object_id:
#             a = "show"
#         extra_context['my_extra_content'] = a
#         return super().change_view(request, object_id, form_url,  extra_context=extra_context, **kwargs)

#     fieldsets = (
#         ('', {'fields': ('vendor_name', 'state', 'district', 'address',
#          'email', 'phonenumber', 'additional_phonenumber')}),
#         ('Bank Details', {'fields': ('bank_name', 'branch_name', 'branch_address',
#          'beneficiary_name', 'account_number', 'ifsc_number', 'micr_code', 'rtgs_neft_number')}),
#         ('Statutory Details', {'fields': ('gst_no', 'pan_no', 'EPF_Reg_No')})
#     )
# # -------------------------------DEFINE HSA WITH TAX-----------------------------------------


# class TaxtypeAdmin(admin.StackedInline):
#     model = Taxtype
#     exclude = ('is_deleted', 'created_by',)
#     min_num = 0
#     extra = 1


# @admin.register(HSN_to_tax)
# class HSN_to_taxAdmin(admin.ModelAdmin):
#     list_display = ('product_hsn_code', 'SGST', 'CGST', 'IGST')
#     actions = [delete_action]
#     search_fields = ['product_hsn_code', 'SGST', 'CGST', 'IGST']
#     exclude = ('is_deleted', 'created_by',)
#     inlines = [TaxtypeAdmin, ]
#     # def get_queryset(self, request):
#     # queryset = super(HSN_to_taxAdmin, self).get_queryset(request)
#     # # our_user = request.user.id
#     # wHouseid=Employee.objects.get(user=request.user.id)
#     # pdt=list(Product.objects.filter(warehouse=wHouseid.warehouse.id).values_list("HSN",flat=True))
#     # return queryset.filter(is_deleted=False).filter(id__in=pdt)

#     def save_model(self, request, obj, form, change):
#         obj.created_by = request.user
#         super().save_model(request, obj, form, change)
#         pass

#     def has_delete_permission(self, request, obj=None):
#         return False


# # ---------------------------------DEFINE NEW PRODUCT-------------------------------------------
# # A new product that is to brought or sell is defined here
# @admin.register(Product)
# class ProductAdmin(admin.ModelAdmin):
#     list_display = ('product_name', 'HSN')
#     exclude = ('is_deleted', 'created_by', 'warehouse')
#     actions = [delete_action]
#     list_filter = (('HSN', admin.RelatedOnlyFieldListFilter),)

#     def get_queryset(self, request):
#         wHouseid = Employee.objects.get(user=request.user.id)
#         queryset = super(ProductAdmin, self).get_queryset(request)
#         return queryset.filter(is_deleted=False).filter(warehouse=wHouseid.warehouse.id)

#     def save_model(self, request, obj, form, change):
#         obj.created_by = request.user
#         wHouseid = Employee.objects.get(user=request.user.id)
#         obj.warehouse = wHouseid.warehouse
#         super().save_model(request, obj, form, change)

#     def has_delete_permission(self, request, obj=None):
#         return False
#     # def formfield_for_foreignkey(self, db_field, request, **kwargs):
#     #     if db_field.name == "HSN":
#     #         pdt=list(Product.objects.filter(warehouse=self.wHouseid.warehouse.id).values_list("HSN",flat=True))
#     #         kwargs["queryset"] = HSN_to_tax.objects.filter(id__in=pdt)
#     #     return super().formfield_for_foreignkey(db_field, request, **kwargs)

# # ----------------------------------PLACE A PURCHASE ORDER-----------------------------------------


# class Purchase_productsAdmin(admin.StackedInline):  # products in order
#     model = Purchase_products
#     exclude = ('is_deleted',)
#     min_num = 1
#     extra = 0

#     def has_delete_permission(self, request, obj=None):
#         return False

#     def has_change_permission(self, request, obj=None):
#         return False


# @admin.register(Purchase)
# class PurchaseAdmin(admin.ModelAdmin):  # main purchase order
#     exclude = ('is_deleted', 'created_by', 'warehouse')
#     list_display = ('id', 'vendor', 'purchase_order_date', 'created_on')
#     inlines = [Purchase_productsAdmin, ]
#     list_filter = (('vendor', admin.RelatedOnlyFieldListFilter),)
#     change_form_template = "change_form_purchase.html"

#     def save_model(self, request, obj, form, change):
#         wHouseId = Employee.objects.get(user=request.user.id)
#         obj.created_by = request.user
#         obj.warehouse = wHouseId.warehouse
#         super().save_model(request, obj, form, change)

#     def has_delete_permission(self, request, obj=None):
#         return False

#     def has_change_permission(self, request, obj=None):
#         return False

#     def get_queryset(self, request):
#         queryset = super(PurchaseAdmin, self).get_queryset(request)
#         wHouseid = Employee.objects.get(user=request.user.id)
#         return queryset.filter(is_deleted=False).filter(warehouse=wHouseid.warehouse.id)

#     # function to exclude purchase bill field while viewing
#     def get_fields(self, request, obj=None):
#         fields = list(super(PurchaseAdmin, self).get_fields(request, obj))
#         exclude_set = set()
#         if obj:
#             exclude_set.add('purchase_bill')
#         return [f for f in fields if f not in exclude_set]

#     # function to display purchase bill while viewing
#     def change_view(self, request, object_id, form_url='', extra_context=None):
#         if object_id:
#             extra_context = extra_context or {}
#             extra_context['bill'] = Purchase.objects.filter(id=object_id)
#         return super().change_view(request, object_id, form_url, extra_context=extra_context)
# # --------------------------------------------------GRN ENTRY-----------------------------------------------------------------------------------


# class GRN_productsAdmin(admin.StackedInline):  # GRN products to be added
#     model = GRN_products
#     exclude = ('is_deleted', 'clearance_status',)
#     min_num = 1
#     extra = 0

#     def has_delete_permission(self, request, obj=None):
#         return False

#     def get_readonly_fields(self, request, obj=None):
#         if obj:  # editing an existing object
#             return self.readonly_fields + ('clearance_status',)
#         return self.readonly_fields


# @admin.register(GRN)
# class GRNAdmin(admin.ModelAdmin):
#     exclude = ('is_deleted', 'created_by', 'warehouse')
#     inlines = [GRN_productsAdmin, ]
#     list_display = ('id', 'order_no', 'date_recived', 'created_on')

#     def has_delete_permission(self, request, obj=None):
#         return False

#     def get_readonly_fields(self, request, obj=None):
#         if obj:  # editing an existing object
#             return self.readonly_fields + ('order_no', 'date_recived',)
#         return self.readonly_fields

#     def get_queryset(self, request):
#         queryset = super(GRNAdmin, self).get_queryset(request)
#         wHouseid = Employee.objects.get(user=request.user.id)
#         return queryset.filter(is_deleted=False).filter(warehouse=wHouseid.warehouse.id)

#     def save_model(self, request, obj, form, change):
#         wHouseId = Employee.objects.get(user=request.user.id)
#         obj.warehouse = wHouseId.warehouse
#         obj.created_by = request.user
#         purchase_no = obj.order_no.id
#         purchase_ = Purchase.objects.get(id=purchase_no)
#         purchase_.status = True
#         purchase_.save()
#         amound = purchase_.total_price
#         cash_obj = Warehouse_details.objects.get(id=wHouseId.warehouse.id)
#         initial_cash = cash_obj.cash
#         final_amount = initial_cash+amound
#         cash_obj.cash = final_amount
#         cash_obj.save()
#         super().save_model(request, obj, form, change)

#     def formfield_for_foreignkey(self, db_field, request, **kwargs):
#         if db_field.name == "order_no":
#             # pdt=list(Product.objects.filter(warehouse=self.wHouseid.warehouse.id).values_list("HSN",flat=True))
#             kwargs["queryset"] = Purchase.objects.filter(status=False)
#         return super().formfield_for_foreignkey(db_field, request, **kwargs)

# # --------------------------------------------------GSN ENTRY-------------------------------------------------------------------------------------
# # --------------------------------------------------ADD Internal stock----------------------------------------------------------------------------


# # stockes mentioned at grn are entered to inventory
# class Stock_batchAdmin(nested_admin.NestedTabularInline):
#     model = Stock_batch
#     extra = 0
#     min_num = 0
#     exclude = ('is_deleted', 'created_by', 'warehouse')

#     def formfield_for_foreignkey(self, db_field, request, **kwargs):
#         if db_field.name == "grn_prod":
#             parent_id = request.resolver_match.kwargs['object_id']
#             kwargs["queryset"] = GRN_products.objects.filter(
#                 Product=parent_id).filter(clearance_status=False)
#         return super().formfield_for_foreignkey(db_field, request, **kwargs)

#     def formfield_for_foreignkey(self, db_field, request, **kwargs):
#         if db_field.name == "product":
#             parent_id = request.resolver_match.kwargs['object_id']
#             kwargs["queryset"] = Product.objects.filter(id=parent_id)
#         return super().formfield_for_foreignkey(db_field, request, **kwargs)

#     def save_model(self, request, obj, form, change):
#         wHouseId = Employee.objects.get(user=request.user.id)
#         obj.warehouse = wHouseId.warehouse
#         super().save_model(request, obj, form, change)


# # display products based on grn
# class GRN_productsAdminProxy(nested_admin.NestedTabularInline):
#     model = GRN_products
#     extra = 0
#     min_num = 0
#     exclude = ('is_deleted', 'warehouse')
#     readonly_fields = ('grn', 'qty_recived',)
#     inlines = (Stock_batchAdmin,)

#     def get_queryset(self, request):
#         qs = super(GRN_productsAdminProxy, self).get_queryset(request)
#         return qs.filter(clearance_status=False)

#     def has_delete_permission(self, request, obj=None):
#         return False

#     def has_add_permission(self, request, obj=None):
#         return False


# class ProxyStock(Product):
#     class Meta:
#         proxy = True
#         verbose_name_plural = 'Internal stock'
#         verbose_name = 'New Internal stock'


# @admin.register(ProxyStock)
# class StockAdmin(nested_admin.NestedModelAdmin):
#     list_display = ('product_name', 'HSN',)
#     exclude = ('is_deleted', 'created_by', 'warehouse')
#     change_form_template = "change_form_stock.html"
#     readonly_fields = ('product_name', 'selling_price',
#                        'HSN', 'discount_amount', 'unit',)
#     inlines = (GRN_productsAdminProxy,)

#     def get_queryset(self, request):
#         queryset = super(StockAdmin, self).get_queryset(request)
#         our_user = request.user.id
#         return queryset.filter(is_deleted=False).filter(created_by=our_user)

#     def has_delete_permission(self, request, obj=None):
#         return False

#     def has_add_permission(self, request, obj=None):
#         return False

#     def change_view(self, request, object_id, form_url='', extra_context=None):
#         qty = list(Stock_batch.objects.filter(product=object_id).filter(stock_quantity__gte=1).filter(
#             created_by=request.user.id).values_list('stock_quantity', flat=True))
#         print(qty, "++++++++++")
#         total_qty = sum(qty)
#         self.total_qty = total_qty
#         extra_context = extra_context or {}
#         extra_context['total'] = total_qty
#         return super().change_view(request, object_id, form_url, extra_context=extra_context)

#     # reirect to template and show batch wise product details
#     def response_change(self, request, obj):
#         prod_id = Product.objects.filter(product_name=obj)
#         if "batches" in request.POST:
#             batches = Stock_batch.objects.filter(
#                 stock_quantity__gte=1).filter(product=prod_id[0].id)
#             return render(request, 'change_form_batch.html', {'form': batches, 'total': self.total_qty})
#         return super().response_change(request, obj)

#     # do not save while checking batch details
#     def save_formset(self, request, form, formset, change):
#         if "batches" in request.POST:
#             instances = formset.save(commit=False)
#             for obj in formset.deleted_objects:
#                 obj.delete()
#         else:  # save while clicking save function
#             instances = formset.save(commit=False)
#             for instance in instances:
#                 instance.created_by = request.user
#                 # wHouseId=Warehouse_details.objects.get(id=2)#need to change after employee creation-------------------------------------
#                 wHouseId = Employee.objects.get(user=request.user.id)
#                 instance.warehouse = wHouseId.warehouse
#                 print("ll")

#                 # super().save_model(request, obj, form, change)
#                 instance.save()
#                 # print("done........")


# # --------------------------------------------------GSN--------------------------------------------------------------------------------------
# class GSN_urgent_reqAdmin(admin.TabularInline):
#     model = GSN_urgent_req
#     exclude = ('is_deleted',)
#     extra = 0
#     min_num = 0

#     def has_delete_permission(self, request, obj=None):
#         return False

#     def has_change_permission(self, request, obj=None):
#         return False

#     def formfield_for_foreignkey(self, db_field, request, **kwargs):
#         if db_field.name == "urgent_req":
#             wHouseId = Employee.objects.get(user=request.user.id)
#             Wid = wHouseId.warehouse
#             fac_id = list(Facility.objects.filter(
#                 warehouse=Wid).values_list("id", flat=True))
#             kwargs["queryset"] = Urgentpurchase.objects.filter(
#                 Facility_id__in=fac_id).filter(is_deleted=False).filter(status=True)
#         return super().formfield_for_foreignkey(db_field, request, **kwargs)


# class GSN_productsAdmin(admin.TabularInline):
#     model = GSN_products
#     exclude = ('is_deleted',)
#     extra = 0
#     min_num = 1

#     def has_delete_permission(self, request, obj=None):
#         return False

#     def has_change_permission(self, request, obj=None):
#         return False

#     def formfield_for_foreignkey(self, db_field, request, **kwargs):
#         if db_field.name == "instock_products":
#             wHouseid = Employee.objects.get(user=request.user.id)
#             kwargs["queryset"] = Stock_batch.objects.filter(
#                 stock_quantity__gte=1).filter(warehouse=wHouseid.warehouse.id)
#         return super().formfield_for_foreignkey(db_field, request, **kwargs)


# @admin.register(GSN)
# class GSNAdmin(admin.ModelAdmin):
#     list_display = ('send_date', 'reciving_status',)  # facility
#     list_filter = (('facility', admin.RelatedOnlyFieldListFilter),)
#     exclude = ('is_deleted', 'reciving_status',
#                'recived_date', 'created_by', 'warehouse')
#     inlines = (GSN_productsAdmin, GSN_urgent_reqAdmin)
#     change_form_template = "change_form_urgent_gsn.html"

#     def has_delete_permission(self, request, obj=None):
#         return False

#     def has_change_permission(self, request, obj=None):
#         return False

#     def get_queryset(self, request):
#         queryset = super(GSNAdmin, self).get_queryset(request)
#         wHouseid = Employee.objects.get(user=request.user.id)
#         our_user = request.user.id
#         return queryset.filter(is_deleted=False).filter(warehouse=wHouseid.warehouse.id)

#     def formfield_for_foreignkey(self, db_field, request, **kwargs):
#         if db_field.name == "facility":
#             wHouseId = Employee.objects.get(user=request.user.id)
#             kwargs["queryset"] = Facility.objects.filter(warehouse=wHouseId.warehouse).filter(
#                 is_deleted=False).filter(facility_status="running")
#         return super().formfield_for_foreignkey(db_field, request, **kwargs)
#     # -----------------------------------------------------------------------------------------------------------------------------------

#     def save_model(self, request, obj, form, change):
#         obj.created_by = request.user
#         wHouseId = Employee.objects.get(user=request.user.id)
#         obj.warehouse = wHouseId.warehouse
#         self.WH = wHouseId.warehouse
#         self.object = obj
#         pass

#     def save_formset(self, request, form, formset, change):
#         instances = formset.save(commit=False)
#         print(instances, "kkkkkkkkkkkkkkkkkkk")
#         for instance in instances:
#             q = Product.objects.get(id=instance.instock_products.product.id)
#             price = q.selling_price
#             Qy = instance.send_qty
#             total_price = price*Qy
#             warehouseID = self.WH
#             Objwarehouse_details = Warehouse_details.objects.get(
#                 id=warehouseID.id)
#             initial_cash = Objwarehouse_details.cash
#             finalCash = initial_cash-total_price
#             if "product id" in str(instance):
#                 print(instance, ".........................instancela")
#                 q = Product.objects.get(
#                     id=instance.instock_products.product.id)
#                 price = q.selling_price
#                 Qy = instance.send_qty
#                 total_price = price*Qy
#                 warehouseID = self.WH
#                 Objwarehouse_details = Warehouse_details.objects.get(
#                     id=warehouseID.id)
#                 initial_cash = Objwarehouse_details.cash
#                 self.finalCash = initial_cash-total_price

#                 print(self.finalCash,
#                       ".......................................finalCash")

#                 product = instance.instock_products.id
#                 qty = instance.send_qty
#                 batch_qty = Stock_batch.objects.filter(id=product)
#                 current_qty = batch_qty[0].stock_quantity
#                 final_stock = current_qty-qty
#                 if final_stock < 0:
#                     for obj in formset.deleted_objects:
#                         obj.delete()
#                     messages.set_level(request, messages.ERROR)
#                     messages.error(
#                         request, "STOCK MORE THAN IN BATCH WAS REMOVED")
#                 else:
#                     self.object.save()
#                     instance.save()
#                     warehouseID = self.WH
#                     Objwarehouse_details = Warehouse_details.objects.filter(
#                         id=warehouseID.id).update(cash=self.finalCash)
#                     Stock_batch.objects.filter(id=product).update(
#                         stock_quantity=final_stock)
#             if "urgent_req" in str(instance):
#                 print("iii")
#                 urgent_request = instance.urgent_req.id
#                 instance.save()
#                 warehouseID = self.WH
#                 Objwarehouse_details = Warehouse_details.objects.filter(
#                     id=warehouseID.id).update(cash=self.finalCash)
#                 Urgentpurchase.objects.filter(
#                     id=urgent_request).update(is_deleted=True)

#             # else:
#             #     urgent_request = instance.urgent_req.id
#             #     instance.save()
#             #     warehouseID=self.WH
#             #     Objwarehouse_details=Warehouse_details.objects.filter(id=warehouseID.id).update(cash=finalCash)
#             #     Urgentpurchase.objects.filter(id=urgent_request).update(is_deleted=True)

# # ----------------------------------Show urget purchase Requests----------------------------------------------------------------------------

# # ----------------------------------Show urget purchase Requests----------------------------------------------------------------------------


# class UrgentpurchaseProxyAdmin(Urgentpurchase):
#     class Meta:
#         proxy = True
#         verbose_name_plural = 'Urgent Requests'


# @admin.register(UrgentpurchaseProxyAdmin)
# class UrgentpurchaseAdmin(admin.ModelAdmin):
#     list_display = ('Facility_id', 'priority', 'need_before', 'status')
#     exclude = ('is_deleted', 'status',)
#     readonly_fields = ('reason', 'priority', 'need_before', 'Facility_id',)
#     change_form_template = "change_form_urgent_req.html"
#     list_filter = (
#         ('Facility_id', admin.RelatedOnlyFieldListFilter), 'priority',)

#     def get_queryset(self, request):
#         qs = super(UrgentpurchaseAdmin, self).get_queryset(request)
#         wHouseId = Employee.objects.get(user=request.user.id)
#         Wid = wHouseId.warehouse.id
#         fac_id = list(Facility.objects.filter(
#             warehouse=Wid).values_list("id", flat=True))
#         return qs.filter(is_deleted=False).filter(Facility_id__in=fac_id)

#     def has_add_permission(self, request, obj=None):
#         return False

#     def has_delete_permission(self, request, obj=None):
#         return False

#     def change_view(self, request, object_id, form_url='', extra_context=None):
#         prods = Urgentpurchase_products.objects.filter(
#             urgentpurchase_id=object_id)
#         extra_context = extra_context or {}
#         extra_context['prods'] = prods
#         return super().change_view(request, object_id, form_url, extra_context=extra_context)

#     def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
#         context.update({
#             'show_save_and_continue': False,
#             'show_save_and_add_another': False,
#             'show_save': False,
#         })
#         return super().render_change_form(request, context, add, change, form_url, obj)

#     def response_change(self, request, obj):
#         if "consider" in request.POST:
#             Urgentpurchase.objects.filter(id=obj.id).update(status=True)
#         return super().response_change(request, obj)
# # ----------------------------------View Internal stocks----------------------------------------------------------------------------


# class InternalStockFacilityAdmin(Facility):
#     class Meta:
#         proxy = True
#         verbose_name_plural = 'Facility stocks'
#         verbose_name = 'View Facility stock'


# @admin.register(InternalStockFacilityAdmin)
# class InternalStockViewAdmin(admin.ModelAdmin):
#     exclude = ('facility_status', 'sts', 'is_deleted')
#     change_form_template = "internal_stock.html"

#     def get_queryset(self, request):
#         qs = super(InternalStockViewAdmin, self).get_queryset(request)
#         wHouseId = Employee.objects.get(user=request.user.id)
#         fac_id = list(Facility.objects.filter(
#             warehouse=wHouseId.warehouse.id).values_list("id", flat=True))
#         return qs.filter(is_deleted=False).filter(id__in=fac_id)

#     def has_delete_permission(self, request, obj=None):
#         return False

#     def has_change_permission(self, request, obj=None):
#         return False

#     def has_add_permission(self, request, obj=None):
#         return False

#     def change_view(self, request, object_id, form_url='', extra_context=None):
#         print(object_id)
#         stock = InternalStock.objects.filter(facility_id=object_id)
#         print(stock)
#         extra_context = extra_context or {}
#         extra_context['stock'] = stock
#         return super().change_view(request, object_id, form_url, extra_context=extra_context)
