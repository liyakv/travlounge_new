import requests
from FCMManager import *
from django import forms
from django.db import models
from django.db.models.aggregates import Count
from django.db.models.base import Model
from WAREHOUSE.models import Warehouse_to_role
from django.contrib import admin
from IT_ADMIN.models import Facility, Facility_to_service, Service_to_role, Service_type, Custom_User, token_get
from CORPORATE.models import Notifications,Mark_as_read
from Manager_Owner.models import Manager_reports
from django.http import HttpResponseRedirect
from HR.models import Payrunbonus, Employee, Employee_bank, Employee_education, Employee_experience, Shift_type, Shift_type_to_date, Team, Team_to_employee, Payroll, Payroll_component, Payrun, Attendance, Leave, leave_request, Assign_leave
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin
from nested_inline.admin import NestedStackedInline, NestedModelAdmin
from django.shortcuts import render, redirect
from datetime import datetime
from django.db.models import Q
from django.db.models import Sum
from collections import Counter
from INCIDENT_MANAGEMENT.models import Incident_Assignment, Incident_Tickets
import math
from django.contrib.admin.widgets import AdminDateWidget
from HR.forms import CouplingUploadForm
from django.forms.models import BaseInlineFormSet
from django.core.exceptions import ValidationError
from django.contrib import messages
import json


# from firebase_admin.messaging import Message
# from fcm_django.models import FCMDevice
# from firebase_admin.messaging import Message

# Register your models here.
# --------------------------------General settings---------------------------


@admin.action(description='Delete')
def delete_action(modeladmin, request, queryset):
    queryset.update(is_deleted=True)

# -----------------------To Initiate facility created by IT Admin---------------------------------


class FacilityInitiationProxy(Facility):
    class Meta:
        proxy = True
        verbose_name = 'Facilities Pending Initiation'  # object name here
        verbose_name_plural = 'Initiate Facilities'  # plural object names here


@admin.register(FacilityInitiationProxy)
class FacilityInitiationProxy(admin.ModelAdmin):
    list_display = ('facility_name', 'facility_status',)
    readonly_fields = [('facility_name'), ('state'), ('District'), ('pincode')]
    fields = [('facility_name'), ('state'), ('District'), ('pincode')]
    change_form_template = "change_form2.html"
    list_per_page = 10

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj=None):
        return False

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        return queryset.filter(Q(facility_status='pending') & Q(is_deleted=False))

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        context.update({
            'show_save_and_continue': False,
            'show_save': False,
        })
        return super().render_change_form(request, context, add, change, form_url, obj)

    # show employees assigned in facility
    def change_view(self, request, object_id, form_url='', extra_context=None):
        emp = Employee.objects.filter(facility_to_service__facility=object_id)
        teams = Team.objects.filter(facility=object_id)
        extra_context = extra_context or {}
        extra_context['employees'] = emp
        extra_context['teams'] = teams
        return super().change_view(request, object_id, form_url, extra_context=extra_context)

    def response_change(self, request, obj):
        if "apply2" in request.POST:
            id = obj.id
            self.model.objects.filter(id=id).update(facility_status="initiated")
            print(obj.facility_status,"hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh")
            headers = {"Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjEzNzUwOTIwMDA1LCJpYXQiOjE2NTQ5MjAwMDUsIm1vYmlsZSI6IjExMjIzMzQ0NTUifQ.AmsZIvdFsld_KzvufGMiB2YtdE67ezjnFOYFdVvZtqU"}    
            r = requests.put(f'http://128.199.139.248:6969/getFacilityById/{obj.id}',
                    data=json.dumps({
                        "id": obj.id,
                        "name": obj.facility_name,
                        "state": obj.state,
                        "district": obj.District,
                        "pincode": obj.pincode,
                        "longitude": obj.longitude,
                        "latitude": obj.latitude,
                        "facility_status":"initiated",
                        "address": obj.Address,
                        "is_deleted": obj.is_deleted,
                    }),headers=headers)
            self.message_user(request, "FACILITY HAS BEEN INITIATED")

            # try:
            #     fh=request.user
            #     print(fh)
            #     user_instance = User.objects.get(id=fh.id)
            #     print(user_instance)
            #     # above fcm code
            #     device = FCMDevice.objects.all()
            #     print(device)
            #     device.send_message(title="hlooo", body="hihloooooooooooooooooooo",message="hiiiiii",data={"test": "test"})
            #     # devices = FCMDevice.objects.all()
            #     # devices.send_message(Message(data={"test": "test"}))
            #     # # Or (send_message parameters include: messages, dry_run, app)
            #     # # FCMDevice.objects.send_message(Message(...))

            # except Exception as e:
            #     print(e.args)
            #     pass

            # userid=request.user
            # print(userid)
            # faclist=Custom_User.objects.filter(user=userid).values_list("Facilities")
            # print(faclist)
            # sendPush(title='new titile',msg='newmsg',registration_token=['d0TaObc1Bro5pVJuCO1Nos:APA91bGxBbA1y63XLE8cshz2l78YjSeMv4oCU_U7WBuBz19fa_oPQBmzEk0QQoFhKraey1qFF_af9In92aNveAbz4F43PyXghqOwHC6dYSdRo8KfcKaWkZz22vlYHusEtc6446ZqNmOd'])
            custom = Custom_User.objects.values_list(
                'user').filter(Facilities=id)
            out = [item for t in custom for item in t]
            tk = token_get.objects.values_list(
                "token_get").filter(user__in=out)
            registration_token = [item for t in tk for item in t]
            # print(registration_token, 'newtoken')
            sendPush(title="facility initiated",
                     msg=f'initiate facility={obj.facility_name}', registration_token=registration_token)
            # print("liyaaaaaaaaaaaaaaaaaaaaaaaaaaaa")
            return redirect('/admin/HR/facilityinitiationproxy/')
        return super().response_change(request, obj)

    # def save_model(self, request, obj, form, change):
    #     try:
    #         user_instance = User.objects.all()
    #         # above fcm code
    #         device = FCMDevice.objects.get(user=obj.id)
    #         device.send_message(title="hlooo", body="hihloooooooooooooooooooo", icon=..., data={"test": "test"})
    #     except Exception as e:
    #         print(e.args)
    #         pass
        # super(Facility, self).save_model(request, obj, form, change)
# -----------------------To Create employees and assign them to facilities---------------------------- (EXPANSION OF USER TABLE)


# employee education details are entered (SET AS OPTIONAL TABLE)
class Employee_educationInline(NestedStackedInline):
    model = Employee_education
    exclude = ('is_deleted',)
    extra = 0
    min_num = 0
    max_num = 6


class LeaveInline(NestedStackedInline):  # leaves alloted for employee
    model = Assign_leave
    exclude = ('is_deleted',)
    min_num = 1
    max_num = 1
    extra = 0
    filter_horizontal = ('leave_id',)


class PayrollInline(NestedStackedInline):  # employee + allowance added here
    model = Payroll
    exclude = ('is_deleted',)
    min_num = 1
    max_num = 1
    extra = 0
    filter_horizontal = ('payroll_component',)

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        if db_field.name == "Payroll_component":
            kwargs["queryset"] = Payroll_component.objects.filter(is_deleted=False,
                given_at="Monthly payrun")
        return super(PayrollInline, self).formfield_for_manytomany(db_field, request, **kwargs)

    def has_delete_permission(self, request, obj=None):
        return False


# employee experience details are entered (SET AS OPTIONAL TABLE)
class Employee_experienceInline(NestedStackedInline):
    model = Employee_experience
    exclude = ('is_deleted',)
    extra = 0
    min_num = 0
    max_num = 6


# employee bank details are entered
class Employee_bankInline(NestedStackedInline):
    model = Employee_bank
    exclude = ('is_deleted',)
    min_num = 1

    def has_delete_permission(self, request, obj=None):
        return False


class EmployeeInline(NestedStackedInline):  # employee basic details are entered
    model = Employee
    form = CouplingUploadForm
    inlines = (Employee_bankInline, Employee_educationInline,
               Employee_experienceInline, PayrollInline, LeaveInline)
    exclude = ('is_deleted', 'status', 'ordinary_employee')
    min_num = 1
    # list_per_page = 10

    def has_delete_permission(self, request, obj=None):
        return False

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "facility_to_service":
            our_user = list(Custom_User.objects.filter(
                user=request.user.id).values_list("Facilities", flat=True))
            kwargs["queryset"] = Facility_to_service.objects.filter(facility__is_deleted=False).filter(
                Q(facility__facility_status='running') | Q(facility__facility_status='pending')).filter(facility__in=our_user)
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


class ProxyUser(User):  # To create a copy of users table
    class Meta:
        proxy = True
        verbose_name_plural = 'Add Employee'
        verbose_name = 'Employee'
        


@admin.register(ProxyUser)
# To modify copy of users table with our employee table
class UserAdmin1(UserAdmin, NestedModelAdmin):
    list_per_page = 10
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        our_user = request.user.id
        fac = list(Custom_User.objects.filter(
            user=our_user).values_list("Facilities", flat=True))
        emp = list(Employee.objects.filter(
            facility_to_service__facility__in=fac).values_list("user", flat=True))
        return queryset.filter(id__in=emp)
    # def get_queryset(self, request):
    #     queryset = super().get_queryset(request)
    #     return queryset.filter (first_name="")
    inlines = (EmployeeInline, )
    list_display = ('username', 'emailId', 'firstname',
                    'lastname', 'is_active', 'date_joined', 'is_staff')
    exclude = ('first_name',)
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (('Permissions'), {
            'fields': ('is_active', 'is_staff', 'groups'),
        }),
    )
    # list_per_page = 10
    # list_filter = (('facility', admin.RelatedOnlyFieldListFilter),)
    change_form_template = "change_form6.html"
    # def get_queryset(self, request):
    #     queryset = super(UserAdmin1, self).get_queryset(request)
    #     return queryset.filter(is_superuser=False)
    class Media:   
        css = {
             'all': ('/static/admin/css/style.css',)
        }

    def has_delete_permission(self, request, obj=None):
        return False

    def firstname(self, obj):
        user_id=obj.id
        q=list(Employee.objects.filter(user=user_id,is_deleted=False).values_list('first_name',flat=True))
        return q
    def lastname(self, obj):
        user_id=obj.id
        q=list(Employee.objects.filter(user=user_id,is_deleted=False).values_list('last_name',flat=True))
        return q
    def emailId(self, obj):
        user_id=obj.id
        q=list(Employee.objects.filter(user=user_id,is_deleted=False).values_list('email',flat=True))
        return q

    def get_form(self, request, obj=None, **kwargs):
        form = super(UserAdmin1, self).get_form(request, obj, **kwargs)
        # adding a User via the Admin doesn't include the permissions at first
        if 'groups' in form.base_fields:
            get_service = list(Employee.objects.filter(user=obj.id).values_list(
                "facility_to_service__service_type", flat=True))
            get_roles = list(Service_to_role.objects.filter(
                service_type__in=get_service).values_list("role", flat=True))
            # ---------------------------------------------------------------------------------------------------------------------------
            # ------------------------------------------------- TEST CODE BLOCK----------------------------------------------------------
            test1 = list(Service_to_role.objects.filter(
                service_type__in=get_service).values_list("no_of_role", flat=True))
            roles_defined = dict(zip(get_roles, test1))
            # print(roles_defined)
            test2 = list(Employee.objects.filter(user=obj.id).values_list(
                "facility_to_service", flat=True))
            # print(test2)
            test3 = list(Employee.objects.filter(
                facility_to_service__in=test2).values_list("user", flat=True))

            # print(test3)
            final_list = []
            for i in User.objects.filter(id__in=test3):
                final_list = list(i.groups.through.objects.filter(
                    user_id__in=test3).values_list("group_id", flat=True))
                break
            # print("final",final_list)
            c = Counter(final_list)
            roles_assigned = dict(c.items())
            # print(roles_assigned)
            exclude_keys = []
            include_keys = []
            for key in roles_defined.keys():
                if key in roles_assigned.keys():
                    if roles_defined[key] <= roles_assigned[key]:
                        exclude_keys.append(key)
                    else:
                        include_keys.append(key)
                else:
                    include_keys.append(key)
                    continue
            # print(f'EXCLUDE KEYS :{exclude_keys} , INCLUDE KEYS :{include_keys}')
            # --------------------------------------------------UNTESTED CODE BLOCK--------------------------------------------------------------------------
            permissions = form.base_fields['groups']
            try:
                current_user = list(User.groups.through.objects.filter(
                    user_id=obj.id).values_list("group_id", flat=True))
            except:
                current_user = list(User.groups.through.objects.filter(
                    user_id=obj.id).values_list("group_id", flat=True))
            z = set.intersection(set(exclude_keys), set(current_user))
            if len(z) > 0:
                # print('duplicates found')
                permissions.queryset = permissions.queryset.filter(
                    id__in=get_roles)
            else:
                # print('no duplicates found')
                permissions.queryset = permissions.queryset.filter(
                    id__in=include_keys).exclude(id__in=exclude_keys)
        return form

    def change_view(self, request, object_id, form_url='', extra_context=None):
        emp = list(Employee.objects.filter(user=object_id).values_list(
            "facility_to_service__service_type", flat=True))
        roles = Service_to_role.objects.filter(service_type__in=emp)
        extra_context = extra_context or {}
        extra_context['data'] = roles
        return super(UserAdmin1, self).change_view(request, object_id, form_url, extra_context=extra_context)
# ----------------------------------------------------------CREATE A NEW SHIFT TYPE-------------------------------------------------------


class RequiredInlineFormSet(BaseInlineFormSet):
    def _construct_form(self, i, **kwargs):
        form = super(RequiredInlineFormSet, self)._construct_form(i, **kwargs)
        form.empty_permitted = False
        return form


class RequiredInlineFormSet(BaseInlineFormSet):
    """
    Generates an inline formset that is required
    """

    def _construct_form(self, i, **kwargs):
        """
        Override the method to change the form attribute empty_permitted
        """
        form = super(RequiredInlineFormSet, self)._construct_form(i, **kwargs)
        form.empty_permitted = False
        return form


class Shift_type_to_dateAdmin(admin.StackedInline):
    model = Shift_type_to_date
    exclude = ('is_deleted',)
    min_num = 1
    extra = 0
    max_num = 7
    formset = RequiredInlineFormSet


@admin.register(Shift_type)
class Shift_typeAdmin(admin.ModelAdmin):
    model = Shift_type
    list_display = ('shift_name',)
    inlines = [Shift_type_to_dateAdmin]
    exclude = ('is_deleted', 'created_by',)
    actions = [delete_action]
    list_per_page = 10

    def has_delete_permission(self, request, obj=None):
        return False

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        # print(obj.shift_name,"gggggggggggggggg")
        # teamfac = Shift_type.objects.filter(shift_name=obj.shift_name).exists()
        # if teamfac:
        #     messages.set_level(request, messages.ERROR)
        #     messages.error(request, 'No changes are permitted ..')
        # else:
        #     obj.save()
        super().save_model(request, obj, form, change)

    def get_queryset(self, request):
        queryset = super(Shift_typeAdmin, self).get_queryset(request)
        our_user = request.user.id
        return queryset.filter(is_deleted=False).filter(created_by=our_user)

# ----------------------------------------------------------CREATE A NEW TEAM FOR EACH FACILITY---------------------------------------------------------------


class Team_to_employeeAdmin(admin.StackedInline):
    model = Team_to_employee
    filter_horizontal = ('employee',)
    extra = 0
    min_num = 1
    max_num = 1
    exclude = ('is_deleted',)

    def has_delete_permission(self, request, obj=None):
        return False

    def get_formset(self, request, obj=None, **kwargs):
        self.obj = obj.id
        fac = list(Team.objects.filter(
            id=obj.id).values_list("facility", flat=True))
        fac_ser = list(Facility_to_service.objects.filter(
            facility__in=fac).values_list("id", flat=True))
        self.facility = fac_ser
        return super(Team_to_employeeAdmin, self).get_formset(request, obj, **kwargs)

    def formfield_for_manytomany(self, db_field, request=None, **kwargs):
        if db_field.name == 'employee':
            emps_in_fac = list(Employee.objects.filter(status='deactive').filter(
                facility_to_service__in=self.facility).values_list("id", flat=True))
            emps_in_team = list(Team_to_employee.objects.filter(
                team=self.obj).values_list("employee", flat=True))
            kwargs["queryset"] = Employee.objects.filter(
                id__in=emps_in_fac) | Employee.objects.filter(id__in=emps_in_team)
        return super(Team_to_employeeAdmin, self).formfield_for_manytomany(db_field, request, **kwargs)


@admin.register(Team)
class TeamAdmin(admin.ModelAdmin):
    model = Team
    def get_inlines(self, request, obj=None):
        if obj:
            return [Team_to_employeeAdmin, ]
        else:
            return []
    list_display = ('team_name', 'facility',)
    exclude = ('is_deleted',)
    list_filter = (('facility', admin.RelatedOnlyFieldListFilter),)
    actions = [delete_action]
    list_per_page = 10

    def save_model(self, request, obj, form, change):
        global shiftcheck
        print(change,"hhhhhhhhhhhhhhh")
        if change:
            shiftcheck=0
            emps_in_team = list(Team_to_employee.objects.filter(team=obj.id).values_list("employee", flat=True))
            a = []
            for i in (request.POST.getlist('team_to_employee_set-0-employee')):
                a.append(int(i))
                Employee.objects.filter(pk=int(i)).update(status='active')
            for item in emps_in_team:
                if item not in a:
                    Employee.objects.filter(pk=item).update(status='deactive')
            super().save_model(request, obj, form, change)
        else:
            shiftcheck=1
            team_name = obj.team_name
            facility = obj.facility
            shift_name = obj.shift_name
            teamfac = Team.objects.filter(team_name=team_name,facility=facility,shift_name=shift_name).exists()
            print(teamfac,11111111111111111111111)
            if teamfac:
                messages.error(request,'Team with this name already exist in this facility and shift!')
                return redirect('/admin/HR/team/add/')
            else:
                shiftcheck=0
                emps_in_team = list(Team_to_employee.objects.filter(team=obj.id).values_list("employee", flat=True))
                a = []
                for i in (request.POST.getlist('team_to_employee_set-0-employee')):
                    a.append(int(i))
                    Employee.objects.filter(pk=int(i)).update(status='active')
                for item in emps_in_team:
                    if item not in a:
                        Employee.objects.filter(pk=item).update(status='deactive')
                super().save_model(request, obj, form, change)

    def has_delete_permission(self, request, obj=None):
        return False

    def get_queryset(self, request):
        our_user = list(Custom_User.objects.filter(
            user=request.user.id).values_list("Facilities", flat=True))
        queryset = super(TeamAdmin, self).get_queryset(request)
        return queryset.filter(is_deleted=False).filter(facility__in=our_user)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "facility":
            our_user = list(Custom_User.objects.filter(
                user=request.user.id).values_list("Facilities", flat=True))
            kwargs["queryset"] = Facility.objects.filter(
                is_deleted=False).filter(id__in=our_user)
        if db_field.name == "shift_name":
            kwargs["queryset"] = Shift_type.objects.filter(
                is_deleted=False).filter(created_by=request.user.id)
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def response_add(self, request, obj):
        if shiftcheck==0:
            url = f"/admin/HR/team/{obj.id}/change/"
            return redirect(url)
        else:
            return redirect('/admin/HR/team/add/')



    def get_readonly_fields(self, request, obj=None):
        if obj:  # editing an existing object
            return self.readonly_fields + ('team_name', 'facility', 'shift_name', 'note')
        return self.readonly_fields
# ----------------------------------------------------------CREATE A PAYROLL COMPONENT----------------------------------------------------------------------




class payrollForm(forms.ModelForm):
     amount = forms.DecimalField(required=False, min_value=1.0)

@ admin.register(Payroll_component)
class Payroll_componentAdmin(admin.ModelAdmin):
    model = Payroll_component
    list_display = ('component_name', 'component_type',)
    exclude = ('is_deleted', 'created_by',)
    actions = [delete_action]
    list_per_page = 10

    form = payrollForm
    def has_delete_permission(self, request, obj=None):
        return False

    # def save_model(self, request, obj, form, change):
    #     obj.created_by = request.user
    #     super().save_model(request, obj, form, change)
    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        global shiftcheck
        print(change,"hhhhhhhhhhhhhhh")
        if change:
            component_id = obj.id
            component_name = obj.component_name
            payrollcomp = Payroll_component.objects.filter(component_name=component_name,is_deleted=False).exclude(id=component_id).exists()
            print(payrollcomp,11111111111111111111111)
            if payrollcomp:
                shiftcheck=1
                print(payrollcomp,"hhhhhhhhhhhhhhh")
                messages.error(request,'Payroll component with this name already exist!')
                return redirect('/admin/HR/payroll_component/{obj.id}/change/')
            else:
                shiftcheck=0
                super().save_model(request, obj, form, change)
        else:
            component_name = obj.component_name
            payrollcomp = Payroll_component.objects.filter(component_name=component_name,is_deleted=False).exists()
            print(payrollcomp,11111111111111111111111)
            if payrollcomp:
                shiftcheck=2
                print(payrollcomp,"hhhhhhhhhhhhhhh")
                messages.error(request,'Payroll component with this name already exist!')
                return redirect('/admin/HR/payroll_component/add/')
            else:
                shiftcheck=0
                super().save_model(request, obj, form, change)
    def response_add(self, request, obj):
        if shiftcheck==1:
            url = f"/admin/HR/payroll_component/{obj.id}/change/"
            return redirect(url)
        elif shiftcheck==2:
            return redirect('/admin/HR/payroll_component/add/')
        else:            
            return redirect('/admin/HR/payroll_component/{obj.id}/change/')
    def response_change(self, request, obj):
        url = f"/admin/HR/payroll_component/{obj.id}/change/"
        return redirect(url)



    def get_queryset(self, request):
        queryset = super(Payroll_componentAdmin, self).get_queryset(request)
        our_user = request.user.id
        return queryset.filter(is_deleted=False).filter(created_by=our_user)



# @admin.register(Payroll_component)
# class Payroll_componentAdmin(admin.ModelAdmin):
#     model = Payroll_component
#     list_display = ('component_name', 'component_type',)
#     exclude = ('is_deleted', 'created_by',)
#     actions = [delete_action]
#     form = payrollForm
#     list_per_page = 1

    # def has_delete_permission(self, request, obj=None):
    #     return False


# ---------------------------------------------------------------------PAYRUN------------------------------------------------------------------------------


class PayrunProxy(Employee):
    class Meta:
        proxy = True
        verbose_name = 'payrun'  # object name here
        verbose_name_plural = 'Payrun'  # plural object names here


@admin.register(PayrunProxy)
class PayrunAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'work_title', 'facility_to_service')
    fields = [('first_name'), ('last_name')]
    readonly_fields = [('first_name'), ('last_name')]
    change_form_template = "change_form3.html"
    list_per_page = 10

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj=None):
        return False

    def change_view(self, request, object_id, form_url='', extra_context=None):
        # check payroll table with employee id
        check_payroll = Payroll.objects.filter(employee=object_id)
        payroll_id = check_payroll[0].id  # get id from above queryset
        # daily pay of the particular employee
        Monthly_pay = check_payroll[0].Monthly_pay
        # ---------------------------------------TEST----------------------------------------------
        # -----------------------------------------------------------------------------------------
        # ----------------NEW CODE REPLACING FOR BLOCK----------------------------#----------------------------------------------------------------------------------------
        check_comps00 = list(Payroll.Payroll_component.through.objects.filter(
            payroll_id=payroll_id).values_list('payroll_component_id', flat=True))
        get_comps00 = Payroll_component.objects.filter(id__in=check_comps00)
        get_comps11 = list(Payroll_component.objects.filter(id__in=check_comps00).filter(
            component_type="Addition").values_list('amount', flat=True))
        get_comps22 = list(Payroll_component.objects.filter(id__in=check_comps00).filter(
            component_type="Deduction").values_list('amount', flat=True))
        amt_to_add00 = sum(get_comps11)
        amt_to_minus00 = sum(get_comps22)
        # ---------------------------------------------------------------------------------------------------------------------------------------------------
        extra_context = extra_context or {}
        extra_context['test'] = get_comps00
        extra_context['pay'] = Monthly_pay  # to send salary data to html
        attendance_check = Attendance.objects.filter(
            employee_id=object_id, salary_mark='notcleared')
        attendance_check0 = list(Attendance.objects.filter(
            employee_id=object_id, salary_mark='notcleared', attendance_mark='no').values_list('attendance_mark', flat=True))
        no_of_absence = len(attendance_check0)
        if attendance_check:
            salary = math.ceil(Monthly_pay-((Monthly_pay/30)*no_of_absence))
            # to send total salary per working days to html
            extra_context['salary'] = salary
            # to send no of absent days to html
            extra_context['no_of_absence'] = no_of_absence
            #------------------------------------------------------------------------------#
            total_payable = math.ceil((salary+amt_to_add00)-(amt_to_minus00))
            # to send final amount to be payed as salary to employee
            extra_context['total_payable'] = total_payable
            #------------------------------------------------------------------------------#
            self.total_payable = total_payable
            self.amt_to_add00 = amt_to_add00
            self.amt_to_minus00 = amt_to_minus00
            #------------------------------------------------------------------------------#
            return super().change_view(request, object_id, form_url, extra_context=extra_context)
        else:
            return super().change_view(request, object_id, form_url, extra_context=extra_context)

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        context.update({
            'show_save_and_continue': False,
            'show_save': False,
        })
        return super().render_change_form(request, context, add, change, form_url, obj)

    def response_change(self, request, obj):
        if "apply3" in request.POST:
            save_payrun = Payrun(employee_id=obj.id, total_addition=self.amt_to_add00, total_deduction=self.amt_to_minus00,
                                 gross_payable=self.total_payable, status="cleared", clearing_date=datetime.today())
            Attendance.objects.filter(
                employee_id=obj).update(salary_mark="cleared")
            save_payrun.save()
            self.payrun = save_payrun.id
            url = "/admin/HR/payrun/"+str(self.payrun)+"/change"
            return redirect(url)
            # return HttpResponseRedirect(".")
        return super().response_change(request, obj)

    def get_queryset(self, request):
        queryset = super(PayrunAdmin, self).get_queryset(request)
        our_user = list(Custom_User.objects.filter(
            user=request.user.id).values_list("Facilities", flat=True))
        return queryset.filter(is_deleted=False).filter(facility_to_service__facility__facility_status="running").filter(facility_to_service__facility__in=our_user)


class Stackpayrun(admin.TabularInline):
    model = Payrunbonus
    exclude = ('is_deleted',)
    min_num = 0
    extra = 0
    # def has_delete_permission(self, request, obj=None):
    #     return False

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "payroll_component":
            kwargs["queryset"] = Payroll_component.objects.filter(
                given_at="Employee payroll")
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


@admin.register(Payrun)
class payrunnewAdmin(admin.ModelAdmin):
    def get_model_perms(self, request):
        """
        Return empty perms dict thus hiding the model from admin index.
        """
        return {}
    inlines = (Stackpayrun,)
    exclude = ('is_deleted', 'status', 'clearing_date', 'extra_bonus_or_fine',)
    readonly_fields = ('employee', 'total_addition',
                       'total_deduction', 'gross_payable',)

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj=None):
        return False

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        amt = []
        for instance in instances:
            comp = instance.payroll_component.id
            times = instance.times
            value = list(Payroll_component.objects.filter(
                id=comp).values_list("component_type", "amount"))
            for a, b in value:
                if a == "Addition":
                    amt.append(b*times)
                else:
                    amt.append(-1*(b*times))
            instance.user = request.user
            instance.save()
        Payrun.objects.filter(id=self.object_id).update(
            extra_bonus_or_fine=sum(amt), gross_payable=self.payable+sum(amt))
        formset.save_m2m()

    def save_model(self, request, obj, form, change):
        self.object_id = obj.id
        self.payable = obj.gross_payable
        super(payrunnewAdmin, self).save_model(request, obj, form, change)

    def response_change(self, request, obj):
        return redirect('/admin/HR/payrunproxy/')

# --------------------------------------------------------Attendence marking DUMMY---------------------------------------------------------------------------


class AuthorAdmin(admin.ModelAdmin):
    pass
# ------------------------------------LEAVE MODULE LIYA-----------------------------------------------------------------------------------------------------------------------


class LeaveAdmin(admin.ModelAdmin):
    model = Leave
    exclude = ('is_deleted', 'created_by',)
    list_per_page = 10

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        user1 = request.user.id
        # our_user = list(Custom_User.objects.filter(user=user1).values_list("Facilities__id",flat=True))
        return queryset.filter(is_deleted=False).filter(created_by=user1)

    def save_model(self, request, obj, form, change):
        p = request.user
        obj.created_by = p
        obj.save()
        super().save_model(request, obj, form, change)


admin.site.register(Leave, LeaveAdmin)

# class serviceFilter(admin.SimpleListFilter):  #MAKING A CUSTOM FILTER TO ONLY SHOW RUNNING AND STOPPED FACILITIES
#     title = ('Service wise')
#     parameter_name = 'service'
#     def lookups(self, request, model_admin):
#         ser=Service_type.objects.all()
#         return ser
#     def queryset(self, request, queryset):
#         if self.value() is None:
#             self.used_parameters[self.parameter_name] = Service_type.objects.all()
#             return "yes"
#         else:
#             # self.used_parameters[self.parameter_name] = 2
#             return queryset.filter(facility_to_service__service_type__service_type="4")


class leave_requestAdmin(admin.ModelAdmin):
    model = leave_request
    # readonly_fields = ['leave_from_date','leave_to_date','Reason',]
    exclude = ('reason_reject', 'status', 'is_deleted',
               'user_id', 'ordinary_employee',)
    list_display = ('employee_id', 'Reason', 'status', 'reason_reject')
    change_form_template = "changeview.html"
    list_per_page = 10
    # list_filter = (serviceFilter,)

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        p = request.user.id
        our_user = list(Custom_User.objects.filter(
            user=p).values_list("Facilities__id", flat=True))
        return queryset.filter(is_deleted=False).filter(employee_id__facility_to_service__facility__in=our_user).filter(status=2)
        # return queryset.filter(is_deleted=False).filter(employee_id__facility_to_service__facility__in=our_user)

    def change_view(self, request, object_id, form_url='', extra_context=None):
        emp = list(leave_request.objects.filter(
            request_id=object_id).values_list("employee_id", flat=True))
        leaves = Assign_leave.objects.filter(employee__in=emp).values_list(
            "leave_id__name", "leave_id__number_of_days", "leave_id__criteria")
        extra_context = extra_context or {}
        extra_context['leave'] = leaves
        return super().change_view(request, object_id, form_url, extra_context=extra_context)

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj=None):
        return False

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        context.update({
            'show_save_and_continue': False,
            'show_save_and_add_another': False,
            'show_save': False,
        })
        return super().render_change_form(request, context, add, change, form_url, obj)

    def response_change(self, request, obj):
        if "Attendancereport" in request.POST:
            empid = obj.employee_id.id
            s = Attendance.objects.filter(employee_id=empid)
            return render(request, 'contactform.html', {'form': s})
        if "test" in request.POST:
            obj.user = request.user
            # if (obj.user).has_perm('HR.can_approve_leave_request'):
            id = obj.request_id
            reason = ""
            reasons = request.POST.get("test")
            self.model.objects.filter(request_id=id).update(
                reason_reject=reasons, status=0)
            # self.message_user(request, "Leave rejected")
            Notifi=Notifications()
            Notifi.title="Approve leave"
            Notifi.description="Leave request approved"
            Notifi.save()
            notifi_read=Mark_as_read()
            notifi_read.Notification_id=Notifi.id
            notifi_read.read_status="unread"
            notifi_read.save()
            empidre = obj.employee_id.id
            shift=list(Team_to_employee.objects.filter(employee=empidre).values_list('team__shift_name',flat=True))
            userid=Manager_reports.objects.get(shift_name_id=shift[0]).manager_id
            notifi_read.recipient.add(Employee.objects.get(user=userid))
        if "_make-unique" in request.POST:
            # print(request.POST, ".............................................test")
            obj.user = request.user
            l = obj.user
            id = obj.request_id
            self.model.objects.filter(request_id=id).update(
                status=1, reason_reject="")
            Notifi=Notifications()
            Notifi.title="shift ended"
            Notifi.description="shift of previous manager ended"
            Notifi.save()
            notifi_read=Mark_as_read()
            notifi_read.Notification_id=Notifi.id
            notifi_read.read_status="unread"
            notifi_read.save()
            notifi_read.recipient.add(Employee.objects.get(user=userid))
            # print("successs")
            # self.message_user(request, "Leave approved")
            # return HttpResponseRedirect("../")
        return super().response_change(request, obj)


admin.site.register(leave_request, leave_requestAdmin)

# ------------------------------------FACILITY DETAILED VIEW LIYA-----------------------------------------------------------------------------------------------------------------------


class FacilityDetailedViewProxy(Facility):
    class Meta:
        proxy = True
        verbose_name = 'Facilities Pending and running'  # object name here
        verbose_name_plural = 'Facilities Detailed View'  # plural object names here


@admin.register(FacilityDetailedViewProxy)
class FacilityDetailedViewProxyAdmin(admin.ModelAdmin):
    list_display = ('facility_name', 'facility_status',)
    readonly_fields = [('facility_name'), ('state'), ('District'), ('pincode'),
                       ('longitude'), ('latitude'), ('Address'), ('facility_status')]
    exclude = ('is_deleted', 'sts',)
    change_form_template = "change_form3_liya.html"
    list_per_page = 10

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj=None):
        return False

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        our_user = list(Custom_User.objects.filter(
            user=request.user.id).values_list("Facilities", flat=True))
        return queryset.filter(facility_status='running').filter(id__in=our_user)

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        context.update({
            'show_save_and_continue': False,
            'show_save': False,
        })
        return super().render_change_form(request, context, add, change, form_url, obj)

    def change_view(self, request, object_id, form_url='', extra_context=None):
        x = Facility_to_service.objects.filter(facility=object_id)
        teams = Team.objects.filter(facility=object_id)
        extra_context = extra_context or {}
        extra_context['osm_data'] = x
        extra_context['team'] = teams
        a = Employee.objects.filter(
            facility_to_service__in=x.values_list('id', flat=True))
        extra_context['opm_data'] = a

        ser_type = Facility_to_service.objects.values_list(
            'service_type').filter(facility=object_id)
        ser_role = Service_to_role.objects.filter(service_type__in=ser_type)
        # print(ser_role)
        extra_context['service_role'] = ser_role

        return super(FacilityDetailedViewProxyAdmin, self).change_view(request, object_id,
                                                                       form_url, extra_context=extra_context)

# ------------------------------------salary summary report LIYA-----------------------------------------------------------------------------------------------------------------------


class SalarysummaryViewProxy(Facility):
    class Meta:
        proxy = True
        verbose_name = 'salary summary report'  # object name here
        verbose_name_plural = 'salary summary report'  # plural object names here


@admin.register(SalarysummaryViewProxy)
class SalarysummaryViewProxyAdmin(admin.ModelAdmin):
    list_display = ('facility_name', 'facility_status',)
    fields = [('facility_name')]
    readonly_fields = [('facility_name')]
    exclude = ('is_deleted', 'sts',)
    my_id_for_formfield = None
    change_form_template = "salarysummary.html"
    list_per_page = 10

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj=None):
        return False

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        p = request.user.id
        our_user = list(Custom_User.objects.filter(
            user=p).values_list("Facilities__id", flat=True))
        return queryset.filter(is_deleted=False).filter(facility_status='running').filter(id__in=our_user)
        # return queryset.filter(facility_status='running')

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        context.update({
            'show_save_and_continue': False,
            'show_save': False,
        })
        return super().render_change_form(request, context, add, change, form_url, obj)

    def response_change(self, request, obj):
        # if "test" in request.POST:
        if "test1" in request.POST or "test2" in request.POST:
            from_date = request.POST["test1"]
            to_date = request.POST["test2"]
            # mon=request.POST["test"]
            # print(mon,"monnnnnnnnnnn")
            if(from_date == "" or to_date == ""):
                self.my_id_for_formfield = None
                # print(self.my_id_for_formfield,
                #       "idddddddddddddddddddddddddddddddddddddddddd")
                return redirect(f'/admin/HR/salarysummaryviewproxy/{obj.id}/change/')
            else:
                x = Facility_to_service.objects.filter(facility=obj)
                user_details = Employee.objects.filter(
                    facility_to_service__in=x.values_list('id', flat=True))
                o = Payrun.objects.filter(
                    employee_id__in=user_details.values_list('id', flat=True))

                # p=o.filter(clearing_date=mon)
                # (date__range=["2020-01-01", "2020-01-31"])
                p = o.filter(clearing_date__range=[from_date, to_date])
                # print(p, "pppppppppppppppppppppp")
                self.my_id_for_formfield = p
                return redirect(f'/admin/HR/salarysummaryviewproxy/{obj.id}/change/')
        return super().response_change(request, obj)

    def change_view(self, request, object_id, form_url='', extra_context=None):

        x = Facility_to_service.objects.filter(facility=object_id)
        user_details = Employee.objects.filter(
            facility_to_service__in=x.values_list('id', flat=True))
        # print(user_details, "employessssssssssssssssssss")
        o = Payrun.objects.filter(
            employee_id__in=user_details.values_list('id', flat=True))
        # print(o, "payrunnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn")
        # sumof=Payrun.objects.filter(employee_id__in=user_details).aggregate(Sum('gross_payable')).get('gross_payable__sum', 0.00)
        object1 = self.my_id_for_formfield
        # print(object1, "objjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj")
        if object1 == None:
            # print(object1, "objectttttttttttttttttttttttttt")
            extra_context = extra_context or {}
            extra_context['s'] = o
        else:
            month = self.my_id_for_formfield
            extra_context = extra_context or {}
            extra_context['s'] = month
            # print("successsssssssssssssssssssssssssssssssss")
            self.my_id_for_formfield = None
        return super(SalarysummaryViewProxyAdmin, self).change_view(request, object_id, form_url, extra_context=extra_context)


# --------------------------------------------MANAGE INCIDENTS--------------------------------------------------------------------------------------
class IncidentViewProxy(Incident_Assignment):
    class Meta:
        proxy = True
        verbose_name = 'Incidents reported'  # object name here
        verbose_name_plural = 'Incidents'  # plural object names here


@admin.register(IncidentViewProxy)
class IncidentViewProxyAdmin(admin.ModelAdmin):
    list_display = ('ticket_id', 'assign_date',)
    exclude = ('is_deleted', 'assign_to', 'assignee_status', 'assign_status',)
    readonly_fields = (('ticket_id'), ('assign_date'),)
    change_form_template = "incidents.html"
    list_per_page = 10

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj=None):
        return False

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        context.update({
            'show_save_and_continue': False,
            'show_save': False,
        })
        return super().render_change_form(request, context, add, change, form_url, obj)

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        our_user = list(Custom_User.objects.filter(
            user=request.user.id).values_list("Facilities", flat=True))
        return queryset.filter(assign_to='HR').filter(is_deleted=False).filter(assignee_status=False).filter(ticket_id__incident_id__initiator_id__facility_to_service__facility__in=our_user)

    def change_view(self, request, object_id, form_url='', extra_context=None):
        ticket = Incident_Assignment.objects.filter(id=object_id).values("ticket_id__incident_id__title", "ticket_id__incident_id__description",
                                                                         "ticket_id__incident_id__initiated_at", "ticket_id__incident_id__priority", "ticket_id__incident_id__initiator_id__first_name", "ticket_id__incident_id__initiator_id__work_title",
                                                                         "ticket_id__incident_id__initiator_id__phonenumber", "ticket_id__incident_id__initiator_id__facility_to_service__facility__facility_name")
        extra_context = extra_context or {}
        extra_context['details'] = ticket
        return super(IncidentViewProxyAdmin, self).change_view(request, object_id, form_url, extra_context=extra_context)

    def response_change(self, request, obj):
        Incident_Assignment.objects.filter(
            id=obj.id).update(assignee_status=True)
        return super().response_change(request, obj)


# /////////////////////////////////////////////////////// HR Leave_request ////////////////////////////////////////////

class Leave_requestProxy(leave_request):
    class Meta:
        proxy = True
        verbose_name = 'Apply leave request'  # object name here
        verbose_name_plural = 'Apply leave request'  # plural object names here


@admin.register(Leave_requestProxy)
class Leave_requestProxyAdmin(admin.ModelAdmin):
    list_display = ('leave_from_date', 'leave_to_date', 'Reason', 'status')
    fields = [('leave_from_date'), ('leave_to_date'), ('Reason'), ]
    list_per_page = 10
    readonly_fields = ('reason_reject',)
    # change_form_template = "requestleave.html"

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        p = request.user.id
        cusid = Custom_User.objects.get(user=p).id
        return queryset.filter(is_deleted=False).filter(user_id=cusid)

    def save_model(self, request, obj, form, change):
        uid = request.user.id
        cusid = Custom_User.objects.get(user=uid)
        obj.user_id = cusid
        obj.ordinary_employee = False
        obj.save()
        super().save_model(request, obj, form, change)
    # def change_view(self, request, object_id, form_url='', extra_context=None):
    #     extra_context = extra_context or {}
    #     extra_context['s'] = "hlooo"
    #     return super(Leave_requestProxyAdmin, self).change_view(request, object_id,form_url, extra_context=extra_context)

    def get_fields(self, request, obj=None):
        if obj:
            if obj.status == 0:
                # c=obj.id
                # qr=Custom_User.objects.get(user=c)
                # h=qr.role
                # reason=self.objects.values_list('Facilities', flat=True).filter(id=c)
                # self.ls=Custom_User.objects.values_list('Facilities', flat=True).filter(user=c)

                return self.fields + [('reason_reject',)]
        return self.fields


# -----------------------To Create warehouse employees and assign them to facilities---------------------------- (EXPANSION OF USER TABLE)


# class WarehouseEmployeeInline(NestedStackedInline): #employee basic details are entered
#     model = Employee
#     inlines = (Employee_bankInline,Employee_educationInline,Employee_experienceInline,PayrollInline,LeaveInline )
#     exclude = ('is_deleted','status','facility_to_service','ordinary_employee')
#     min_num = 1
#     def has_delete_permission(self, request, obj=None):
#         return False
#     # def formfield_for_foreignkey(self, db_field, request, **kwargs):
#     #     if db_field.name == "warehouse":
#     #         our_user = list(Custom_User.objects.filter(user=request.user.id).values_list("Facilities",flat=True))
#     #         kwargs["queryset"] = Facility_to_service.objects.filter(facility__is_deleted=False).filter(Q(facility__facility_status='running') | Q(facility__facility_status='pending')).filter(facility__in=our_user)
#     #     return super().formfield_for_foreignkey(db_field, request, **kwargs)

# //////////////////////////////////////////////////////////////////////////////////////////////////////////

# class ProxyUserwarehouse(User): #To create a copy of users table
#     class Meta:
#         proxy = True
#         verbose_name_plural = 'Add Warehouse Employee'
#         verbose_name = 'Warehouse Employee'

# @admin.register(ProxyUserwarehouse)
# class UserAdmin2(UserAdmin,NestedModelAdmin):  #To modify copy of users table with our employee table
#     def get_queryset(self, request):
#         queryset = super().get_queryset(request)
#         our_user = request.user.id
#         ware = list(Custom_User.objects.filter(user=our_user).values_list("warehouse",flat=True))
#         emp = list(Employee.objects.filter(warehouse__in=ware).values_list("user",flat=True))
#         return queryset.filter (id__in=emp)
#     # def get_queryset(self, request):
#     #     queryset = super().get_queryset(request)
#     #     return queryset.filter (first_name="")
#     inlines = (WarehouseEmployeeInline, )
#     list_display = ('username','email', 'first_name', 'last_name', 'is_active', 'date_joined', 'is_staff')
#     exclude = ('first_name',)
#     fieldsets = (
#         (None, {'fields': ('username', 'password')}),
#         (('Permissions'), {
#             'fields': ('is_active', 'is_staff', 'groups'),
#         }),
#     )
#     list_per_page=20
#     change_form_template = "change_form7.html"
#     # def get_queryset(self, request):
#     #     queryset = super(UserAdmin1, self).get_queryset(request)
#     #     return queryset.filter(is_superuser=False)
#     def has_delete_permission(self, request, obj=None):
#         return False
    # def get_form(self, request, obj=None, **kwargs):
    #     form = super(UserAdmin2, self).get_form(request, obj, **kwargs)
    #     # adding a User via the Admin doesn't include the permissions at first
    #     if 'groups' in form.base_fields:
    #         get_service = list(Employee.objects.filter(user=obj.id).values_list("facility_to_service__service_type",flat=True))
    #         get_roles = list(Service_to_role.objects.filter(service_type__in=get_service).values_list("role",flat=True))
    #         #---------------------------------------------------------------------------------------------------------------------------
    #         #------------------------------------------------- TEST CODE BLOCK----------------------------------------------------------
    #         test1 = list(Service_to_role.objects.filter(service_type__in=get_service).values_list("no_of_role",flat=True))
    #         roles_defined = dict(zip(get_roles,test1))
    #         # print(roles_defined)
    #         test2 = list(Employee.objects.filter(user=obj.id).values_list("facility_to_service",flat=True))
    #         # print(test2)
    #         test3 = list(Employee.objects.filter(facility_to_service__in=test2).values_list("user",flat=True))

    #         # print(test3)
    #         final_list=[]
    #         for i in User.objects.filter(id__in=test3):
    #             final_list = list(i.groups.through.objects.filter(user_id__in=test3).values_list("group_id",flat=True))
    #             break
    #         # print("final",final_list)
    #         c= Counter(final_list)
    #         roles_assigned = dict(c.items())
    #         # print(roles_assigned)
    #         exclude_keys = []
    #         include_keys = []
    #         for key in roles_defined.keys():
    #             if key in roles_assigned.keys():
    #                 if roles_defined[key] <= roles_assigned[key]:
    #                     exclude_keys.append(key)
    #                 else:
    #                     include_keys.append(key)
    #             else:
    #                 include_keys.append(key)
    #                 continue
    #         # print(f'EXCLUDE KEYS :{exclude_keys} , INCLUDE KEYS :{include_keys}')
    #         #--------------------------------------------------UNTESTED CODE BLOCK--------------------------------------------------------------------------
    #         permissions = form.base_fields['groups']
    #         try:
    #             current_user = list(User.groups.through.objects.filter(user_id=obj.id).values_list("group_id",flat=True))
    #         except:
    #             current_user = list(User.groups.through.objects.filter(user_id=obj.id).values_list("group_id",flat=True))
    #         z=set.intersection(set(exclude_keys),set(current_user))
    #         if len(z) > 0:
    #             # print('duplicates found')
    #             permissions.queryset = permissions.queryset.filter(id__in=get_roles)
    #         else:
    #             # print('no duplicates found')
    #             permissions.queryset = permissions.queryset.filter(id__in=include_keys).exclude(id__in=exclude_keys)
    #     return form
    # def change_view(self, request, object_id, form_url='', extra_context=None):
    #     print(object_id,"hiiiiiiiiiiiiiiii")
    #     emp = Employee.objects.filter(user=object_id).values_list("warehouse",flat=True)
    #     print(emp,"employeeeeeeeeeee")
    #     # ware = Warehouse_details.objects.filter(id__in=emp).values_list("id",flat=True)
    #     # roles = Warehouse_to_role.objects.filter(warehouse__in=ware)
    #     print(roles,"rolesssssss")
    #     # users = User.objects.get(id=object_id)
    #     # print(users,"usersssssssssssss")
    #     extra_context = extra_context or {}
    #     extra_context['data'] =roles
    #     return super(UserAdmin2, self).change_view(request, object_id, form_url, extra_context=extra_context)

# /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


# class CharityFilter(admin.SimpleListFilter):
#     title = ('Facility to service')
#     parameter_name = 'facility_to_service'
#     def lookups(self, request, model_admin):
#         # queryset = model_admin.queryset(request).filter(organisation__type='CHARITY')
#         our_user = request.user.id
#         fac = list(Custom_User.objects.filter(user=our_user).values_list("Facilities",flat=True))
#         emp = model_admin.queryset(request).filter(facility_to_service__facility__in=fac).values_list("user",flat=True)
#         return emp.values_list('facility_to_service')

#     def queryset(self, request, queryset):
#         if self.value():
#           return queryset.filter(facility_to_service=self.value())

class AttendanceDetailedViewProxy(Employee):
    class Meta:
        proxy = True
        verbose_name = 'Employee Attendance'  # object name here
        verbose_name_plural = 'Employee Attendance'  # plural object names here
        

from django.conf import settings
@admin.register(AttendanceDetailedViewProxy)
class AttendanceDetailedViewProxyAdmin(admin.ModelAdmin):
    list_display = ('user', 'first_name', 'email', 'phonenumber',)
    # readonly_fields = [('facility_name'),('state'),('District'),('pincode'),('longitude'),('latitude'),('Address'),('facility_status')]
    exclude = ('is_deleted', 'sts',)
    my_id_for_formfield = None
    change_form_template = "attendanceview.html"
    list_per_page = 10
    list_filter = (
        ('facility_to_service', admin.RelatedOnlyFieldListFilter),
    )
    # list_filter = (CharityFilter,)
    fields = [('user'), ]

    # list_per_page = 10


    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj=None):
        return False

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        our_user = request.user.id
        faci = list(Custom_User.objects.filter(
            user=our_user).values_list("Facilities", flat=True))
        empy = list(Employee.objects.filter(
            facility_to_service__facility__in=faci).values_list("id", flat=True))
        emplist = queryset.filter(id__in=empy)
        return emplist

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        context.update({
            'show_save_and_continue': False,
            'show_save': False,
        })
        return super().render_change_form(request, context, add, change, form_url, obj)

    def response_change(self, request, obj):
        if "attend1" in request.POST or "attend2" in request.POST:
            from_date = request.POST["attend1"]
            to_date = request.POST["attend2"]
            if(from_date == "" or to_date == ""):
                self.my_id_for_formfield = None
                return redirect(f'/admin/HR/attendancedetailedviewproxy/{obj.id}/change/')
            else:
                # id=obj.id
                attendance = Attendance.objects.filter(employee_id=obj)
                p = attendance.filter(date__range=[from_date, to_date])
                self.my_id_for_formfield = p
                return redirect(f'/admin/HR/attendancedetailedviewproxy/{obj.id}/change/')
        return super().response_change(request, obj)

    def change_view(self, request, object_id, form_url='', extra_context=None):
        attendance = Attendance.objects.filter(employee_id=object_id)
        object1 = self.my_id_for_formfield
        if object1 == None:
            extra_context = extra_context or {}
            extra_context['emp_attend'] = attendance
        else:
            month = self.my_id_for_formfield
            extra_context = extra_context or {}
            extra_context['emp_attend'] = month
            self.my_id_for_formfield = None
        return super(AttendanceDetailedViewProxyAdmin, self).change_view(request, object_id, form_url, extra_context=extra_context)


admin.site.register(Attendance)
