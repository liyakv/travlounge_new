from datetime import date
from django.db import models
from IT_ADMIN.models import Facility, BaseModel, Facility_to_service, Custom_User
from django.core.validators import MaxValueValidator,MinValueValidator
from django.contrib.auth.models import User, Group
import cloudinary
from cloudinary.models import CloudinaryField
from django.core.validators import FileExtensionValidator
from django.utils import timezone
from django.core.exceptions import ValidationError
# from .admin import TeamAdmin

# Create your models here.

# ----------------------------------------------------------Employee---------------------------------------------------------------

BLOOD_CHOICES = (("A+", "A+"),
                 ("A-", "A-"),
                 ("B+", "B+"),
                 ("B-", "B-"),
                 ("O+", "O+"),
                 ("O-", "O-"),
                 ("AB+", "AB+"),
                 ("AB-", "AB-"),)

STATUS = (("active", "active"),
          ("deactive", "deactive"),)

def validate_date(date):
    if date < timezone.now().date():
        raise ValidationError("Date cannot be in the past")

class Employee(BaseModel):  # Add employees basic details
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=60)
    middle_name = models.CharField(max_length=60, null=True, blank=True)
    last_name = models.CharField(max_length=60)
    dob = models.DateField()
    blood_group = models.CharField(max_length=3, choices=BLOOD_CHOICES)
    email = models.EmailField(
        max_length=60, blank=True, null=True, unique=True)
    phonenumber = models.BigIntegerField(
        unique=True, validators=[MaxValueValidator(9999999999),MinValueValidator(1000000000)])
    state = models.CharField(max_length=60)
    district = models.CharField(max_length=60)
    address = models.TextField(max_length=300)
    # image =CloudinaryField('Employee photo', help_text="Upload a formal passport size image of employee here (only in JPG or PNG format)",validators=[FileExtensionValidator(allowed_extensions=['jpg','png'])])
    image = CloudinaryField(
        'Employee photo', help_text="Upload a formal passport size image of employee here (only in JPG or PNG format)")
    work_title = models.CharField(max_length=60)
    # identitiy = CloudinaryField('employee_identity', help_text="Upload a pdf file with one or more identity details inside",validators=[FileExtensionValidator(allowed_extensions=['pdf'])])
    identitiy = CloudinaryField(
        'employee identity', help_text="Upload a pdf file with one or more identity details inside")
    facility_to_service = models.ForeignKey(
        Facility_to_service, on_delete=models.CASCADE, null=True)
    account_expiry_date = models.DateField(blank=True, null=True, validators=[validate_date])
    status = models.CharField(
        max_length=10, default="deactive", choices=STATUS)
    # facility employee is default true and warehouse becomes false
    ordinary_employee = models.BooleanField(default=True)
    # warehouse = models.ForeignKey(
    #     Warehouse_details, on_delete=models.CASCADE, null=True)

    def __str__(self):
        if self.facility_to_service is not None:
            return self.first_name + " - " + self.work_title + " - " + self.facility_to_service.facility.facility_name + " - " + self.facility_to_service.service_type.service_type
        # else:
        #     return self.first_name + " - " + self.work_title + " - " + self.warehouse.name


class Employee_bank(BaseModel):  # Add employees bank details (one to one table)
    employee = models.OneToOneField(Employee, on_delete=models.CASCADE)
    account_name = models.CharField(max_length=60)
    account_number = models.BigIntegerField(
        validators=[MaxValueValidator(999999999999999999)],unique=True)
    ifs_code = models.CharField(max_length=60, verbose_name="IFSC Code")
    bank_name = models.CharField(max_length=60)
    branch = models.CharField(max_length=60)

    class Meta:
        verbose_name = 'Employee Bank Detail'

    def __str__(self):
        return self.employee.first_name


# Add employees education details (one to many table)
class Employee_education(BaseModel):
    employee = models.ForeignKey(Employee, on_delete=models.CASCADE)
    course_title = models.CharField(max_length=60)
    university_or_board_name = models.CharField(max_length=60)
    percentage = models.PositiveIntegerField(
        validators=[MaxValueValidator(999)])
    certificate = CloudinaryField('Docs/employee_education')

    class Meta:
        verbose_name = 'Employee Education Detail'
    def __str__(self):
        return self.employee.first_name


# Add employees work experience details (one to many table)
class Employee_experience(BaseModel):
    Employee = models.ForeignKey(Employee, on_delete=models.CASCADE)
    work_title = models.CharField(max_length=60)
    company_name = models.CharField(max_length=60)
    years_worked = models.PositiveIntegerField(
        validators=[MaxValueValidator(99)])
    experience_certificate = CloudinaryField('Docs/employee_experience')

    class Meta:
        verbose_name = 'Employee Work Experience Detail'
    def __str__(self):
        return self.Employee.first_name
# ----------------------------------------------------------Add Shift Type---------------------------------------------------------------


class Shift_type(BaseModel):  # create diffrent shifts here
    shift_name = models.CharField(max_length=60,unique=True)
    start_time = models.TimeField()
    # min time to report for marking attendence for the shift
    shift_attendence_time = models.TimeField()
    end_time = models.TimeField()
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.shift_name

    class Meta:
        verbose_name_plural = 'Shifts'
        verbose_name = 'New Shifts'


class Shift_type_to_date(BaseModel):  # assign shifts to each weekdays
    shift_type = models.OneToOneField(Shift_type, on_delete=models.CASCADE)
    monday = models.BooleanField(default=False)
    tuesday = models.BooleanField(default=False)
    wednesday = models.BooleanField(default=False)
    thursday = models.BooleanField(default=False)
    friday = models.BooleanField(default=False)
    saturday = models.BooleanField(default=False)
    sunday = models.BooleanField(default=False)
        
    def __str__(self):
        return self.shift_type.shift_name


    class Meta:
        verbose_name = 'Shift Active days'

    def clean(self):
        if not (self.monday or self.tuesday or self.wednesday or self.wednesday or self.thursday or self.friday or self.saturday or self.sunday):
            raise ValidationError("You must select a day")

# ----------------------------------------------------------Create teams--------------------------------------------------------------------------


class Team(BaseModel):  # create a new team
    team_name = models.CharField(max_length=60)
    facility = models.ForeignKey(Facility, on_delete=models.CASCADE)
    shift_name = models.ForeignKey(Shift_type, on_delete=models.CASCADE)
    note = models.TextField(max_length=300)

    def __str__(self):
        return self.team_name
    
    def is_add_shift(self):
        teams_name = self.team_name
        facilitys = self.facility
        shift_names = self.shift_name
        teamfac = Team.objects.filter(team_name=teams_name,facility=facilitys,shift_name=shift_names).exists()
        return teamfac
    
    # def clean(self):
    #     asa=TeamAdmin.save_model()
    #     if asa:
    #         raise ValidationError('Starting DateTime cannot be in the past!')

class Team_to_employee(BaseModel):  # assign employees to team
    team = models.ForeignKey(Team, on_delete=models.CASCADE)
    employee = models.ManyToManyField(Employee, blank=True)

    def __str__(self):
        return self.team.team_name

    class Meta:
        verbose_name = 'Set Employees to Team'
    

# ----------------------------------------------------------Create Payroll components---------------------------------------------------------------


COMP_TYPE = (("Addition", "Addition"), ("Deduction", "Deduction"))
GIVEN_TYPE = (("Monthly payrun", "Monthly payrun"),
              ("Employee payroll", "Employee payroll"))


class Payroll_component(BaseModel):  # create a new payroll component
    component_name = models.CharField(max_length=60)
    component_type = models.CharField(max_length=14, choices=COMP_TYPE)
    amount = models.DecimalField(max_digits=8, decimal_places=2)
    given_at = models.CharField(max_length=16, choices=GIVEN_TYPE)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'New Payroll component'
        verbose_name_plural = 'Payroll components'

    def __str__(self):
        return self.component_name+" - " + self.component_type + " - " + str(self.amount)


class Payroll(BaseModel):  # create payroll for each employee
    employee = models.ForeignKey(Employee, on_delete=models.CASCADE)
    Payroll_component = models.ManyToManyField(Payroll_component, blank=True)
    Monthly_pay = models.DecimalField(max_digits=8, decimal_places=2)

    class Meta:
        verbose_name = 'Payroll Detail'
    
    def __str__(self):
        return self.employee.first_name


# --------------------------------------------------------------Create Payrun------------------------------------------------------------------------------
STATUS_CHOICES = (("cleared", "cleared"), ("payed", "payed"))


class Payrun(BaseModel):
    employee = models.ForeignKey(Employee, on_delete=models.CASCADE)
    total_addition = models.DecimalField(max_digits=8, decimal_places=2)
    total_deduction = models.DecimalField(max_digits=8, decimal_places=2)
    gross_payable = models.DecimalField(max_digits=8, decimal_places=2)
    status = models.CharField(max_length=12, choices=STATUS_CHOICES)
    clearing_date = models.DateField()
    extra_bonus_or_fine = models.DecimalField(
        max_digits=8, decimal_places=2, default=0)

    def __str__(self):
        return str(self.id)


class Payrunbonus(BaseModel):
    payrun = models.ForeignKey(Payrun, on_delete=models.CASCADE)
    payroll_component = models.ForeignKey(
        Payroll_component, on_delete=models.CASCADE)
    times = models.PositiveIntegerField(default=1)

    class Meta:
        verbose_name = 'Bonuses / Deduction'
# --------------------------------------------------------------Attendence Dummy Model-----------------------------------------------------------------------


Attend = (
    ("yes", "yes"),
    ("no", "no")
)
salary_mark_choice = (
    ("cleared", "cleared"),
    ("notcleared", "notcleared")
)


class Attendance(BaseModel):
    employee_id = models.ForeignKey(
        Employee, on_delete=models.CASCADE, verbose_name='Employee')
    date = models.DateField(auto_now_add=True)
    in_time = models.TimeField(null=True,blank=True)
    out_time = models.TimeField(null=True,blank=True)
    total_hours = models.FloatField(null=True,blank=True)
    attendance_mark = models.CharField(
        max_length=4, choices=Attend, default='no')
    salary_mark = models.CharField(
        max_length=10, choices=salary_mark_choice, default='notcleared')
    shift_employee = models.ForeignKey(Shift_type, on_delete=models.CASCADE)
    

    def __str__(self):
        return self.employee_id.first_name


# --------------------------------------LEAVE BY LIYA-----------------------------------------------------------------------
CRITERIA_CHOICES = (
    ("yearly", "yearly"),
    ("monthly", "monthly")
)


class Leave(BaseModel):
    leave_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=60,unique=True)
    description = models.TextField(max_length=300)
    number_of_days = models.PositiveIntegerField(null=True)
    criteria = models.CharField(max_length=10, choices=CRITERIA_CHOICES)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.name + f" ({self.criteria}:{self.number_of_days})"


class Assign_leave(BaseModel):
    Assign_id = models.AutoField(primary_key=True)
    employee = models.ForeignKey(Employee, on_delete=models.CASCADE)
    leave_id = models.ManyToManyField(Leave, blank=True)

    class Meta:
        verbose_name = 'New Leave Assignment'
        verbose_name_plural = 'Assign leaves'

    def __str__(self):
        return str(self.leave_id.name)


def validate_leave_from_date(leave_from_date):
    if leave_from_date < timezone.now().date():
        raise ValidationError("Date cannot be in the past")


def validate_leave_to_date(leave_to_date):
    if leave_to_date < timezone.now().date():
        raise ValidationError("Date cannot be in the past")


STATUS_CHOICES = ((1, 'Approved'), (0, 'Rejected'),
                  (2, 'Waiting for approval'),)


class leave_request(BaseModel):
    request_id = models.AutoField(primary_key=True)
    employee_id = models.ForeignKey(
        Employee, on_delete=models.CASCADE, null=True)
    user_id = models.ForeignKey(
        Custom_User, on_delete=models.CASCADE, null=True)
    leave_from_date = models.DateField(validators=[validate_leave_from_date])
    leave_to_date = models.DateField(validators=[validate_leave_to_date])
    Reason = models.TextField()
    status = models.IntegerField(choices=STATUS_CHOICES, default=2)
    reason_reject = models.CharField(
        ('reason for rejection'), max_length=50, blank=True)
    # leave requested by HR or other employees (False means HR)
    ordinary_employee = models.BooleanField(default=True)
    # def __str__(self):
    #     return str(self.employee_id)

    def __str__(self):
        if self.employee_id is not None:
            return self.employee_id.first_name
        else:
            print("kkkkk")
            return self.user_id.user.first_name

    class Meta:
        verbose_name = 'Employees leave request'
    # class Meta:
    #     db_table='leave_request'
    #     permissions = (
    #         ('can_approve_leave_request', "Can approve leave_request"),
    #     )
