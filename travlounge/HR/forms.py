from dataclasses import field
from HR.widgets import BootstrapDateTimePickerInput
from django.db import models
from django import forms
from HR .models import Employee
from django.forms.fields import DateField
from django.contrib.admin.widgets import AdminDateWidget
from django.forms.widgets import NumberInput


def validate_file_extension(value):
    import os
    from django.core.exceptions import ValidationError
    ext = os.path.splitext(value.name)[1]
    valid_extensions = ['.pdf']
    if not ext.lower() in valid_extensions:
        raise ValidationError(u'Unsupported file extension.')


class CouplingUploadForm(forms.ModelForm):

    dob = forms.DateTimeField(
        required=True, widget=NumberInput(attrs={'type': 'date'}))
    account_expiry_date = forms.DateTimeField(
        required=True, widget=NumberInput(attrs={'type': 'date'}))

    class Meta:
        model = Employee
        fields = '__all__'


class CouplingUploadForm2(forms.ModelForm):

    dob = forms.DateTimeField(
        required=True, widget=NumberInput(attrs={'type': 'date'}))
    account_expiry_date = forms.DateTimeField(
        required=True, widget=NumberInput(attrs={'type': 'date'}))

    class Meta:
        model = Employee
        fields = '__all__'

