from django.db import models

from django.core.validators import MaxValueValidator
from IT_ADMIN.models import BaseModel
# Create your models here.

# //////////////////////////////////////////////////////////////////////////////////////////// User

class User_register(BaseModel): 
    first_name = models.CharField(max_length=60)
    last_name = models.CharField(max_length=50)
    email = models.EmailField(max_length=60,blank=True, null= True, unique= True)
    mobile = models.BigIntegerField(unique=True,validators=[MaxValueValidator(9999999999)])