@admin.register(Audit_schedule)
class Audit_scheduleAdmin(admin.ModelAdmin):
    list_display = ('audit_title',)
    exclude = ('is_deleted','justification_flag','score','status')
    actions = [delete_action]
    def has_delete_permission(self, request, obj=None):
        return False
    def get_queryset(self, request):
        queryset = super( Audit_scheduleAdmin, self).get_queryset(request)
        return queryset.filter(is_deleted=False)
@admin.register(Audit_schedule)
class Audit_scheduleAdmin(admin.ModelAdmin):
    list_display = ('audit_title',)
    exclude = ('is_deleted','justification_flag','score','status')
    actions = [delete_action]
    def has_delete_permission(self, request, obj=None):
        return False
    def get_queryset(self, request):
        queryset = super( Audit_scheduleAdmin, self).get_queryset(request)
        return queryset.filter(is_deleted=False)  