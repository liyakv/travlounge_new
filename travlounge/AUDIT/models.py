from urllib import response
from django.contrib import messages
from django.db import models
from IT_ADMIN.models import Facility, BaseModel, Facility_to_service
import cloudinary
from cloudinary.models import CloudinaryField
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User, Group
from django.core.validators import FileExtensionValidator
import datetime
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator

# Create your models


AUDIT_STATUS_CHOICES = (
    ("Awaiting", "Awaiting"),
    ("In Progress", "In Progress"),
    ("finished", "finished")
)


class Audit_headings(BaseModel):
    heading = models.CharField(max_length=50)

    def __str__(self):
        return self.heading

    class Meta:
        verbose_name = ' Audit Questions'
        verbose_name_plural = ' Make Audit Questions'


class Audit_questions(BaseModel):
    heading_id = models.ForeignKey(Audit_headings, on_delete=models.CASCADE)
    questions = models.CharField(max_length=50)
    instructions = models.CharField(max_length=50)

    def __str__(self):
        return str('')

    class Meta:
        verbose_name = ' Audit Question'
        verbose_name_plural = ' Audit Questions'


class Auditor_information(BaseModel):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    organisation = models.CharField(max_length=50)
    auditor_name = models.CharField(max_length=50)
    highest_position = models.CharField(max_length=50)
    email_regex = RegexValidator(regex=r'^[A-Za-z0-9]+@[a-z]+\.[a-z]{2,3}$', message='Email must be entered in the correct format ( eg. mail@example.com )')
    email = models.CharField(validators=[email_regex], max_length=50)
    phone_regex = RegexValidator(regex=r'^(\+[9][1])?(\+[9][1]\s)?\d{10}$', message="Phone number must be entered in the correct format (Either +91xxxxxxxxxx or xxxxxxxxxxx)")
    contact_number1 = models.CharField(validators=[phone_regex], max_length=17)
    contact_number2 = models.CharField(validators=[phone_regex], max_length=50)

    

    class Meta:
        verbose_name = 'New auditor'
        verbose_name_plural = 'Add auditors'

    def __str__(self):
        return self.auditor_name

    def save(self, *args, **kwargs):
        self.user.is_staff = True
        self.user.is_active = True
        self.user.email = self.email
        self.user.save()
        super(Auditor_information, self).save(*args, **kwargs)



SCORE_CHOICES = (
    ("pass", "pass"),
    ("fail", "fail"))


class Audit_schedule(BaseModel):
    audit_type = models.CharField(max_length=50)
    audit_title = models.CharField(max_length=50,)
    start_datetime = models.DateTimeField(help_text="Format: YYYY-MM-DD <br/> &emsp;&emsp;&emsp;&emsp; HH:MM:SS")
    end_datetime = models.DateTimeField(help_text="Format: YYYY-MM-DD <br/> &emsp;&emsp;&emsp;&emsp; HH:MM:SS")
    facility_id = models.ForeignKey(
        Facility, on_delete=models.CASCADE, verbose_name='Select facility')
    service_id = models.ForeignKey(
        Facility_to_service, on_delete=models.CASCADE, null=True, verbose_name='Select service')
    auditor = models.ForeignKey(
        Auditor_information, on_delete=models.CASCADE, related_name="auditor")
    score = models.CharField(max_length=50, choices=SCORE_CHOICES)
    heading_id = models.ForeignKey(
        Audit_headings, on_delete=models.CASCADE, verbose_name='Select Questions')
    status = models.CharField(
        max_length=200, default="Awaiting", choices=AUDIT_STATUS_CHOICES)
    justification_flag = models.BooleanField(max_length=200, default=False)


    class Meta:
        verbose_name = 'A New Scheduled Audit'
        verbose_name_plural = 'Schedule an Audit'

    def __str__(self):
        return self.audit_title

    def facility_id_link(self):
        return self.facility_id

    # def save(self, *args, **kwargs):
    #     print(self.start_datetime.replace(tzinfo=None),'101010',datetime.datetime.now())
    #     if self.start_datetime.replace(tzinfo=None) < datetime.datetime.now():
    #         raise ValidationError("The date cannot be in the past!")
    #     if self.end_datetime.replace(tzinfo=None) < self.start_datetime.replace(tzinfo=None):
    #         raise ValidationError("The date cannot be lesser than starting time!")
    #     super(Audit_schedule, self).save(*args, **kwargs)
    
    def is_past(self):
        if self.start_datetime.replace(tzinfo=None) < datetime.datetime.now():
            resul=None
            return resul
        else:
            resul=1
            return resul
    def is_lesser(self):
        if self.end_datetime.replace(tzinfo=None) < self.start_datetime.replace(tzinfo=None):
            result=None
            return result
        else:
            result=1
            return result

    def clean(self):
        print(self.id,'\n\n\n\n\n\n')
        if self.id==None:
            if not self.start_datetime==None:
                if not self.is_past():
                    raise ValidationError('Starting DateTime cannot be in the past!')
                if not self.is_lesser():
                    raise ValidationError('Ending time/date cannot be lesser than starting!')

    facility_id_link.short_description = "facility"
    facility_id_link.allow_tags = True


JUSTIFICATION_CHOICES = (
    ("cleared", "cleared"),
    ("pending", "pending"))


class Audit_issues(BaseModel):
    issues = models.CharField(max_length=50)
    description = models.TextField(max_length=50)
    justification = models.CharField(max_length=50)
    status = models.CharField(
        max_length=200, default="pending", choices=JUSTIFICATION_CHOICES)
    audit_no = models.ForeignKey(
        Audit_schedule, on_delete=models.CASCADE, related_name="auditnum", verbose_name='Audit')
    justification_status = models.CharField(
        max_length=100, default="pending", choices=JUSTIFICATION_CHOICES)

    def __str__(self):
        return self.issues

    class Meta:
        verbose_name = 'Issues'
        verbose_name_plural = 'Issues'


class Audit_info(BaseModel):
    audit_no = models.ForeignKey(
        Audit_schedule, on_delete=models.CASCADE, verbose_name='Audit')
    auditor_id = models.ForeignKey(
        Auditor_information, on_delete=models.CASCADE, verbose_name='Auditor')
    audit_start_datetime = models.DateTimeField(null=True, blank=True)
    audit_end_datetime = models.DateTimeField(null=True, blank=True)


class Audit_performance_analysis_report(BaseModel):
    audit_no = models.ForeignKey(
        Audit_schedule, on_delete=models.CASCADE, null=True, verbose_name='Audit')
    auditor_id = models.ForeignKey(
        Auditor_information, on_delete=models.CASCADE, null=True, verbose_name='Auditor')
    comments = models.CharField(max_length=200, null=True)
    honesty = models.IntegerField(null=True)
    work_quality = models.IntegerField(null=True)
    punctality = models.IntegerField(null=True,  verbose_name='punctuality')
    attitude = models.IntegerField(null=True)

    def __str__(self):
        return str(self.auditor_id)

    class Meta:
        verbose_name = 'Auditor analysis'
        verbose_name_plural = 'Auditor analysis'


class Audit_star_classification(BaseModel):
    audit_no = models.ForeignKey(
        Audit_schedule, on_delete=models.CASCADE, verbose_name='Audit')
    facility_id = models.ForeignKey(
        Facility, on_delete=models.CASCADE, verbose_name='Facility')
    service = models.ForeignKey(
        Facility_to_service, on_delete=models.CASCADE, null=True)
    star = models.IntegerField()

    class Meta:
        verbose_name = 'Audit rating'
        verbose_name_plural = 'Audit rating'

    def __str__(self):
        return str(self.audit_no.audit_title)


class Auditshedule_docs(BaseModel):
    
    # def validate_file_size(value):
    #     print('hello\n\n')
    #     filesize= value.size
    #     if filesize > 10485760:
    #         raise ValidationError("You cannot upload file more than 10Mb")
    #     else:
    #         return value

    Audit_schedule = models.ForeignKey(
        Audit_schedule, on_delete=models.CASCADE, related_name="doc", verbose_name='Audit')
    doc_name = models.CharField(
        max_length=60, null=True, verbose_name='Document Name  (document should in pdf format)')
    docs = CloudinaryField('Docs/Audit_schedule_docs',help_text='Upload document in pdf format')

    def __str__(self):
        return self.doc_name

    def clean(self):
        if self.id==None:
            extensions=('.pdf', '.doc', '.docx',
                        '.ods', '.xls', '.xlsm', '.xlsx')
            if not self.docs.name.endswith(extensions):
                raise ValidationError("File should be in pdf, doc, docx, xls, xlsx or ods format")
            size=self.docs.size
            if size>10485760:
                raise ValidationError("You cannot upload file more than 10MB")
            else:
                pass
        else:
            pass
        


class Auditshedule_return_docs(BaseModel):
    Audit_schedule = models.ForeignKey(
        Audit_schedule, on_delete=models.CASCADE, verbose_name='Audit')
    doc_name = models.CharField(
        max_length=60, null=True, verbose_name='Document Name(document should in pdf format)')
    docs = CloudinaryField('Docs/Audit_schedule_return_docs', help_text="Upload a pdf file with one or more identity details inside")

    class Meta:
        verbose_name = 'Return document'
        verbose_name_plural = 'Return documents'

    def __str__(self):
        return self.doc_name

    def clean(self):
        if self.id==None:
            extensions=('.pdf', '.doc', '.docx',
                        '.ods', '.xls', '.xlsm', '.xlsx')
            if not self.docs.name.endswith(extensions):
                raise ValidationError("File should be in pdf, doc, docx, xls, xlsx or ods format")
            size=self.docs.size
            if size>10485760:
                raise ValidationError("You cannot upload file more than 10MB")
            
            else:
                pass
        else:
            pass


class Audit_answers_saved(BaseModel):
    audit_shedule = models.ForeignKey(
        Audit_schedule, on_delete=models.CASCADE, related_name="audit_ans", verbose_name='Audit')
    question_id = models.CharField(max_length=100, verbose_name='Question')
    answer = models.CharField(max_length=100)
    instructions = models.CharField(max_length=100)


class Audit_implimentations(models.Model):
    Audit_schedule = models.ForeignKey(
        Audit_schedule, on_delete=models.CASCADE, related_name="impli")
    implimentations = models.CharField(max_length=200)
    descriptions = models.CharField(max_length=200)

    def __str__(self):
        return str('')

    class Meta:
        verbose_name = 'Implementation'
        verbose_name_plural = 'Implementation'
