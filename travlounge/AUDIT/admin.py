from django.contrib import admin
from django.db.models.signals import post_init
from AUDIT .models import Audit_info, Auditor_information, Audit_schedule, Audit_headings, Audit_questions, Auditshedule_docs, Audit_answers_saved, Audit_issues, Audit_implimentations, Auditshedule_return_docs, Audit_star_classification, Audit_performance_analysis_report
from django import forms
from IT_ADMIN .models import Facility_to_service, Service_type, Facility,Custom_User
from django.utils.html import format_html
from django.urls import path
from django.http import HttpResponse
from django.shortcuts import redirect, render
from django.http import HttpResponseRedirect
from django.db.models import Q
import jsonify
from django.http import JsonResponse
import json
from django.contrib import messages
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import Group, User, Permission
from nested_inline.admin import NestedStackedInline, NestedModelAdmin
from AUDIT .forms import CouplingUploadForm
from django.forms.models import BaseInlineFormSet


# Register your models here
@admin.action(description='Delete')
def delete_action(modeladmin, request, queryset):
    queryset.update(is_deleted=True)

class RequiredInlineFormSet(BaseInlineFormSet):
    def _construct_form(self, i, **kwargs):
        form = super(RequiredInlineFormSet, self)._construct_form(i, **kwargs)
        form.empty_permitted = False
        return form

class QuestionAdmin(admin.TabularInline):
    model = Audit_questions
    fields = [('questions'), ('instructions'), ('is_deleted'), ]
    extra = 1
    formset = RequiredInlineFormSet
    actions = [delete_action]

    def has_delete_permission(self, request, obj=None):
        return False

    def get_queryset(self, request):
        queryset = super(QuestionAdmin, self).get_queryset(request)
        return queryset.filter(is_deleted=False)


@admin.register(Audit_headings)
class QuestionHeadingAdmin(admin.ModelAdmin):
    inlines = [QuestionAdmin, ]
    actions = [delete_action]
    exclude = ('is_deleted',)
    search_fields = ['heading']
    list_per_page = 10
    def has_delete_permission(self, request, obj=None):
        return False

    def get_queryset(self, request):
        queryset = super(QuestionHeadingAdmin, self).get_queryset(request)
        return queryset.filter(is_deleted=False)


class Auditshedule_docsAdmin(admin.TabularInline):
    model = Auditshedule_docs
    exclude = ('is_deleted',)
    form = CouplingUploadForm
    extra = 1
    verbose_name = 'Audit schedule Documents'  # object name here
    verbose_name_plural = 'Audit schedule Documents'


@admin.register(Audit_schedule)
class Audit_scheduleAdmin(admin.ModelAdmin):
    change_form_template = "change_form52.html"
    inlines = [Auditshedule_docsAdmin, ]
    fields = [('audit_type'), ('audit_title'), ('start_datetime'), ('end_datetime'),
              ('facility_id'), ('service_id'), ('auditor'), ('heading_id'), ]
    list_display = ('audit_title', 'status')
    readonly_fields = ()
    exclude = ('is_deleted', 'justification_flag', 'score', 'status',)
    actions = [delete_action]
    list_per_page = 10
    search_fields = ['audit_title']
    list_filter = ('status',)

    def has_delete_permission(self, request, obj=None):
        return False

    def get_queryset(self, request):
        request.session['id'] = request.user.id
        pay = request.session['id']
        del request.session['id']
        queryset = super(Audit_scheduleAdmin, self).get_queryset(request)
        return queryset.filter(is_deleted=False)

    def add_view(self, request, form_url='', extra_context=None):
        if request.method == 'GET':
            object_passed = request.GET.getlist('data')
            if len(object_passed) != 0:
                list_tuple = Audit_schedule.objects.values_list(
                    'audit_title').filter(is_deleted=False)
                list_facility = [i[0] for i in list_tuple]
                if object_passed[0] in list_facility:
                    return JsonResponse({"message": "Successfully published", 'status': 200})
            request.GET.getlist('offset_limit')
            id_passed = request.GET.getlist('offset_limit')
            if len(id_passed) != 0:
                list1 = []
                service_id_fetched = Facility_to_service.objects.values_list(
                    'service_type_id').filter(facility=id_passed[0])
                service_id = [item for t in service_id_fetched for item in t]
                for each in service_id:
                    servicetype = Service_type.objects.get(
                        id=each).service_type
                    list1.append(servicetype)
                data_id = Facility_to_service.objects.values_list(
                    'id').filter(facility=id_passed[0])
                out_pt = [item for t in data_id for item in t]

                dictionary = dict(zip(out_pt, list1))

                return JsonResponse(dictionary)
        return super(Audit_scheduleAdmin, self).add_view(request, form_url, extra_context=extra_context)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "auditor":
            kwargs["queryset"] = Auditor_information.objects.filter(
                is_deleted=False)
        if db_field.name == "heading_id":
            kwargs["queryset"] = Audit_headings.objects.filter(
                is_deleted=False)
        if db_field.name == "facility_id":
            kwargs["queryset"] = Facility.objects.filter(
                is_deleted=False, facility_status='running')
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        context.update({
            'show_save_and_continue': False,
            'show_save_and_add_another': False,
            'show_save': False, })
        return super().render_change_form(request, context, add, change, form_url, obj)


class Auditshedule_docs2Admin(admin.TabularInline):
    model = Auditshedule_docs
    exclude = ('is_deleted',)
    readonly_fields = ('Audit_schedule', 'doc_name',)

    def has_delete_permission(self, request, obj=None):
        return False

    def get_queryset(self, request):
        queryset = super(Auditshedule_docs2Admin, self).get_queryset(request)
        return queryset.filter(is_deleted=False)

    def has_add_permission(self, request, obj=None):
        return False


class Audit_implimentationsAdmin(admin.TabularInline):
    model = Audit_implimentations
    exclude = ('is_deleted',)
    extra = 1


class Audit_issuesAdmin(admin.TabularInline):
    model = Audit_issues
    exclude = ('is_deleted', 'status', 'justification', 'justification_status')
    extra = 1


class Auditshedule_return_docsAdmin(admin.TabularInline):
    model = Auditshedule_return_docs
    exclude = ('is_deleted', 'status')
    form = CouplingUploadForm
    extra = 1


class Audit_scheduleProxy(Audit_schedule):
    class Meta:
        proxy = True
        verbose_name = 'Assigned Audit'
        verbose_name_plural = 'Assigned Audits'


@admin.register(Audit_scheduleProxy)
class Audit_scheduleProxyAdmin(admin.ModelAdmin):
    change_form_template = "change_form53.html"
    list_per_page=10
    # list_filter = (
    #     ('auditor'),
    # )

    list_display = ('auditor', 'audit_title', 'status')

    readonly_fields = [('audit_type'), ('audit_title'), ('start_datetime'),
                       ('end_datetime'), ('facility_id'), ('service_id'), ('auditor'), ]
    fields = [('auditor'), ('audit_type'), ('audit_title'), ('start_datetime'),
              ('end_datetime'), ('facility_id'), ('service_id'), ('score'), ]
    inlines = [Audit_implimentationsAdmin,
               Audit_issuesAdmin, Auditshedule_return_docsAdmin, ]
    search_fields = ['audit_title']

    def has_delete_permission(self, request, obj=None):
        return False

    def get_queryset(self, request):
        current_user = request.user
        queryset = super(Audit_scheduleProxyAdmin, self).get_queryset(request)
        return queryset.filter(is_deleted=False, status='Awaiting', auditor__user=request.user)

    def has_add_permission(self, request, obj=None):
        return False

    def change_view(self, request, object_id, form_url='', extra_context=None):

        x = Audit_schedule.objects.get(pk=object_id).heading_id.id
        check_status = Audit_questions.objects.filter(heading_id=x)
        check_value = Auditshedule_docs.objects.values(
            'docs').filter(Audit_schedule=object_id)
        extra_context = extra_context or {}
        extra_context['question'] = check_status
        extra_context['docs'] = check_value
        return super().change_view(request, object_id, form_url, extra_context=extra_context)

    def save_model(self, request, obj, form, change):

        for (a, b, c) in zip((request.POST.getlist('question')), (request.POST.getlist('instruction')), (request.POST.getlist('score'))):
            data_save = Audit_answers_saved(
                audit_shedule=obj, question_id=a, instructions=b, answer=c)
            data_save.save()
        super().save_model(request, obj, form, change)
        Audit_schedule.objects.filter(id=obj.id).update(status='In Progress')

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        context.update({
            'show_save_and_continue': False,

            'show_save_and_add_another': False,
        })
        return super().render_change_form(request, context, add, change, form_url, obj)


class Audit_issuesProxy(Audit_issues):
    class Meta:
        proxy = True
        verbose_name = '  View issues'
        verbose_name_plural = ' View issues'


@admin.register(Audit_issuesProxy)
class Audit_issuesProxyAdmin(admin.ModelAdmin):
    change_form_template = "change_form54.html"

    list_filter = (
        ('status'),
    )
    exclude = [('is_deleted'), ('justification_status')]
    list_display = ('audit_no', 'issues', 'description',
                    'justification', 'status')

    readonly_fields = [('issues'), ('justification'), ('audit_no'), ]
    search_fields = ['audit_no__audit_title', 'issues', 'description']

    list_per_page=10

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj=None):
        return False

    def response_change(self, request, obj):

        if "reassign" in request.POST and obj.status == 'cleared':
            messages.error(request, ' "cleared issue cannot be reassigned""')
            return HttpResponseRedirect(".")
        if not "reassign" in request.POST and obj.status == 'pending':
            messages.error(
                request, ' "take some action!!! either reassign or clear the issue"')

            return HttpResponseRedirect(".")
        else:
            self.model.objects.filter(id=obj.id).update(
                justification_status="pending")
            return super().response_change(request, obj)

    def get_queryset(self, request):
        queryset = super(Audit_issuesProxyAdmin, self).get_queryset(request)
        group=request.user.groups.values_list('name', flat=True).first()
        if group== None:
            return queryset.filter(is_deleted=False)
        else:
            return queryset.filter(is_deleted=False,audit_no__auditor__user=request.user)

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        context.update({
            'show_save_and_continue': False,
            'show_save_and_add_another': False,
        })
        return super().render_change_form(request, context, add, change, form_url, obj)


class Audit_implimentations2Admin(admin.TabularInline):
    model = Audit_implimentations
    exclude = ('is_deleted',)
    readonly_fields = ('Audit_schedule', 'implimentations', 'descriptions')

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj=None):
        return False


class Audit_issues2Admin(admin.TabularInline):
    model = Audit_issues
    exclude = ('is_deleted', 'status', 'justification')
    readonly_fields = ('issues', 'justification_status', 'description',)
    list_per_page = 10

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj=None):
        return False


class Auditshedule_return_docs2Admin(admin.TabularInline):
    model = Auditshedule_return_docs
    exclude = ('is_deleted', 'status',)
    readonly_fields = ('doc_name', 'docs')

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj=None):
        return False


class Audit2_scheduleProxy(Audit_schedule):
    class Meta:
        proxy = True
        verbose_name = '  View Completed  Audit'
        verbose_name_plural = ' View Completed  Audits'


@admin.register(Audit2_scheduleProxy)
class Audit2_scheduleProxyAdmin(admin.ModelAdmin):

    change_form_template = "change_form55.html"
    
    list_display = ('auditor', 'audit_title', 'status')

    readonly_fields = [('audit_type'), ('audit_title'), ('start_datetime'),
                       ('end_datetime'), ('facility_id_link'), ('service_id'), ('auditor'), ('score')]

    fields = [('audit_type'), ('audit_title'), ('start_datetime'),
              ('end_datetime'), ('facility_id_link'), ('service_id'), ('auditor'), ('score'), ]
    inlines = [Audit_implimentations2Admin, Audit_issues2Admin, ]
    search_fields = ['audit_title', ]
    list_per_page=10
    def change_view(self, request, object_id, form_url='', extra_context=None):
        check_status = Audit_answers_saved.objects.filter(
            audit_shedule=object_id)
        extra_context = extra_context or {}
        extra_context['question'] = check_status
        extra_context['redoc'] = Auditshedule_return_docs.objects.values(
            'docs').filter(Audit_schedule=object_id)
        return super().change_view(request, object_id, form_url, extra_context=extra_context)


    def get_list_filter(self, request):
        if request.user.is_superuser:
            return ['auditor']
        else:
            return []

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj=None):
        return False

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        context.update({
            'show_save_and_continue': False,
            'show_save_and_add_another': False,
            'show_save': False,

        })
        return super().render_change_form(request, context, add, change, form_url, obj)

    def get_queryset(self, request):
        queryset = super(Audit2_scheduleProxyAdmin, self).get_queryset(request)
        group=request.user.groups.values_list('name', flat=True).first()
        if group== None:
            return queryset.filter(is_deleted=False, status="In Progress")
        else:
            return queryset.filter(is_deleted=False, status="In Progress", auditor__user=request.user)


    class Media:
        js = ('js/admin/admin.js',)
        css = {
            'all': ('css/admin/admin.css',)
        }

    def response_change(self, request, obj):
        pending_issues = Audit_schedule.objects.filter(
            auditnum__status='pending')
        if obj in pending_issues:
            messages.error(request, 'clear all the issues')
            return HttpResponseRedirect(".")
        if set(['rating']).issubset(request.POST.keys()):
            Audit_schedule.objects.filter(id=obj.id).update(status='finished')
            rated_value = request.POST.getlist('rating')
            data_save = Audit_star_classification(
                audit_no=obj, facility_id=obj.facility_id, service=obj. service_id, star=rated_value[0])
            data_save.save()
            return redirect(f'/admin/AUDIT/audit_performance_analysis_report/add/?objct={obj.id}',)
        else:
            messages.error(request, 'rate the auditor')
            return HttpResponseRedirect(".")
        return super().response_change(request, obj)


class Audit_performance_analysis_reportAdmin(admin.ModelAdmin):
    def has_module_permission(self, request):
        return False
    exclude = [('audit_no'), ('auditor_id'), ('honesty'),
               ('work_quality'), ('punctality'), ('attitude'), ('is_deleted'), ]
    change_form_template = "change_form56.html"

    def add_view(self, request, form_url='', extra_context=None):
        extra_context = extra_context or {}
        extra_context['my_extra_content'] = request.GET.get('objct')
        return super(Audit_performance_analysis_reportAdmin, self).add_view(request, form_url, extra_context=extra_context)
    list_per_page = 10

    def save_model(self, request, obj, form, change):

        my_id_for_formfield = 0
        if set(['honesty', 'rating_work', 'rating_punctuality', 'rating_attitude']).issubset(request.POST.keys()):
            self.my_id_for_formfield = 1
            objectpassed = request.POST.getlist('object_passed')
            get_object = objectpassed[0]

            attitude = request.POST.getlist('rating_attitude')
            punctuality = request.POST.getlist('rating_punctuality')
            work = request.POST.getlist('rating_work')
            comments = request.POST.getlist('comments')
            honesty = request.POST.getlist('honesty')
            id_audit = Audit_schedule.objects.get(id=get_object)
            auditor1 = Audit_schedule.objects.get(id=get_object).auditor
            data_save = Audit_performance_analysis_report(
                audit_no=id_audit, auditor_id=auditor1, comments=comments[0], honesty=honesty[0], work_quality=work[0], punctality=punctuality[0], attitude=attitude[0])
            data_save.save()
        else:
            self.my_id_for_formfield = 2
            messages.error(request, 'No changes are permitted ..')

    def response_add(self, request, obj):
        object_id1 = self.my_id_for_formfield
        if object_id1 == 1:
            return redirect('/admin/AUDIT/audit_performance_analysis_reportadminproxy')
        if object_id1 == 2:
            objectpassed = request.POST.getlist('object_passed')
            get_object = objectpassed[0]
            return redirect(f'/admin/AUDIT/audit_performance_analysis_report/add/?objct={get_object}')

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        context.update({
            'show_save_and_continue': False,
            'show_save_and_add_another': False,

        })
        return super().render_change_form(request, context, add, change, form_url, obj)


admin.site.register(Audit_performance_analysis_report,
                    Audit_performance_analysis_reportAdmin)


class Audit_star_classificationProxy(Audit_star_classification):
    class Meta:
        proxy = True
        verbose_name = '  View closed  Audit'
        verbose_name_plural = ' View closed  Audits'


@admin.register(Audit_star_classificationProxy)
class Audit_star_classificationProxyAdmin(admin.ModelAdmin):
    def has_delete_permission(self, request, obj=None):
        return False
    list_display = ('facility_id', 'service', 'star')
    readonly_fields = [('facility_id'), ('service'), ('star'), ('audit_no'), ]
    exclude = [('is_deleted'), ]
    list_per_page = 10
    search_fields = ['facility_id__facility_name', ]

    def has_add_permission(self, request, obj=None):
        return False

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        context.update({
            'show_save_and_continue': False,
            'show_save_and_add_another': False,
            'show_save': False,
        })
        return super().render_change_form(request, context, add, change, form_url, obj)


class Audit_performance_analysis_reportAdminProxy(Audit_performance_analysis_report):
    class Meta:
        proxy = True
        verbose_name = ' Auditor performance '
        verbose_name_plural = 'Auditor performance'


@admin.register(Audit_performance_analysis_reportAdminProxy)
class Audit_performance_analysis_reportAdminProxyAdmin(admin.ModelAdmin):

    list_filter = (('audit_no'), ('auditor_id'),)
    list_display = ('auditor_id', 'audit_no',)
    readonly_fields = [('audit_no'), ('auditor_id'), ('honesty'),
                       ('work_quality'), ('punctality'), ('attitude'), ]
    fields = [('audit_no'), ('auditor_id'), ('honesty'),
              ('work_quality'), ('punctality'), ('attitude'), ]
    list_per_page = 10
    search_fields = [ 'auditor_id__auditor_name','audit_no__audit_title' ]

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj=None):
        return False

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        context.update({
            'show_save_and_continue': False,
            'show_save_and_add_another': False,
            'show_save': False,
        })
        return super().render_change_form(request, context, add, change, form_url, obj)


# employee basic details are entered
class Add_auditorInline(NestedStackedInline):
    model = Auditor_information

    exclude = ('is_deleted', 'status',)
    min_num = 1

    def has_delete_permission(self, request, obj=None):
        return False
    actions = [delete_action]


class ProxyUser(User):  # To create a copy of users table
    class Meta:
        proxy = True
        verbose_name_plural = 'Add Auditors'
        verbose_name = 'Auditor'


@admin.register(ProxyUser)
# To modify copy of users table with our employee table
class UserAdmin1(UserAdmin):
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        # our_user = request.user.id
        # fac = list(User.objects.all())
        # emp = list(Auditor_information.objects.all(
        #     ).values_list("user", flat=True))
        users = User.objects.filter(groups__name='AUDIT')
        return queryset.filter(id__in=users)

    inlines = (Add_auditorInline, )
    list_display = ('username', 'email', 'is_staff', 'Auditor')
    search_fields = ('username', 'email','auditor_information__auditor_name')
    
    def Auditor(self, obj1):
        perm = Group.objects.filter(name='AUDIT')
        # gp=Permission.objects.all()
        # print(gp,588888888888555)
        # print(perm,'1111111111111')
        # users = User.objects.filter(Q(groups__permissions=perm)).distinct()
        # users = User.objects.filter(groups__name='AUDIT')
        # print(users,1111111111110000000)
        # if Auditor_information.objects.filter(user=obj1.id).exists():
        #     print(Auditor_information.objects.filter(user=obj1.id).exists())
        # print('000000000000000000000')
        aud = Auditor_information.objects.get(user=obj1)
        # print(aud,'222222222222222')
        # aud=Auditor_information.objects.all()
        return aud.auditor_name
    fieldsets = (
        (None, {'fields': ('username','password')}),
        # (('Personal info'), {'fields': ('email',)}),
        (('Permissions'), {
            'fields': ('is_active', 'is_staff',  'groups'),
        }),
    )
    readonly_fields=('groups', )

    list_per_page = 10

    def has_delete_permission(self, request, obj=None):
        return False

    def response_add(self, request, obj):
        # q=user.groups(user_id=obj.id,group_id=6)
        # q.save()
        user = User.objects.get(id=obj.id)
        group = Group.objects.get(id=6)
        user.groups.add(group)
        p=Custom_User(user_id=obj.id,role_id=6)
        p.save()
        url = f"/admin/AUDIT/proxyuser/{obj.id}/change/"
        return redirect(url)

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        context.update({
            'show_save_and_continue': False,
            'show_save_and_add_another': False,
        })
        return super().render_change_form(request, context, add, change, form_url, obj)
