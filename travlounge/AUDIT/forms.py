from dataclasses import field, fields
from django import forms
from django.forms import ModelForm
from django.db.models.manager import Manager
from AUDIT .models import Audit_info, Auditor_information, Audit_schedule, Audit_headings, Audit_questions, Auditshedule_docs, Audit_answers_saved, Audit_issues, Audit_implimentations, Auditshedule_return_docs, Audit_star_classification, Audit_performance_analysis_report
from django.core.exceptions import ValidationError
import os


# def validate_file_extension(value):
#     ext = os.path.splitext(value.name)[1]
#     valid_extensions = ['.pdf', '.doc', '. docx',
#                         '.ods', '.xls', '.xlsm', '.xlsx']
#     if not ext.lower() in valid_extensions:
#         raise ValidationError(u'Unsupported file extension.')


class CouplingUploadForm(forms.ModelForm):

    docs = forms.FileField(label=' File Upload:',
                           required=True,
                            # validators=[validate_file_extension],
                           help_text='Upload document in pdf format')

    class Meta:
        model = Auditshedule_docs
        exclude = ['doc_name']
