

# # ===========================models edited=========================================================
# from django.db import models
# from IT_ADMIN.models import BaseModel, Facility, Facility_to_service, Service_type
# import WAREHOUSE.models
# import cloudinary
# from cloudinary.models import CloudinaryField
# # # Create your models here.


# class Missingproducts(BaseModel):
#     GSN_number = models.ForeignKey(
#         "WAREHOUSE.GSN", on_delete=models.CASCADE, related_name='GRNnumber')
#     # --------------------------------
#     product = models.ForeignKey(
#         "WAREHOUSE.Stock_batch", on_delete=models.CASCADE, related_name='PID')
#     quantity = models.CharField(max_length=50)
#     facility = models.ForeignKey(
#         Facility, on_delete=models.CASCADE, related_name='facilitydamaged')

#     def __str__(self):
#         return str("")

#     class Meta:
#         verbose_name = 'Damaged / Missing Product'
#         verbose_name_plural = 'Damaged / Missing Products'


# class Urgentpurchase(BaseModel):
#     REASON_CHOICES = (("None", "--------"),
#                       ("PRODUCTS DEMAND INCREASED", "PRODUCTS DEMAND INCREASED"),
#                       ("THERE WERE MORE DAMAGED PRODUCTS",
#                        "THERE WERE MORE DAMAGED PRODUCTS"),
#                       ("OTHER", "OTHER"),)
#     PRIORITY_CHOICES = (("None", "--------"), ("0", "LOW"),
#                         ("1", "MEDIUM"),
#                         ("2", "HIGH"),)
#     reason = models.CharField(
#         max_length=50, choices=REASON_CHOICES, default='pending')
#     need_before = models.DateField(null=True, blank=True)
#     Facility_id = models.ForeignKey(
#         Facility, on_delete=models.CASCADE, related_name='facility_urgent')
#     priority = models.CharField(
#         max_length=50, choices=PRIORITY_CHOICES, default="MEDIUM")
#     status = models.BooleanField(default=False)
#     # warehouse = models.ForeignKey("IT_ADMIN.Warehouse_details",on_delete=models.CASCADE)

#     def __str__(self):
#         return str(self.Facility_id)+" : "+str(self.need_before)

#     class Meta:
#         verbose_name = 'A New Urgent Purchase Request'
#         verbose_name_plural = 'Urgent purchases'


# class Urgentpurchase_products(BaseModel):
#     urgentpurchase_id = models.ForeignKey(
#         Urgentpurchase, on_delete=models.CASCADE, related_name='purchase_id')
#     product_name = models.ForeignKey(
#         "WAREHOUSE.Product", on_delete=models.CASCADE)
#     quantity = models.CharField(max_length=50)

#     def __str__(self):
#         return str(self.id)

#     class Meta:
#         verbose_name = 'Products for Urgent Purchase'


# class InternalStock(BaseModel):
#     facility_id = models.ForeignKey(
#         Facility, on_delete=models.CASCADE, related_name='facility_stock', null=True, blank=True)
#     product = models.ForeignKey(
#         "WAREHOUSE.Stock_batch", on_delete=models.CASCADE)
#     stock_quantity = models.PositiveIntegerField(null=True, blank=True)

#     def __str__(self):
#         return str(self.product)


# # ==========================models before edit====================================================
# # from django.db import models
# # from IT_ADMIN.models import BaseModel,Facility,Facility_to_service,Service_type
# # # import WAREHOUSE.models
# # import cloudinary
# # from cloudinary.models import CloudinaryField
# # # # Create your models here.
# # class Missingproducts(BaseModel):#i think no change
# #     GSN_number =  models.ForeignKey("WAREHOUSE.GSN",on_delete=models.CASCADE, related_name='GRNnumber')
# #     product=models.ForeignKey("WAREHOUSE.Stock_batch",on_delete=models.CASCADE, related_name='PID')   #--------------------------------
# #     quantity=models.CharField(max_length=50)
# #     facility=models.ForeignKey(Facility,on_delete=models.CASCADE,related_name='facilitydamaged')
# #     def __str__(self):
# #         return str("")


# # class Urgentpurchase(BaseModel):#warehouse forginkey field
# #     REASON_CHOICES = (("None", "--------"),
# #      ("PRODUCTS DEMAND INCREASED", "PRODUCTS DEMAND INCREASED"),
# #     ("THERE WERE MORE DAMAGED PRODUCTS", "THERE WERE MORE DAMAGED PRODUCTS"),
# #     ("OTHER", "OTHER"),)
# #     PRIORITY_CHOICES = ( ("None", "--------"),("0", "LOW"),
# #     ("1", "MEDIUM"),
# #     ("2", "HIGH"),)
# #     reason = models.CharField(max_length=50,choices = REASON_CHOICES,default = 'pending')
# #     need_before=models.DateField(null=True, blank=True)
# #     Facility_id=models.ForeignKey(Facility,on_delete=models.CASCADE,related_name='facility_urgent')
# #     priority=models.CharField(max_length=50,choices = PRIORITY_CHOICES,default="MEDIUM")
# #     status=models.BooleanField(default=False)
# #     def __str__(self):
# #         return str(self.Facility_id)+" : "+str(self.need_before)
# #     class Meta:
# #         verbose_name = 'A New Urgent Purchase Request'
# #         verbose_name_plural = 'Urgent purchases'


# # class Urgentpurchase_products(BaseModel):#no change
# #     urgentpurchase_id=models.ForeignKey(Urgentpurchase,on_delete=models.CASCADE,related_name='purchase_id')
# #     product_name=models.ForeignKey("WAREHOUSE.Product",on_delete=models.CASCADE)
# #     quantity=models.CharField(max_length=50)

# #     def __str__(self):
# #         return str(self.id)
# #     class Meta:
# #         verbose_name = 'Products for Urgent Purchase'

# # class InternalStock(BaseModel):
# #     facility_id=models.ForeignKey(Facility,on_delete=models.CASCADE,related_name='facility_stock',null=True, blank=True)
# #     product = models.ForeignKey("WAREHOUSE.Stock_batch",on_delete=models.CASCADE)
# #     stock_quantity = models.PositiveIntegerField(null=True,blank=True)
# #     def __str__(self):
# #         return str(self.product)
