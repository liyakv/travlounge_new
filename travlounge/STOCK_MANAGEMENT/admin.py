# from django.contrib import admin
# from STOCK_MANAGEMENT.models import Urgentpurchase,Urgentpurchase_products,InternalStock,Missingproducts
# from django.shortcuts import redirect
# from HR.models import Employee
# from IT_ADMIN.models import Facility,token_get,Custom_User
# from WAREHOUSE.models import GSN,GSN_products,Stock_batch
# from django.contrib.admin import SimpleListFilter
# from django import forms
# from django.http import JsonResponse
# from FCMManager import sendPush
# from  FCMManager import *

# # -------------------------------------------------InternalStock-------------------------------------------------
# class MinMaxQty(SimpleListFilter):  #list filter
#     title = ('stock quantity')
#     parameter_name = 'stock'
#     def lookups(self, request, model_admin):
#         return(
#             ('0','Minimum stock'),
#             ('1','Maximum stock')
#         )
#     def queryset(self, request, queryset):
#         if self.value()=='0':
#             return queryset.filter(stock_quantity__lte=10)
#         if self.value()=='1':
#             return queryset.filter(stock_quantity__gte=100)
# @admin.register(InternalStock)
# class StockAdmin(admin.ModelAdmin):
#     change_form_template = "extend_stock.html"
#     list_display = ('product_name','get_batch','stock_quantity',)
#     list_filter=('product__batch_expiry',MinMaxQty,)
#     fields=('id',)
    
#     list_per_page=5
#     def has_delete_permission(self, request, obj=None):
#         return False
#     def has_change_permission(self, request, obj=None):
#         return False
#     def has_add_permission(self, request, obj=None):
#         return False
#     def get_queryset(self, request):
#         # ------------------------------------------------------------------------------------------------------------
#         userid=request.user.id
#         Facility_id=list(Employee.objects.filter(user=userid).values_list('facility_to_service__facility',flat=True))
#         if Facility_id:
#             f_id=Facility_id[0]
#             queryset = super().get_queryset(request).filter(is_deleted=False,facility_id=f_id)
#         # -------------------------------------------------------------------------------------------------------------
#         return queryset
#     def change_view(self, request, object_id, form_url='', extra_context=None): #function used to check if html passes button to stop or restart facility
#         extra_context = extra_context or {}
#         x=InternalStock.objects.filter(id=object_id).values('product__product__product_name','stock_quantity','product__product__selling_price','product_id','product__batch_no','product__batch_expiry')
#         extra_context['status']=x
#         return super().change_view(request, object_id, form_url,  extra_context=extra_context)
#     def product_name(self, obj):
#         q=list(InternalStock.objects.filter(id=obj.id).values_list('product__product__product_name',flat=True))
#         return q
#     def get_batch(self, obj):
#         return obj.product.batch_no
#     get_batch.short_description = 'Batch number'
#     get_batch.admin_order_field = 'product__batch_no'

# # --------------------------------------------gsn---------------------------------------------------------------------
# @admin.action(description='Delete')
# def delete_action(modeladmin, request, queryset):
#     queryset.update(is_deleted=True)

# class GSN_Form(forms.ModelChoiceField):
#     def label_from_instance(self, obj):
#         return "{}".format(obj.id)

# class GSNproductsAdmin(admin.TabularInline):
#     model = GSN_products
#     fields=('instock_products','send_qty')
#     readonly_fields=('instock_products','send_qty')
#     extra = 1
#     def has_delete_permission(self, request, obj=None):
#         return False
# class GSNProxy(GSN):
#     class Meta:
#         proxy=True
#         verbose_name = 'notes'
#         verbose_name_plural = 'Goods send note'
# @admin.register(GSNProxy)
# class GSN_Admin(admin.ModelAdmin):
#     inlines = [GSNproductsAdmin,]
#     list_display = ('gsn_id','recived_date','send_date','Number_products','reciving_status')
#     exclude = ('is_deleted','reciving_status','facility')
#     readonly_fields=('send_date',)
#     list_filter=('reciving_status',)
#     list_per_page=5
#     change_form_template = "extend_gsn.html"
#     def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
#         context.update({
#             'show_save_and_continue': False,
#             'show_save_and_add_another': False,
#             'show_save': False,
#             })
#         return super().render_change_form(request, context, add, change, form_url, obj)

#     def gsn_id(self, obj):
#         return obj.id
#     def Number_products(self, obj):
#         q=GSN_products.objects.filter(gsn=obj.id)
#         return q.count()
#     def has_add_permission(self, request, obj=None):
#         return False

#     def response_change(self, request, obj, post_url_continue=None):
#         GSN.objects.filter(id=obj.id).update(reciving_status=True)
#         y1=GSN_products.objects.all()
#         y=y1.filter(gsn=obj.id).values('instock_products','send_qty','instock_products__batch_no','instock_products__product')
#         # ----------------------------------------------------------------------------------------------------------
#         userid=request.user.id
#         Facility_id=list(Employee.objects.filter(user=userid).values_list('facility_to_service__facility',flat=True))
#         if Facility_id:
#             f_id=Facility_id[0]
#         # -------------------------------------------------------------------------------------------------------------
#         for i in y:
#             b=i['instock_products__batch_no']
#             p=i['instock_products__product']
#             batch_check=InternalStock.objects.filter(product__batch_no__icontains=b,product__product=p,facility_id=f_id)
#             if batch_check.exists():
#                 extng_qty=list(batch_check.values_list('stock_quantity', flat=True))
#                 qty=i['send_qty']+extng_qty[0]
#                 batch_check.update(stock_quantity=qty)
#             else:
#                 InternalStock(product_id=i['instock_products'],stock_quantity=i['send_qty'],facility_id_id=f_id).save()
#         return redirect("/admin/STOCK_MANAGEMENT/")
#     def change_view(self, request, object_id, form_url='', extra_context=None): #function used to check if html passes button to stop or restart facility
#         check_facility = GSN.objects.filter(id=object_id)
#         if check_facility:
#             check_status=check_facility[0].reciving_status
#             extra_context = extra_context or {}
#             extra_context['status'] = check_status 
#         return super().change_view(request, object_id, form_url,extra_context=extra_context)
#     def has_delete_permission(self, request, obj=None):
#         return False
#     def get_queryset(self, request):
#         # ------------------------------------------------------------------------------------------------------------
#         userid=request.user.id
#         Facility_id=list(Employee.objects.filter(user=userid).values_list('facility_to_service__facility',flat=True))
#         if Facility_id:
#             f_id=Facility_id[0]
#             queryset = super().get_queryset(request).filter(is_deleted=False,facility_id=f_id)
#         # --------------------------------------------------------------------------------------------------------------
#         return queryset

# # --------------------------------------damaged_products---------------------------------------------------------------
# class Damage_Form(forms.ModelChoiceField):
#     def label_from_instance(self, obj):
#         return "{}".format(obj.id)
# @admin.register(Missingproducts)
# class DamagedproductAdmin(admin.ModelAdmin):
#     list_display=['id','created_on']
#     exclude = ('is_deleted','facility')
#     list_filter=('created_on',)
#     list_per_page=5
#     change_form_template = "damage_prdts.html"
#     def has_delete_permission(self, request, obj=None):
#         return False

#     def get_queryset(self, request):
#         queryset = super().get_queryset(request)
#         return queryset.filter(is_deleted=False)
#     def formfield_for_foreignkey(self, db_field, request, **kwargs):
#         if db_field.name == "GSN_number":
#             id_user=request.user.id
#             check_facility_id=list(Employee.objects.filter(user=id_user).values_list('facility_to_service__facility',flat=True))
#             if check_facility_id:
#                 facility_id=check_facility_id[0]
#                 request.session['facilityID'] = facility_id
#             return Damage_Form(queryset=GSN.objects.filter(reciving_status=False,facility=facility_id))
#         if db_field.name == "facility":
#             facility_id=request.session['facilityID']
#             kwargs["queryset"] = Facility.objects.filter(id=facility_id)
#         return super().formfield_for_foreignkey(db_field, request, **kwargs)
#     def save_model(self, request, obj, form, change):
#         self.object = obj
#         pass
#     def response_add(self, request, obj, post_url_continue=None):
#         print(obj.product.id)
#         GSN_stk=GSN_products.objects.filter(gsn=obj.GSN_number,instock_products__id=obj.product.id)
#         if GSN_stk.exists():
#             initial_qty=list(GSN_stk.values_list('send_qty',flat=True))
#             stock_quantity_n=int(initial_qty[0])-int(obj.quantity)
#             GSN_stk.update(send_qty=stock_quantity_n)
#             id_user=request.user.id
#             check_facility_id=list(Employee.objects.filter(user=id_user).values_list('facility_to_service__facility',flat=True))
#             if check_facility_id:
#                 facility_id=check_facility_id[0]
#                 damage_save=Missingproducts(GSN_number=obj.GSN_number,product=obj.product,quantity=obj.quantity,facility_id=facility_id)
#                 damage_save.save()
#                 USERID=request.user.id

#                 x = Employee.objects.get(user=request.user).facility_to_service.facility.id
#                 y=Facility.objects.filter(id=x).values_list('warehouse',flat=True)
#                 custom=Employee.objects.values_list('user').filter(warehouse__in=y)
#                 out = [item for t in custom for item in t]
#                 tk = token_get.objects.values_list("token_get").filter(user__in=out)
#                 registration_token = [item for t in tk for item in t]
#                 sendPush(title="missing product reported",msg=f'Product ID={obj.product.id},Product Name={obj.product}',registration_token=registration_token)
#         return redirect("/admin/STOCK_MANAGEMENT/")
#     def add_view(self, request, form_url='', extra_context=None):
#         if request.method == 'GET':
#             id_passed = request.GET.get('pdt')
#             lst=[]
#             if id_passed != None:
#                 out1=list(GSN_products.objects.filter(gsn=id_passed).values_list('instock_products',flat=True))
#                 out=Stock_batch.objects.filter(id__in=out1).values('id','batch_no','product__product_name')
#                 objlist=list(out)
#                 lst.append(objlist)
#                 responseobj = {}
#                 responseobj['data']=lst
#                 return JsonResponse(responseobj)
#         return super().add_view(request, form_url='', extra_context=None)

# # ------------------------------------------------Urgentpurchase------------------------------------------------------

# @admin.action(description='Delete')
# def delete_action(modeladmin, request, queryset):
#     queryset.update(is_deleted=True)
# class StartTimeFilter(SimpleListFilter):
#     title = ('order status')
#     parameter_name = 'status'
#     def lookups(self, request, model_admin):
#         return(
#             ('true','Closed requests'),
#             ('false','Open requests')
#         )
#     def queryset(self, request, queryset):
#         if self.value()=='false':
#             return queryset.filter(status=False)
#         if self.value()=='true':
#             return queryset.filter(status=True)



# class UrgentpurchaseproductsAdmin(admin.TabularInline): 
#     model = Urgentpurchase_products
#     exclude=('is_deleted','PID',)
#     extra = 1
#     actions = [delete_action]
#     def get_queryset(self, request):
#         queryset = super().get_queryset(request)
#     def has_delete_permission(self, request, obj=None):
#         return False
# @admin.register(Urgentpurchase)
# class UrgentpurchaseAdmin(admin.ModelAdmin):
#     list_display = ('created_on','Number_products_ordered','priority','status',)
#     list_filter=(StartTimeFilter,)
#     list_per_page=5
#     inlines = [UrgentpurchaseproductsAdmin,]
#     exclude=('is_deleted','status',)
#     def has_change_permission(self, request, obj=None):
#         return False
#     def Number_products_ordered(self, obj):
#         q=Urgentpurchase_products.objects.filter(urgentpurchase_id=obj.id)
#         return q.count()
#     def get_queryset(self, request):
#        queryset = super(UrgentpurchaseAdmin, self).get_queryset(request)
#        return queryset.order_by('status')
#     def has_delete_permission(self, request, obj=None):
#         return False
#     def formfield_for_foreignkey(self, db_field, request, **kwargs):
#         if db_field.name == "Facility_id":
#             id_user=request.user.id
#             check_facility_id=list(Employee.objects.filter(user=id_user).values_list('facility_to_service__facility',flat=True))
#             if check_facility_id:
#                 facility_id=check_facility_id[0]
#                 kwargs["queryset"] = Facility.objects.filter(id=facility_id)
#         return super().formfield_for_foreignkey(db_field, request, **kwargs)


